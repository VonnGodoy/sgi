package br.com.vle.controller.usuario;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.model.ModelUsuario;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class AlterarSenhaController extends ControllerBase {


	private static final long serialVersionUID = 8776100090021916640L;

	@Inject
	private transient Logger logger;

	@Inject
	private DaoUsuario daoUsuario;
	
	@Inject
	private ModelUsuario model;
	
	@Inject
	private DaoPessoa daoPessoa;
	
	private String senhaAtual;
	
	private String senhaNova;
	
	private String senhaConfirm;
	 
		
	/**
	 * Initializing Data Access Service for LazyUserDataModel class role list
	 * for UserContoller class
	 */
	@PostConstruct
	public void init() {
		
    	DTOFiltroBase filtro = new DTOFiltroBase();

    	if(this.getUsuarioLogado().getId() != null && !this.getUsuarioLogado().getId().toString().equals("")){
    		filtro.addFiltro("cpf_cnpj", this.getUsuarioLogado().getId().toString());
    	}		
		model.setPessoa(new Pessoa());
		model.setPessoa(daoPessoa.buscarPessoa(filtro));
		
		logger.info("====================================");
		logger.info("==  Start Alterar Senha Controller  ==");
		logger.info("=====================================");
		//logger.info("senha atual  = " + getUsuarioLogado().getPassword());
		model.setUsuarioSelecionado(getUsuarioLogado());
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("=== Stop Alterar Senha Controller  ===");
		logger.info("====================================");
	}

	
	public String alterarSenha() {

		if(!validaPreenchimento()){
			
			return redirect(Pages.MANTER_USUARIO_ALTERAR_SENHA, false);
		}else if(validaSenha()){
		
		model.getUsuarioSelecionado().setPassword(this.senhaNova.toString());
		daoUsuario.alterarSenha(model.getUsuarioSelecionado());
		messageInfo("Senha Alterada com sucesso");
		return redirect(Pages.HOME, false);
		
		}else {
			
			return redirect(Pages.MANTER_USUARIO_ALTERAR_SENHA, false);
		}

	}
	
	public boolean validaPreenchimento() {
		boolean retorno = true;
		
		if(this.senhaAtual.toString().equals("") || this.senhaAtual.toString() == null) {
			
			retorno = false;
			messageErro("O campo Senha e de Preenchimento Obrigatório");
		}
		
		if(this.senhaNova.toString().equals("") || this.senhaNova.toString() == null) {
			
			retorno = false;
			messageErro("O campo Nova Senha e de Preenchimento Obrigatório");
		}
		
		if(this.senhaNova.toString().equals("") || this.senhaNova.toString() == null) {
			
			retorno = false;
			messageErro("O campo Confirmar Senha e de Preenchimento Obrigatório");
		}
		
		return retorno;
	}
	
	public boolean validaSenha() {
		boolean retorno = true;
		
		if(!this.senhaNova.toString().equals(this.senhaConfirm.toString())) {
			
			retorno = false;
			messageErro("O campos Nova Senha e Confirmar Senha Não Conferem");
		}
		
		if(this.senhaAtual.toString().equals(this.senhaNova.toString())) {
			
			retorno = false;
			messageErro("Senha Deve Ser Diferente da Ultima Senha Válida");
		}
		
		return retorno;
	}
	
	public String cancelarAlterarSenha() {
		return facesRedirect(Pages.HOME);
	}
	
	public ModelUsuario getModel() {
		return model;
	}

	public void setModel(ModelUsuario model) {
		this.model = model;
	}

	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	public String getSenhaNova() {
		return senhaNova;
	}

	public void setSenhaNova(String senhaNova) {
		this.senhaNova = senhaNova;
	}

	public String getSenhaConfirm() {
		return senhaConfirm;
	}

	public void setSenhaConfirm(String senhaConfirm) {
		this.senhaConfirm = senhaConfirm;
	}

}
