package br.com.vle.controller.usuario;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.faces.component.html.HtmlInputText;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.controller.perfilacesso.Eventos;
import br.com.vle.controller.perfilacesso.ProducePerfilAcessoSelecionado;
import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Usuario;
import br.com.vle.model.ModelUsuario;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
@URLMapping(id = "alterarcontroller", pattern = "/manter_usuario_alterar/#{alterarUsuarioController.id}/", viewId = "/page/manterusuario/manter_usuario_alterar.xhtml")
public class AlterarUsuarioController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	private @Inject
	transient Logger logger;

	@Inject
	private DaoUsuario daoUsuario;

	private String id;

	@Inject
	@ProducePerfilAcessoSelecionado
	private PerfilAcesso perfilSelecionado;

	@Inject
	private ModelUsuario model;

	@Inject
	private Conversation conversation;

	@Inject
	private Event<Eventos> events;

	private HtmlInputText nome = new HtmlInputText();
	private HtmlInputText sobrenome = new HtmlInputText();
	private HtmlInputText email = new HtmlInputText();

	/**
	 * Initializing Data Access Service for LazyUserDataModel class role list
	 * for UserContoller class
	 */
	@PostConstruct
	public void init() {
		logger.info("======================================");
		logger.info("==  Start AlterarUsuarioController  ==");
		logger.info("==  conversation = " + conversation.getId());
		logger.info("======================================");

		if (conversation.isTransient()) {
			logger.info("==  conversation begin             ==");
			conversation.begin();
		}

	}

	@PreDestroy
	public void destroi() {
		logger.info("=======================================");
		logger.info("===  Stop AlterarUsuarioController  ===");
		logger.info("=======================================");
	}

	public String alterar() {
		//logger.info("nome atual = " + model.getUsuarioSelecionado().getNome());
		logger.info("Perfil atual = " + perfilSelecionado.getId());

		model.getUsuarioSelecionado().setPerfilAcesso(perfilSelecionado);

		daoUsuario.update(model.getUsuarioSelecionado());

		messageInfo("usuario_alterar_sucesso");

		return voltar(true);

	}

	public String alterarSenha() {
		//logger.info("nome atual = " + model.getUsuarioSelecionado().getNome());

		model.getUsuarioSelecionado().setPerfilAcesso(perfilSelecionado);

		daoUsuario.update(model.getUsuarioSelecionado());

		messageInfo("usuario_alterar_senha_sucesso");

		return facesRedirect(Pages.HOME);

	}

	public String cancelarAlterarSenha() {
		return facesRedirect(Pages.HOME);
	}

	/**
	 * @return
	 */
	public String doBuscarPerfil() {
//		logger.info("nome atual = " + model.getUsuarioSelecionado().getNome());
		logger.info("doBuscarPerfil atual = " + perfilSelecionado);
		logger.info("novo nome  = " + (String) nome.getSubmittedValue());

//		model.getUsuarioSelecionado().setNome((String) nome.getSubmittedValue());
//		model.getUsuarioSelecionado().setSobrenome((String) sobrenome.getSubmittedValue());
//		model.getUsuarioSelecionado().setEmail((String) email.getSubmittedValue());

		return redirect(Pages.MANTER_PERFIL_SELECAO);
	}

	@URLAction
	public String loadItem() {
		if (id != null) {
			Usuario usuario = daoUsuario.find(id);
			if(usuario == null){
				messageErro("usuario_nao_existe");
				return facesRedirect(Pages.HOME);
			}
			model.setUsuarioSelecionado(usuario);
			Eventos e = new Eventos();
			e.setPerfilAcesso(usuario.getPerfilAcesso());
			events.fire(e);
		}
		return "pretty:store";
	}

	public ModelUsuario getModel() {
		return model;
	}

	public void setModel(ModelUsuario model) {
		this.model = model;
	}

	public HtmlInputText getNome() {
		return nome;
	}

	public void setNome(HtmlInputText nome) {
		this.nome = nome;
	}

	public HtmlInputText getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(HtmlInputText sobrenome) {
		this.sobrenome = sobrenome;
	}

	public HtmlInputText getEmail() {
		return email;
	}

	public void setEmail(HtmlInputText email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
