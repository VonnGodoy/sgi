package br.com.vle.controller.pessoa;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.controller.perfilacesso.Eventos;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Usuario;
import br.com.vle.enuns.EstadosEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.model.ModelEndereco;
import br.com.vle.model.ModelPessoa;
import br.com.vle.servico.dao.DaoEndereco;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;

/**
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class IncluirPessoaController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	private @Inject
	transient Logger logger;

	@Inject	
	private DaoPessoa daoPessoa;
	
	@Inject	
	private DaoUsuario daoUsuario;
	
	@Inject	
	private DaoEndereco daoEndereco;
	
	@Inject
	private ModelPessoa model;
	 
	@Inject
	private Conversation conversation;
	
	@Inject 
	private Event<Eventos> events;
	
	private EstadosEnum estado;
	
	private EstadosEnum[] estados(){
		return EstadosEnum.values();	
	}

	/**
	 * Initializing Data Access Service for LazypessoaDataModel class role list
	 * for PessoaContoller class
	 */
	@PostConstruct
	public void init() {
		logger.info("=====================================");
		logger.info("==  Start Incluir Pessoa Controller  ==");
		logger.info("==  conversation = " + conversation.getId());
		logger.info("=====================================");
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("======================================");
		logger.info("===  Stop Incluir Pessoa Controller  ===");
		logger.info("======================================");
	}


	
	public String salvar() {


		logger.info("=======================================");
		logger.info("==========  Salvando Pessoa  ==========");
		logger.info("=======================================");
		
		model.getPessoa().setAtivo(true);
		model.getPessoa().setCpfCnpj(model.getCpfCnpj());
		model.getPessoa().setDtNascimento(model.getDtNascimento());
		model.getPessoa().setEmail(model.getEmail());
		model.getPessoa().setNome(model.getNome());
		model.getPessoa().setIe(model.getIe());
		model.getPessoa().setNumero(model.getNumero());
		model.getPessoa().setRefNome(model.getRefNome());
		model.getPessoa().setRefNome1(model.getRefNome1());
		model.getPessoa().setRefTel(model.getRefTel());
		model.getPessoa().setRefTel1(model.getRefTel1());
		model.getPessoa().setRg(model.getRg());
		model.getPessoa().setTelPrin(model.getTelPrin());
		model.getPessoa().setTelSec(model.getTelSec());
		model.getPessoa().setDtCadastro(new Date());
		
		if(!model.getEstado().equals(EstadosEnum.SELECIONE)) {
		model.getPessoa().setSsp(model.getEstado().getUf().toString());
		
		
		daoPessoa.create(model.getPessoa());
				
		messageInfo("Pessoa Incluida Com Sucesso");
		
		criarUsuario();
		
		model.setPessoa(new Pessoa());
		}else{
			messageErro("O Campo SSP é Obrigatório");
			return redirect(Pages.MANTER_PESSOA_INCLUIR);
		}
		
		return voltar(true);

	}
	
	public void criarUsuario() {
		
		Usuario NewUsuario = new Usuario();
		
		NewUsuario.setId(model.getPessoa().getCpfCnpj().toString());
		NewUsuario.setPerfilAcesso(model.getPerfilUsuario());
		NewUsuario.setAtivo(model.getStatus().getStatus());
		NewUsuario.setPrimeiroAcesso(true);
		
		String value = NewUsuario.getId().toString().substring(0,6);
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			byte[] hash = messageDigest.digest(value.getBytes("UTF-8"));
			StringBuilder stringBuilder=  new StringBuilder();
			for (int i = 0; i < hash.length; i++) {
				stringBuilder.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
			}
			NewUsuario.setPassword(stringBuilder.toString());
		} catch (NoSuchAlgorithmException e) {
			
			messageErro("Erro ao Gerar Senha de usuario");
			NewUsuario.setPassword(null);
		} catch (UnsupportedEncodingException e) {
			
			messageErro("Erro ao Gerar Senha de usuario encoding errado");
			NewUsuario.setPassword(null);
		}
		
		daoUsuario.create(NewUsuario);
		
		model.setCpfCnpj("");
		model.setStatus(StatusEnum.SELECIONE);
		
		//messageInfo("usuario_salvar_sucesso");
	}
	
	public void buscarEndereco() {

		if (model.getCep()!=null && model.getCep().toString().trim().length() == 8) {

			ModelEndereco endereco = daoEndereco.buscaEnderecoPorCep(model.getCep().toString());

			if(endereco!= null && endereco.getStatusConexao()) {

				messageErro("Erro ao Buscar CEP");
			}else if(endereco!= null){
				if (model.getPessoa() == null) {
					model.setPessoa(new Pessoa());
				}
				
				model.setLogradouro(endereco.getLogradouro());
				model.setComplemento(endereco.getComplemento());
				model.setBairro(endereco.getBairro());
				model.setLocalidade(endereco.getLocalidade());
				model.setUf(endereco.getUf());
				
				model.getPessoa().setLogradouro(endereco.getLogradouro());
				model.getPessoa().setComplemento(endereco.getComplemento());
				model.getPessoa().setBairro(endereco.getBairro());
				model.getPessoa().setLocalidade(endereco.getLocalidade());
				model.getPessoa().setUf(endereco.getUf());
				model.getPessoa().setCep(endereco.getCep());

			}else {
				messageErro("Cep Inválido");
			}
			
		}else {
			
			messageErro("O Campo CEP é Obrigatório");
		}

	}

	public ModelPessoa getModel() {
		return model;
	}

	public void setModel(ModelPessoa model) {
		this.model = model;
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}
	
	public EstadosEnum[] getEstados() {
		return estados();
	}
	
	public EstadosEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadosEnum estado) {
		this.estado = estado;
	}

}
