package br.com.vle.controller.usuario.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.vle.model.dto.UsuarioDTO;

/**
 *
 * @author Victor Godoi
 */

@Path("services/usuario")
public interface UsuarioRest {
	
	@GET
	@Path("/todos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	List<UsuarioDTO> todosUsuario();

}
