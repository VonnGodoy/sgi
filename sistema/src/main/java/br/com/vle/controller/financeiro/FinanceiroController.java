package br.com.vle.controller.financeiro;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.entidade.Financeiro;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.enuns.TipoContratosEnum;
import br.com.vle.model.ModelFinanceiro;
import br.com.vle.relatorios.financeiro.ListaFinanceiro;
import br.com.vle.servico.dao.DaoFinanceiro;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class FinanceiroController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	@Inject
	private Conversation conversation;

	@Inject
	private transient Logger logger;

	@Inject
	private ModelFinanceiro model;

	@Inject
	private DaoFinanceiro daoFinanceiro;

	@Inject
	private ListaFinanceiro listaFinanceiro;
	

	@PostConstruct
	public void init() {
		logger.info("=====================================");
		logger.info("=====  Start Financeiro Controller  =====");
		logger.info("=====================================");

	}

	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("=====  Stop Financeiro Controller  =====");
		logger.info("====================================");
	}

	public String limpar(){ 

		model.setIdContrato("");
		model.setIdImovel("");
		model.setIdPessoa("");
		model.setContratoAtivo(StatusEnum.SELECIONE);
		model.setTpContrato(TipoContratosEnum.SELECIONE);

		return redirect(Pages.MANTER_CONTRATOS);
	}    

	public String lancamentos(Financeiro lan){

		model.setSelecionado(lan);
		return redirect(Pages.FINANCEIRO_LANCAMENTOS);

	}

	public void prepara(){

		if(conversation.isTransient())
			conversation.begin();

	} 

	public String gerarRelatorioSelecionados() throws Exception {
		
		if(model.getSelecionados().size() > 0) {
			
			listaFinanceiro.gerar(model.getSelecionados());

			messageInfo("Relatorio Financeiro Gerada Com Sucesso");
		}else {
			messageErro("Nenhum Lancamento Foi Selecionado");
		}

		return redirect(Pages.FINANCEIRO);
	}

	public String gerarRelatorioTodos() throws Exception {

		DTOFiltroBase filtro = new DTOFiltroBase();

		listaFinanceiro.gerar(daoFinanceiro.buscarTudo(filtro));

		messageInfo("Relatorio Financeiro Gerada Com Sucesso");

		return redirect(Pages.FINANCEIRO);
	}

}
