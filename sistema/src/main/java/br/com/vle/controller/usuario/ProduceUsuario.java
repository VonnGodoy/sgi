package br.com.vle.controller.usuario;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.entidade.Usuario;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

public class ProduceUsuario {

	@Inject
	private DaoUsuario daoUsuario;

	@Produces
	@RequestScoped 
    @Named("usuarios")
	public List<Usuario> getSistemas(){
		DTOFiltroBase filtro = new DTOFiltroBase();
		filtro.setPrimeiroRegistroPage(0);
		filtro.setQtdRegistroPaginacao(0L);
		filtro.addFiltro("ativo", Boolean.TRUE);
		List<Usuario> lista = daoUsuario.buscarTodosUsuarios(filtro);
		return lista;
	}
	
}
