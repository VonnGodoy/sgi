package br.com.vle.controller.login;

import static br.com.vle.comum.UtilJSF.getBundleMsg;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.CookieController;
import br.com.vle.comum.Pages;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Usuario;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.model.ModelEmail;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;
import br.com.vle.util.DateUtil;
import br.com.vle.util.JavaMail;
import br.com.vle.util.ValidatorUtils;

/**
 * Login Controller
 * 
 * @author Victor Godoi
 */
@SuppressWarnings("serial")
@Named
@SessionScoped
public class LoginController extends ControllerBase {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	private transient CookieController cookieController = new CookieController();

	private String username;
	private String password;
	private Boolean remember;
	private String email;
	private Date dtNasc;

	@Inject
	private DaoUsuario dao;

	@Inject
	private DaoPessoa daoPessoa;

	private Usuario usuario;

	private JavaMail javaMail = new JavaMail();

	private ModelEmail model;

	/**
	 * Creates a new instance of LoginController
	 */
	public LoginController() {

	}

	@PostConstruct
	public void init() {
		checkCookie();
	}

	public boolean validarPrenchimento(){

		boolean retorno = true;

		if(this.username == null || this.username.equals("")) {
			retorno = false;
			messageErro("O Campo CPF/CNPJ é de preenchimento obrigatório");

		}

		if(this.password == null || this.password.equals("")) {
			retorno = false;
			messageErro("O Campo Senha é de preenchimento obrigatorio");
		}

		return retorno;

	}

	public String login() {

		Pages page = Pages.HOME;
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();


		try {

			if(validarPrenchimento()) {

				request.login(username, password);

				this.usuario = dao.find(this.username);

				if (!usuario.getAtivo()) {
					messageErro("Usuário não está ativo!");
					HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
					if (session != null) {
						session.invalidate();
					}
					return redirect(Pages.LOGIN);
				}

				this.usuario.setPassword(null);
				if (this.usuario.getPrimeiroAcesso()) {
					page = Pages.MANTER_USUARIO_ALTERAR_SENHA;
				}

				if (logger.isInfoEnabled()) {
					logger.info("User ({0}) loging in #" + DateUtil.getCurrentDateTime(),
							request.getUserPrincipal().getName());
				}

				if (remember) {
					gravaCookie();
				}
			}else {
				return redirect(Pages.LOGIN);
			}

		} catch (ServletException e) {
			if (e.toString().contains("User already logged in")) {
				messageInfo("Usuario {0} já está logado", username);
				return redirect(Pages.HOME);
			}
			logger.error(e.toString(), e);
			context.addMessage(null, new FacesMessage("Error!", getBundleMsg("erro_login", null)));

			messageErro("Usuario/Senha Invàlido");
			return redirect(Pages.LOGIN);
		}

		return facesRedirect(page);
	}

	public String redirect() {

		if(this.username !=null) {

			return redirect(Pages.RESET_SENHA);
		}else {
			messageErro("CPF/CNPJ é de Preenchimento obrigatório");
			return redirect(Pages.LOGIN);	
		}
	}

	public String voltar() {
		this.dtNasc = null;
		this.email = null;
		this.usuario = null;

		return redirect(Pages.LOGIN);
	}

	public String resetSenha() {
		boolean erro = false;
		FacesContext context = FacesContext.getCurrentInstance();

		if (this.usuario != null) {

			if (!usuario.getAtivo()) {
				messageErro("Usuário não está ativo Contate o Administrador!");
				HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
				if (session != null) {
					session.invalidate();
				}
				return redirect(Pages.LOGIN);
			}
		} else {

			this.usuario = dao.find(this.username);
			this.usuario.setPassword(null);

			if (!usuario.getAtivo()) {
				messageErro("Usuário não está ativo! /n Contate o Administrador");
				HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
				if (session != null) {
					session.invalidate();
				}
				return redirect(Pages.LOGIN);
			} else {

				erro = validarDados();

			}

		}
		if (!erro) {
			this.dtNasc = null;
			this.email = null;
			return redirect(Pages.LOGIN);
		} else {
			return redirect(Pages.RESET_SENHA);
		}
	}

	public boolean validarDados() {
		boolean erro = false;

		DTOFiltroBase filtro = new DTOFiltroBase();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String dtInformada = formato.format(this.dtNasc);

		if (this.dtNasc != null && this.email != null) {

			if (this.usuario != null && !this.usuario.getId().toString().equals("")) {

				if (ValidatorUtils.isValidDocument(this.usuario.getId().toString())) {

					filtro.addFiltro("cpf_cnpj", this.usuario.getId().toString());

					Pessoa pessoa = daoPessoa.buscarPessoa(filtro);

					String dtDB = formato.format(pessoa.getDtNascimento());

					if (dtDB.toString().equals(dtInformada) && pessoa.getEmail().toString().equals(this.email)) {
						gerarSenha(pessoa);
						pessoa = null;
					} else {
						messageErro("Dados Informados não coferem");
						erro = true;
						;
					}
				} else {
					messageErro("Cpf/Cnpj Inválido");
					erro = true;
				}

			} else {

				messageErro("O Campo Cpf/Cnpj é de Preenchimento Obrigatório");
				erro = true;
			}

		} else if (this.dtNasc == null) {
			messageErro("O Campo Data de Nascimento é de Preenchimento Obrigatório");
			erro = true;

		} else if (this.email == null) {
			messageErro("O Campo Email é de Preenchimento Obrigatório");
			erro = true;

		}

		return erro;
	}

	private void gerarSenha(Pessoa pessoa) {
		boolean erro = false;

		this.usuario.setPrimeiroAcesso(true);
		String value = this.usuario.getId().toString().substring(0, 6);
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			byte[] hash = messageDigest.digest(value.getBytes("UTF-8"));
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < hash.length; i++) {
				stringBuilder.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
			}
			this.usuario.setPassword(stringBuilder.toString());
		} catch (NoSuchAlgorithmException e) {
			erro = true;
			messageErro("Erro ao Gerar Senha de usuario");
			this.usuario.setPassword(null);
		} catch (UnsupportedEncodingException e) {
			erro = true;
			messageErro("Erro ao Gerar Senha de usuario encoding errado");
			this.usuario.setPassword(null);
		}

		if (!erro) {
			dao.update(this.usuario);

			model.setDestinatario(pessoa.getEmail());
			model.setAssunto("Nova Senha Imobiliaria");
			model.setMensagem("Sua Senha foi restaurada com sucesso, sua nova senha é: "+pessoa.getCpfCnpj().toString().substring(0,6));

			javaMail.Enviar(model);

			this.usuario = null;

			messageInfo("Nova Senha enviada para o email cadastrado");
		}
	}

	/**
	 * Listen for logout button clicks on the #{loginController.logout} action and
	 * navigates to login screen.
	 */
	public void logout() {

		setUsuario(null);
		cookieController.limparCookieLogin();

		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();

		if (logger.isInfoEnabled()) {
			logger.info("User ({0}) loging out #" + DateUtil.getCurrentDateTime(),
					request.getUserPrincipal().getName());
		}

		try {
			request.logout();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		if (session != null) {
			session.invalidate();
		}
		FacesContext.getCurrentInstance().getApplication().getNavigationHandler()
		.handleNavigation(FacesContext.getCurrentInstance(), null, "/login.xhtml?faces-redirect=true");
	}

	private void gravaCookie() {
		cookieController.gravaCookie(CookieController.BTUSER, username);
		cookieController.gravaCookie(CookieController.BTPASSWD, password);
		cookieController.gravaCookie(CookieController.BTREMEMBER, remember.toString());
	}

	public void checkCookie() {

		username = cookieController.carregaCookie(CookieController.BTUSER);
		password = cookieController.carregaCookie(CookieController.BTPASSWD);
		String stremenber = cookieController.carregaCookie(CookieController.BTREMEMBER);
		if (stremenber != null) {
			remember = Boolean.valueOf(stremenber);
		}

		if (remember != null && remember) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();

			try {
				String url = "/home/";
				request.login(username, password);

				this.usuario = dao.find(this.username);
				this.usuario.setPassword(null);
				if (this.usuario.getPrimeiroAcesso()) {
					url = "/alterar_senha/";
				}

				if (logger.isInfoEnabled()) {
					logger.info("User ({0}) loging in #" + DateUtil.getCurrentDateTime(),
							request.getUserPrincipal().getName());
				}

				facesContext.getExternalContext().redirect(request.getContextPath() + url);

			} catch (ServletException e1) {
				logger.error(e1.getMessage(), e1);
				facesContext.addMessage(null, new FacesMessage("Error!", getBundleMsg("erro_login", null)));
				messageErro("erro_login");
			} catch (IOException e1) {
				logger.error(e1.getMessage(), e1);
			}

		}

	}

	/**
	 * @return username
	 */
	@NotNull
	@NotEmpty
	public String getUsername() {
		return username;
	}

	/**
	 * 
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 
	 * @return password
	 */
	@NotNull
	@NotEmpty
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDtNasc() {
		return dtNasc;
	}

	public void setDtNasc(Date dtNasc) {
		this.dtNasc = dtNasc;
	}

	@Produces
	@Named("usuarioLogado")
	@SessionScoped
	@ProduceUsuarioLogado
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Boolean getRemember() {
		return remember;
	}

	public void setRemember(Boolean remember) {
		this.remember = remember;
	}

}
