package br.com.vle.controller.usuario.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.entidade.Usuario;
import br.com.vle.model.dto.UsuarioDTO;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

@Named
public class UsuarioRestBean implements UsuarioRest {

	@Inject
	private DaoUsuario dao;
	
	@Override
	public List<UsuarioDTO> todosUsuario() {
		List<Usuario> buscaPorNamedQuery = dao.buscaPorNamedQuery(Usuario.TOTAL);
		List<UsuarioDTO> lista = new ArrayList<UsuarioDTO>();
		for (Usuario u : buscaPorNamedQuery) {
			lista.add(new UsuarioDTO(u.getId(),""));
			//lista.add(new UsuarioDTO(u.getId(),u.getNome()));
		}
		return lista;
	}

}
