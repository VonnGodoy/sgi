package br.com.vle.controller.perfilacesso;

import br.com.vle.entidade.PerfilAcesso;

/**
 *
 * @author Victor Godoi
 */

public class Eventos {
	
	private PerfilAcesso perfilAcesso;
	private String nome;
	private String cpfCnpj;

	public PerfilAcesso getPerfilAcesso() {
		return perfilAcesso;
	}

	public void setPerfilAcesso(PerfilAcesso perfilAcesso) {
		this.perfilAcesso = perfilAcesso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String doc) {
		cpfCnpj = doc;
	}	

}
