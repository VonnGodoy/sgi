package br.com.vle.controller.imoveis;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.controller.perfilacesso.Eventos;
import br.com.vle.entidade.StatusImoveis;
import br.com.vle.enuns.StatusImoveisEnum;
import br.com.vle.enuns.TipoImovelEnum;
import br.com.vle.model.ModelEndereco;
import br.com.vle.model.ModelImoveis;
import br.com.vle.servico.dao.DaoEndereco;
import br.com.vle.servico.dao.DaoImoveis;
import br.com.vle.servico.dao.DaoStatusImoveis;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
//@URLMapping(id = "alterarcontroller", pattern = "/manter_usuario_alterar/#{alterarUsuarioController.id}/", viewId = "/page/manterusuario/manter_usuario_alterar.xhtml")
public class AlterarImoveisController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	private @Inject
	transient Logger logger;

	@Inject
	private DaoImoveis daoImoveis;

	@Inject
	private DaoEndereco daoEndereco;
	
	@Inject
	private DaoStatusImoveis daoStatusImoveis;

	@Inject
	private ModelImoveis model;

	@Inject
	private Conversation conversation;

	@Inject
	private Event<Eventos> events;

	
	@PostConstruct
	public void init() {
		logger.info("======================================");
		logger.info("==  Start Alterar Pessoa Controller  ==");
		logger.info("==  conversation = " + conversation.getId());
		logger.info("======================================");

		if (conversation.isTransient()) {
			logger.info("==  conversation begin             ==");
			conversation.begin();
		}

	}

	@PreDestroy
	public void destroi() {
		logger.info("=======================================");
		logger.info("===  Stop Alterar Pessoa Controller  ===");
		logger.info("=======================================");
	}

	public String alterar() {
		
		List <StatusImoveis> list =  daoStatusImoveis.buscarTudo();
		
		for(StatusImoveis status: list) {
			if(status.getId().toString().equals(model.getStatusImovel().getId())) {
				model.getImovelSelecionado().setStatusImovel(status);
				break;
			}
			
		}
		
		for(TipoImovelEnum tipo: model.getListatpImoveis()) {
			if(tipo.getId().toString().equals(model.getTpImovel().getId())) {
				model.getImovelSelecionado().setTpImovel(tipo.getId().toString());
				break;
			}
			
		}

		daoImoveis.update(model.getImovelSelecionado());

		messageInfo("Cadastro de Imovél Atualizao Com Sucesso");
		
		model.setImovelSelecionado(null);
		model.setStatusImovel(StatusImoveisEnum.SELECIONE);
		model.setTpImovel(TipoImovelEnum.SELECIONE);
		
		return redirect(Pages.MANTER_IMOVEIS);
		//return voltar(true);

	}
	
	public String buscarEndereco() {

		if (model.getImovelSelecionado().getCep().toString()!=null && model.getImovelSelecionado().getCep().toString().trim().length() == 8) {

			ModelEndereco endereco = daoEndereco.buscaEnderecoPorCep(model.getImovelSelecionado().getCep().toString());

			if(endereco!= null && endereco.getStatusConexao()) {

				messageErro("Erro ao Buscar CEP");
			}else if(endereco!= null){
				model.getImovelSelecionado().setLogradouro(endereco.getLogradouro());
				model.getImovelSelecionado().setComplemento(endereco.getComplemento());
				model.getImovelSelecionado().setBairro(endereco.getBairro());
				model.getImovelSelecionado().setLocalidade(endereco.getLocalidade());
				model.getImovelSelecionado().setUf(endereco.getUf());

			}else {
				messageErro("Cep Inválido");
			}
			
		}else {
			
			messageErro("O Campo CEP é Obrigatório");
		}
		return redirect(Pages.MANTER_IMOVEIS_ALTERAR);
	}

	public ModelImoveis getModel() {
		return model;
	}

	public void setModel(ModelImoveis model) {
		this.model = model;
	}


}
