package br.com.vle.controller.usuario;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.vle.entidade.Usuario;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.model.ModelUsuario;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

public class DataModelUsuario extends LazyDataModel<Usuario> implements Serializable{

	private static final long serialVersionUID = -3960618974569641885L;
	
    private List<Usuario> datasource;
    
    private int pageSize;
    
    private int rowIndex;
    
    private int rowCount;
    
   
    
    private DaoUsuario dao;
    
    private ModelUsuario modelUsuario;
    
    /**
     *
     * @param crudService
     */
    public DataModelUsuario(ModelUsuario model, DaoUsuario dao) {
        this.modelUsuario = model;
        this.dao = dao;
    }

    /**
     * Lazy loading user list with sorting ability
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<User>
     */ 
    @Override
    public List<Usuario> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	DTOFiltroBase filtro = new DTOFiltroBase();
    	filtro.setPrimeiroRegistroPage(first);
    	
    	if(modelUsuario.getQtdRows()!=null){
    		filtro.setQtdRegistroPaginacao(modelUsuario.getQtdRows().longValue());
    	}
    	
    	if (!sortOrder.equals(SortOrder.UNSORTED)) {
    		filtro.setOrdenacao(sortField, sortOrder.name());
		}
    	
    	if(modelUsuario.getCpfCnpj() != null && !"".equals(modelUsuario.getCpfCnpj())){
    		filtro.addFiltro("id", modelUsuario.getCpfCnpj().toString());
    	}
    	
    	if(modelUsuario.getStatus() != null && !modelUsuario.getStatus().equals(StatusEnum.SELECIONE)){
    		filtro.addFiltro("ativo", modelUsuario.getStatus().getStatus());
    	}
    	
    	if(modelUsuario.getPerfilUsuario() != null){
    		filtro.addFiltro("perfilAcesso", modelUsuario.getPerfilUsuario());
    	}
    	
    	
    	datasource = dao.buscarTodosUsuarios(filtro);
        setRowCount(dao.totalPerfisAcesso(filtro));   
        return datasource;
    }
    
    /**
     * Checks if the row is available
     * @return boolean
     */
    @Override
    public boolean isRowAvailable() {
        if(datasource == null) 
            return false;
        int index = rowIndex % pageSize ; 
        return index >= 0 && index < datasource.size();
    }
    
    /**
     * Gets the user object's primary key
     * @param user
     * @return Object
     */
    @Override
    public Object getRowKey(Usuario user) {
        return user.getId().toString();
    }

    /**
     * Returns the user object at the specified position in datasource.
     * @return 
     */
    @Override
    public Usuario getRowData() {
        if(datasource == null)
            return null;
        int index =  rowIndex % pageSize;
        if(index > datasource.size()){
            return null;
        }
        return datasource.get(index);
    }
    
    /**
     * Returns the user object that has the row key.
     * @param rowKey
     * @return 
     */
    @Override
    public Usuario getRowData(String rowKey) {
        if(datasource == null)
            return null;
       for(Usuario user : datasource) {  
           if(user.getId().toString().equals(rowKey))  
           return user;  
       }  
       return null;  
    }
    
    
    /**
     *
     * @param pageSize
     */
    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Returns page size
     * @return int
     */
    @Override
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Returns current row index
     * @return int
     */
    @Override
    public int getRowIndex() {
        return this.rowIndex;
    }
    
    /**
     * Sets row index
     * @param rowIndex
     */
    @Override
    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    /**
     * Sets row count
     * @param rowCount
     */
    @Override
    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }
    
    /**
     * Returns row count
     * @return int
     */
    @Override
    public int getRowCount() {
        return this.rowCount;
    }
     
    /**
     * Sets wrapped data
     * @param list
     */
    @SuppressWarnings("unchecked")
	@Override
    public void setWrappedData(Object list) {
        this.datasource = (List<Usuario>) list;
    }
    
    /**
     * Returns wrapped data
     * @return
     */
    @Override
    public Object getWrappedData() {
        return datasource;
    }
}
                    
