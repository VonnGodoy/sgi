package br.com.vle.controller.perfilacesso;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import br.com.vle.entidade.Role;
import br.com.vle.enuns.RolesEnum;

/**
 *
 * @author Victor Godoi
 */

public class ProducePerfilAcesso {

	@Produces
	@ApplicationScoped 
    @Named("roles")
	public List<Role> getRoles(){
		
		List<Role> lista = new ArrayList<Role>();
		for (RolesEnum role : RolesEnum.values()) {
			if(role.getPai()!=null){
				lista.add(new Role(role));
			}
		}
		
		
		return lista;
	}
	
	@Produces
	@ApplicationScoped 
	@Named("todasroles")
	public List<Role> getTodasRoles(){
		
		List<Role> lista = new ArrayList<Role>();
		for (RolesEnum role : RolesEnum.values()) {
			lista.add(new Role(role));
		}
		
		return lista;
	}
	
	@Produces
	@ApplicationScoped 
    @Named("rolesPai")
	public List<Role> getRolesPai(){
		
		List<Role> lista = new ArrayList<Role>();
		for (RolesEnum role : RolesEnum.values()) {
			if(role.getPai()==null){
				lista.add(new Role(role));
			}
		}
		
		return lista;
	}
	
	
//	@Produces
//	@ApplicationScoped 
//    @Named("todoTiposFuncao")
//	public TipoFuncionalidadeEnum[] getTabelaComplexidades(){
//		return TipoFuncionalidadeEnum.values();
//	}
	
//	@Produces
//	@ApplicationScoped 
//	@Named("tiposfuncao")
//	public TipoFuncionalidadeEnum[] getTiposFuncao(){
//		TipoFuncionalidadeEnum[] tipos = new TipoFuncionalidadeEnum[]{TipoFuncionalidadeEnum.EE,TipoFuncionalidadeEnum.CE,TipoFuncionalidadeEnum.SE};
//		return tipos;
//	}
	
//	@Produces
//	@ApplicationScoped 
//	@Named("tiposfuncaoDados")
//	public TipoFuncionalidadeEnum[] getTiposFuncaoDados(){
//		TipoFuncionalidadeEnum[] tipos = new TipoFuncionalidadeEnum[]{TipoFuncionalidadeEnum.AIE,TipoFuncionalidadeEnum.ALI};
//		return tipos;
//	}
	
//	@Produces
//	@ApplicationScoped 
//	@Named("nivelComplexidades")
//	public NivelComplexidade[] getNivelComplexidade(){
//		return NivelComplexidade.values();
//	}
	
}
