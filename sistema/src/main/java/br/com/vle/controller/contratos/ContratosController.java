package br.com.vle.controller.contratos;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.entidade.Contrato;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.enuns.PeriodosEnum;
import br.com.vle.enuns.ResponsavelDespesaEnum;
import br.com.vle.enuns.SituacaoContratoEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.enuns.TipoContratosEnum;
import br.com.vle.model.ModelContratos;
import br.com.vle.relatorios.contrato.ContratoAluguel;
import br.com.vle.relatorios.contrato.ListaDeContratos;
import br.com.vle.servico.dao.DaoContratos;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class ContratosController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;
	
    @Inject
	private Conversation conversation;

	@Inject
	private transient Logger logger;
	
	@Inject
	private ModelContratos model;
	
	@Inject
	private DaoContratos daoContratos;
	
	@Inject
	private ContratoAluguel rel;
	
	@Inject
	private ListaDeContratos listarContratos;
	
	@PostConstruct
	public void init() {
		logger.info("=====================================");
		logger.info("=====  Start Contratos Controller  =====");
		logger.info("=====================================");
		
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("=====  Stop Contratos Controller  =====");
		logger.info("====================================");
	}
	
	public String limpaConsulta(){ 
		
		model.setIdContrato("");
		model.setIdImovel("");
		model.setIdPessoa("");
		model.setContratoAtivo(StatusEnum.SELECIONE);
		model.setTpContrato(TipoContratosEnum.SELECIONE);
		
		return redirect(Pages.MANTER_CONTRATOS);
	}    
    
    public String novoContrato(){
    	
    	model.setContrato(new Contrato());

    	return redirect(Pages.MANTER_CONTRATOS_INCLUIR);
	
    }

    public String abrirRenovar(Contrato con){
    	
    	
    	model.setContratoSelecionado(con);
    	carregarCombos();
    	
    	return redirect(Pages.MANTER_CONTRATOS_RENOVAR);
    	
    }

    public String abrirDetalhar(Contrato con){
    	
    	model.setContratoSelecionado(con);
    	carregarCombos();
    	
    	return redirect(Pages.MANTER_CONTRATOS_DETALHAR);
    	
    }
    
    public String cancelarContrato(Contrato con) {
    	
    	con.setContratoStatus(SituacaoContratoEnum.CANCELADO.getId());
    	
    	daoContratos.update(con);
    	messageInfo("ContratoAluguel Cancelado com Sucesso");
    	
    	return redirect(Pages.MANTER_CONTRATOS);
    }
 
    public void prepara(){
    	
    	if(conversation.isTransient())
    		conversation.begin();
    	
    }
    
    
    public String gerarContrato() throws Exception {
    	
    	rel.gerar(model.getContratoSelecionado());
    	
    	messageInfo("Contrato Gerado Com Sucesso");
    	
    	return redirect(Pages.MANTER_CONTRATOS);
    }
    
    public String gerarRelatorioSelecionados() throws Exception {
    	
    	if(model.getContratosSelecionados().size() > 0) {
    	listarContratos.gerar(model.getContratosSelecionados());
    	
    	messageInfo("Lista De Contratos Gerada Com Sucesso");
    	}else {
    		messageErro("Nenhum Contrato Foi Selecionado");
    	}
    	return redirect(Pages.MANTER_CONTRATOS);
    }
    
    public String gerarRelatorioTodos() throws Exception {
    	
    	DTOFiltroBase filtro = new DTOFiltroBase();
    	
    	listarContratos.gerar(daoContratos.buscarTudo(filtro));
    	
    	messageInfo("Lista De Contratos Gerada Com Sucesso");
    	
    	return redirect(Pages.MANTER_CONTRATOS);
    }
    
    public void carregarCombos() {
    	
    PeriodosEnum[] per = PeriodosEnum.values();
    ResponsavelDespesaEnum[] res = ResponsavelDespesaEnum.values();
    
    for(PeriodosEnum periodo : per) {
    	
    	if(periodo.getNome().toString().equals(model.getContratoSelecionado().getValidade().toString())) {
    		
    		model.setValidade(periodo);
    		break;
    	}
    	
    }
    	
    for(ResponsavelDespesaEnum r : res) {
    	
    	if(r.getId().toString().equals(model.getContratoSelecionado().getRespDespesas().toString())) {
    		
    		model.setResponsavel(r);
    		
    		break;
    	}
    	
    	
    }
    	
    }
    
    
}
