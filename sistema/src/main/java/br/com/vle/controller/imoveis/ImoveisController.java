package br.com.vle.controller.imoveis;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.entidade.Imoveis;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.enuns.StatusImoveisEnum;
import br.com.vle.enuns.TipoImovelEnum;
import br.com.vle.model.ModelImoveis;
import br.com.vle.relatorios.imoveis.ListaImoveis;
import br.com.vle.servico.dao.DaoImoveis;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class ImoveisController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;
	
    @Inject
	private Conversation conversation;

	@Inject
	private transient Logger logger;
	
	@Inject
	private ModelImoveis model;
	
	@Inject
	private DaoImoveis dao;
	
	@Inject 
	private ListaImoveis listaImoveis;
	
	
	
	@PostConstruct
	public void init() {
		logger.info("=====================================");
		logger.info("=====  Start Imoveis Controller  =====");
		logger.info("=====================================");
		
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("=====  Stop Imoveis Controller  =====");
		logger.info("====================================");
	}
	
	public StatusEnum[] getStatus(){
		return StatusEnum.values();
	}
	
	public String limpaConsulta(ActionEvent actionEvent){ 
		model.setCep("");
		model.setStatusImovel(StatusImoveisEnum.SELECIONE);
		model.setTpImovel(TipoImovelEnum.SELECIONE);
		
		return redirect(Pages.MANTER_IMOVEIS);
	}
    
    /**
     * @param imoveis
     */
    public String selecionar(){
    	
    	if(model.getImovelSelecionado() == null){
    		
    		messageErro("imoveis_msg_selecionar");
    		return null;
    	}
    	
    	return voltar();
    }
    
    
    public String novoImovel(){
    	
    	return redirect(Pages.MANTER_IMOVEIS_INCLUIR);
	
    }
    
    public String fechaDetalhe(){
    	
    	model.setCep("");
    	model.setStatusImovel(StatusImoveisEnum.SELECIONE);
    	model.setTpImovel(TipoImovelEnum.SELECIONE);
    	
    	return redirect(Pages.MANTER_IMOVEIS);
	
    }
    
    public String voltar(boolean retorno) {
    	
    	model.setCep(null);
    	model.setStatusImovel(StatusImoveisEnum.SELECIONE);
    	model.setTpImovel(TipoImovelEnum.SELECIONE);
    	
    	return voltar(retorno);
    }
    
    
    /**
     * @param imoveis
     */
    public String abrirAlterar(Imoveis imovel){
    	
    	prenchercombo(imovel);
    	
    	model.setImovelSelecionado(imovel);
    	return redirect(Pages.MANTER_IMOVEIS_ALTERAR);
    	
    }
    
    /**
     * @param usuario
     */
    public String abrirDetalhar(Imoveis imovel){
    	
    	prenchercombo(imovel);
    	
    	model.setImovelSelecionado(imovel);
    	return redirect(Pages.MANTER_IMOVEIS_DETALHAR);
    	
    }
 
    public void prepara(){
    	
    	if(conversation.isTransient())
    		conversation.begin();
    	
    }

    public String doLimparConsulta() {
    	
    	model.setCep(null);
    	model.setStatusImovel(StatusImoveisEnum.SELECIONE);
    	model.setTpImovel(TipoImovelEnum.SELECIONE);
    	
    	
    	return redirect(Pages.MANTER_IMOVEIS);
    }

	
	/**
	 * 
	 * @param actionEvent
	 */
	public String delete(Imoveis imovel) {
		
		for (TipoImovelEnum tipo : model.getListatpImoveis()) {

			if (tipo.getTipo().equals(imovel.getTpImovel())) {
				imovel.setTpImovel(tipo.getId());
				break;
			}
		}
		
		dao.delete(imovel);
		messageInfo("Excluido com sucesso.");
		
    	return redirect(Pages.MANTER_IMOVEIS);
    }

	public void prenchercombo(Imoveis imovel) {
		for (StatusImoveisEnum status : model.getListaStatusImoveis()) {

			if (status.getId().equals(imovel.getStatusImovel().getId())) {
				model.setStatusImovel(status);
				break;
			}
		}

		for (TipoImovelEnum tipo : model.getListatpImoveis()) {

			if (tipo.getTipo().equals(imovel.getTpImovel())) {
				model.setTpImovel(tipo);
				break;
			}
		}
	}
	
	public String gerarRelatorioSelecionados() throws Exception {
		
    	if(model.getImoveis().size() > 0) {
    	listaImoveis.gerar(model.getImoveis());
    	
    	messageInfo("Relatorio De Imoveis Gerada Com Sucesso");
    	}else {
    		messageErro("Nenhum Imovel Foi Selecionado");
    	}
		
	return redirect(Pages.MANTER_IMOVEIS);
	}
	
	public String gerarRelatorioTodos() throws Exception {
		
		DTOFiltroBase filtro = new DTOFiltroBase();
    	
    	listaImoveis.gerar(dao.buscarTudo(filtro));
    	
    	messageInfo("Relatorio De Imoveis Gerada Com Sucesso");
		
	return redirect(Pages.MANTER_IMOVEIS);
	}
}
