package br.com.vle.controller.imoveis;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.entidade.Imoveis;
import br.com.vle.entidade.StatusImoveis;
import br.com.vle.enuns.StatusImoveisEnum;
import br.com.vle.enuns.TipoImovelEnum;
import br.com.vle.model.ModelEndereco;
import br.com.vle.model.ModelImoveis;
import br.com.vle.servico.dao.DaoEndereco;
import br.com.vle.servico.dao.DaoImoveis;

/**
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class IncluirImoveisController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	private @Inject
	transient Logger logger;

	@Inject	
	private DaoImoveis daoImoveis;
	
	@Inject	
	private DaoEndereco daoEndereco;
	
	@Inject
	private ModelImoveis model;
	 
	@Inject
	private Conversation conversation;

	/**
	 * Initializing Data Access Service for LazyImoveisDataModel class role list
	 * for ImoveisContoller class
	 */
	@PostConstruct
	public void init() {
		logger.info("=====================================");
		logger.info("==  Start Incluir Imovel Controller  ==");
		logger.info("==  conversation = " + conversation.getId());
		logger.info("=====================================");
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("======================================");
		logger.info("===  Stop Incluir Imovel Controller  ===");
		logger.info("======================================");
	}


	
	public String salvar() {


		logger.info("=======================================");
		logger.info("==========  Salvando Imovel  ==========");
		logger.info("=======================================");
		
		StatusImoveis status = new StatusImoveis();
		
		status.setId(model.getStatusImovel().getId().toString());
		status.setStatus(model.getStatusImovel().getStatus().toString());
		
		model.getImovel().setTpImovel(model.getTpImovel().getId().toString());
		model.getImovel().setStatusImovel(status);
		
		daoImoveis.create(model.getImovel());
				
		messageInfo("imoveis_salvar_sucesso");
		
		model.setImovelSelecionado(null);
		model.setStatusImovel(StatusImoveisEnum.SELECIONE);
		model.setTpImovel(TipoImovelEnum.SELECIONE);
		
		return voltar(true);

	}

		public void buscarEndereco() {

		if (model.getImovel().getCep().toString()!=null && model.getImovel().getCep().toString().trim().length() == 8) {

			ModelEndereco endereco = daoEndereco.buscaEnderecoPorCep(model.getImovel().getCep().toString());
			
			if(endereco!= null && endereco.getStatusConexao()) {

				messageErro("Erro ao Buscar CEP");
			}else if(endereco!= null){
				model.getImovel().setLogradouro(endereco.getLogradouro());
				model.getImovel().setComplemento(endereco.getComplemento());
				model.getImovel().setBairro(endereco.getBairro());
				model.getImovel().setLocalidade(endereco.getLocalidade());
				model.getImovel().setUf(endereco.getUf());

			}else {
				messageErro("Cep Inválido");
			}
			
		}else {
			
			messageErro("O Campo CEP é Obrigatório");
		}

	}
	
	public ModelImoveis getModel() {
		return model;
	}

	public void setModel(ModelImoveis model) {
		this.model = model;
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

}
