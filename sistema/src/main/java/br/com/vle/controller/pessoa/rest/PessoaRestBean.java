package br.com.vle.controller.pessoa.rest;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.shade.org.apache.commons.beanutils.BeanUtils;

import br.com.vle.entidade.Pessoa;
import br.com.vle.model.dto.PessoaDTO;
import br.com.vle.servico.dao.DaoPessoa;

/**
 *
 * @author Victor Godoi
 */

@Named
public class PessoaRestBean implements PessoaRest {

	@Inject
	private DaoPessoa dao;
	
	@Override
	public List<PessoaDTO> todos() {
		List<Pessoa> buscaPorNamedQuery = dao.buscaPorNamedQuery(Pessoa.ALL);
		List<PessoaDTO> lista = new ArrayList<PessoaDTO>();
		for (Pessoa u : buscaPorNamedQuery) {
			lista.addAll((List<? extends PessoaDTO>)u);
		}
		return lista;
	}
	
	@Override
	public PessoaDTO porDocumento(String CpfCnpj) {
		String query = "select u FROM Pessoa u.cpf_cnpj = "+CpfCnpj+"";
		List<Pessoa> buscaPorNamedQuery = dao.buscaPorNamedQuery(query);
		PessoaDTO dto = new PessoaDTO();
		for (Pessoa u : buscaPorNamedQuery) {
			try {
				BeanUtils.copyProperties(dto, u);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return dto;
	}

}
