package br.com.vle.controller.perfilacesso;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vle.comum.ControllerBase;
import br.com.vle.entidade.Role;
import br.com.vle.model.ModelPerfil;
import br.com.vle.servico.dao.DaoPerfilAcesso;

/**
 *
 * @author Victor Godoi
 */

@SuppressWarnings("serial")
@Named
@RequestScoped
public class AlterarPerfilController extends ControllerBase {


	private static final Logger logger = LoggerFactory.getLogger(AlterarPerfilController.class);
	
	@Inject
	private DaoPerfilAcesso servicePerfilAcesso;
	
	@Inject
	private ModelPerfil model;

	/**
	 * Initializing Data Access Service for LazyUserDataModel class role list
	 * for UserContoller class
	 */
	@PostConstruct
	public void init() {
		if(logger.isInfoEnabled()){
			logger.info("=====================================");
			logger.info("==  Start AlterarPerfilController  ==");
			logger.info("==  model = " + model.getNomePerfilConsulta());
			logger.info("=====================================");
		}
	}

	@PreDestroy
	public void destroi() {
		if(logger.isInfoEnabled()){
			logger.info("====================================");
			logger.info("==  Stop AlterarPerfilController  ==");
			logger.info("====================================");
		}
	}

	
	
	public String alterar(){
		
		List<Role> rolespais  = new ArrayList<Role>();
		List<Role> selecionados =  model.getPerfilSelecionado().getRoles();
		
		for (Role role :selecionados) {
			
			Role rolepai = new Role(role.getId(), role.getRolePai());
			if(!rolespais.contains(rolepai)){
				rolespais.add(rolepai);
			}
		}
		model.getPerfilSelecionado().getRoles().addAll(rolespais);
		
		servicePerfilAcesso.update(model.getPerfilSelecionado());
		messageInfo("Perfil Alterado com sucesso");
		return voltar(true);
	}
	
	
}
