package br.com.vle.controller.contratos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.entidade.Financeiro;
import br.com.vle.enuns.MesesEnum;
import br.com.vle.enuns.SituacaoContratoEnum;
import br.com.vle.enuns.SituacaoFinancasEnum;
import br.com.vle.model.ModelContratos;
import br.com.vle.servico.dao.DaoContratos;
import br.com.vle.servico.dao.DaoFinanceiro;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class RenovarContratosController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	private @Inject
	transient Logger logger;

	@Inject
	private DaoContratos daoContratos;
	
	@Inject
	private DaoFinanceiro daoFinanceiro;

	@Inject
	private ModelContratos model;

	@Inject
	private Conversation conversation;

	
	@PostConstruct
	public void init() {
		logger.info("======================================");
		logger.info("==  Start Renovar Contratos Controller  ==");
		logger.info("==  conversation = " + conversation.getId());
		logger.info("======================================");

		if (conversation.isTransient()) {
			logger.info("==  conversation begin             ==");
			conversation.begin();
		}

	}

	@PreDestroy
	public void destroi() {
		logger.info("=======================================");
		logger.info("===  Stop Renovar Contratos Controller  ===");
		logger.info("=======================================");
	}

	public String renovar() {
		
		model.getContratoSelecionado().setValidade(model.getValidade().getNome());
		model.getContratoSelecionado().setPeriodo(model.getDtIni()+" A "+ model.getDtFim());
		model.getContratoSelecionado().setRespDespesas(model.getResponsavel().getId());
		model.getContratoSelecionado().setContratoStatus(SituacaoContratoEnum.VIGENTE.getId().toString());
		model.getContratoSelecionado().setTpcontrato(model.getContratoSelecionado().getTpcontrato());
		
		gerarFinanceiro();

		daoContratos.update(model.getContratoSelecionado());

		messageInfo("Contrato Renovado Com Sucesso");
		
		model.setContratoSelecionado(null);
		model.setImovelSelecionado(null);
		model.setPessoaSelecionada(null);

		
		return voltar(true);

	}
	
	public void gerarFinanceiro() {

		MesesEnum[] meses = MesesEnum.values();
		List<Financeiro> mensalidades = new ArrayList();
		Financeiro mensalidade = null;
		String mesInicio = model.getDtIni().toString().substring(0,3);
		int ano = Integer.valueOf(model.getDtIni().toString().substring(5));
		String prox ="";

		for (int i = 0 ; i < model.getValidade().getParcelas(); i++) {
			mensalidade = new Financeiro();
			
			mensalidade.setContrato(model.getContratoSelecionado());
			mensalidade.setImovel(model.getContratoSelecionado().getImovel());
			mensalidade.setPessoa(model.getContratoSelecionado().getPessoa());
			mensalidade.setDiaVencimento(model.getContratoSelecionado().getDiaVencimento());
			mensalidade.setStatus(SituacaoFinancasEnum.PENDENTE.getId());
			mensalidade.setTxMultaAtraso(model.getContratoSelecionado().getTxMultaAtraso());
			mensalidade.setValorMensalidade(model.getContratoSelecionado().getValorMensalidade());
			
			for(MesesEnum mes : meses) {
				
				if(mes.getNome().toString().equals(mesInicio)) {
					mensalidade.setMes(mes.getNome()+"-"+ano);
					mensalidades.add(mensalidade);
					mesInicio = "";
					prox = mes.getProx();
					break;
				}
				
			if (mes.getNome().toString().equals(prox)) {
				
				mensalidade.setMes(mes.getNome()+"-"+ano);
				mensalidades.add(mensalidade);
				prox = mes.getProx();
				
				if(prox.toString().equals("JAN")) {
					ano = ano + 1;
				}
				break;
			}
		}

		}
		
		daoFinanceiro.create(mensalidades);
	}

	public ModelContratos getModel() {
		return model;
	}

	public void setModel(ModelContratos model) {
		this.model = model;
	}


}
