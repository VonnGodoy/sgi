package br.com.vle.controller.financeiro;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.enuns.ResponsavelDespesaEnum;
import br.com.vle.enuns.SituacaoFinancasEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.enuns.TipoContratosEnum;
import br.com.vle.model.ModelFinanceiro;
import br.com.vle.servico.dao.DaoFinanceiro;

/**
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class LancamentosFinanceiroController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	private @Inject
	transient Logger logger;

	@Inject	
	private DaoFinanceiro daoFinanceiro;

	@Inject
	private ModelFinanceiro model;

	@Inject
	private Conversation conversation;

	/**
	 * Initializing Data Access Service for LazyImoveisDataModel class role list
	 * for ImoveisContoller class
	 */
	@PostConstruct
	public void init() {
		logger.info("=========================================");
		logger.info("==  Start Lancamento Financeiro Controller  ==");
		logger.info("==  conversation = " + conversation.getId());
		logger.info("=========================================");

	}

	@PreDestroy
	public void destroi(){
		logger.info("===========================================");
		logger.info("===  Stop Lancamento Financeiro Controller  ===");
		logger.info("===========================================");
	}

	public void duraçao() {

	}

	public String salvar() {
		Pages retorno = null;
		
		long Var = model.getValorReceber().longValue();
		long Vrec = model.getValorRecebido().longValue();	
		
			if(Vrec >= Var) {
				
				model.getSelecionado().setTotal_mes(model.getValorRecebido());
				model.getSelecionado().setStatus(SituacaoFinancasEnum.PAGO.getStatus().toString());
				
				try {
				daoFinanceiro.update(model.getSelecionado());
				
				messageInfo("Lancamento Efetuado com Sucesso");
				
				retorno = Pages.FINANCEIRO;
				}catch(Exception e){
					messageErro("Erro ao Efetuar Lancamento");
					retorno = Pages.FINANCEIRO_LANCAMENTOS;
				}
			}else {
				messageWarn("Valor Recebido Deve Ser Maior ou Igual o  Valor a Receber");
				retorno = Pages.FINANCEIRO_LANCAMENTOS;
			}

			return redirect(retorno);

	}

	public String voltar() {

		model.setIdContrato("");
		model.setIdImovel("");
		model.setIdPessoa("");
		model.setContrato(null);
		model.setImovelSelecionado(null);
		model.setPessoaSelecionada(null);
		model.setContratoAtivo(StatusEnum.SELECIONE);
		model.setTpContrato(TipoContratosEnum.SELECIONE);
		model.setResponsavel(ResponsavelDespesaEnum.SELECIONE);

		return redirect(Pages.FINANCEIRO);

	}

	public ModelFinanceiro getModel() {
		return model;
	}

	public void setModel(ModelFinanceiro model) {
		this.model = model;
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

}
