package br.com.vle.controller.login;

import java.security.acl.Group;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;

import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vle.entidade.Role;
import br.com.vle.entidade.Usuario;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

public class UserAndPass  extends UsernamePasswordLoginModule {
	
	private static final Logger log = LoggerFactory.getLogger(UserAndPass.class);
	
	@Inject
	private DaoUsuario daoUsuario;
	private List<Role> roles;

    @SuppressWarnings("rawtypes")
    public void initialize(Subject subject, CallbackHandler callbackHandler,
            Map sharedState,
            Map options) {
        super.initialize(subject, callbackHandler, sharedState, options);
    }


    @Override
    protected String getUsersPassword() throws LoginException {
    	String password = null;
    	Usuario usuario = null;
    	
		if(daoUsuario == null){
			InitialContext ic;
			try {
				ic = new InitialContext();
				daoUsuario = (DaoUsuario) ic.lookup("java:module/DaoUsuario");
			} catch (NamingException e) {
				log.error(e.getMessage(), e);
			}
		}
		usuario = daoUsuario.find(super.getUsername());
    		
    	
    	if(usuario == null){
    		throw new LoginException("Usuário não existe!");
    	}
    	
    	password = usuario.getPassword();
    	roles = usuario.getPerfilAcesso().getRoles();
        
        return password;
    }

    @Override
    protected boolean validatePassword(String inputPassword, String expectedPassword) {
        return super.validatePassword(inputPassword, expectedPassword);
    }

    @Override
    protected Group[] getRoleSets() throws LoginException {
        SimpleGroup group = new SimpleGroup("Roles");
        group.addMember(new SimplePrincipal("user"));
        for (Role role : roles) {
        	group.addMember(new SimplePrincipal(role.getRole()));
        }
        return new Group[] { group };
    }

}
