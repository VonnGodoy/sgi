package br.com.vle.controller.pessoa;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.controller.perfilacesso.Eventos;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Usuario;
import br.com.vle.enuns.EstadosEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.model.ModelPessoa;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;
import br.com.vle.util.ValidatorUtils;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class PessoaController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;
	
    @Inject
	private Conversation conversation;

	@Inject
	private transient Logger logger;
	
	@Inject
	private ModelPessoa modelPessoa;
	
	@Inject
	private DaoUsuario daoUsuario;
	
	@Inject
	private DaoPessoa daoPessoa;
	
	@Inject 
	private Event<Eventos> events;
	
	@PostConstruct
	public void init() {
		logger.info("=====================================");
		logger.info("=====  Start Pessoa Controller  =====");
		logger.info("=====================================");
		
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("=====  Stop Pessoa Controller  =====");
		logger.info("====================================");
	}
	
	public StatusEnum[] getStatus(){
		return StatusEnum.values();
	}

	public void limpaConsulta(ActionEvent actionEvent){ 
		modelPessoa.setCpfCnpj(null);
		modelPessoa.setNome(null);
		modelPessoa.setStatus(null);
		modelPessoa.setPerfilUsuario(null);
	}
    
    /**
     * @return
     */
    public String novaPessoa(){
    			
    			return redirect(Pages.MANTER_PESSOA_INCLUIR);
    			
    }
    
    /**
     * @param pessoa
     */
    public String selecionar(){
    	
    	if(modelPessoa.getPessoaSelecionado() == null){
    		messageErro("pessoa_msg_selecionar");
    		return null;
    	}
    	
    	return voltar();
    }
    
    /**
     * @param pessoa
     */
    public String abrirAlterar(Pessoa pessoa){
    	
    	EstadosEnum[] estados = EstadosEnum.values();
    	
    	for (EstadosEnum estado: estados) {
    		
    		if(estado.getUf().toString().equals(pessoa.getSsp())) {
    			modelPessoa.setEstado(estado);
    			break;
    		}
    	}
    	
    	modelPessoa.setPessoaSelecionado(pessoa);
    	modelPessoa.setUsuarioSelecionado(buscaUsuario(pessoa.getCpfCnpj().toString()));
    	modelPessoa.setPerfilUsuario(modelPessoa.getUsuarioSelecionado().getPerfilAcesso());
    	
    	return redirect(Pages.MANTER_PESSOA_ALTERAR);
    	
    }
 
    public void prepara(){
    	
    	if(conversation.isTransient())
    		conversation.begin();
    	
    }


	/**
	 * @param pessoa
	 */
	public String ativarUsuario(Pessoa pessoa) {
		
		Usuario usu  = buscaUsuario(pessoa.getCpfCnpj().toString());
		
		daoPessoa.desbloqueio(pessoa);
		daoUsuario.desbloqueio(usu);
		messageInfo("Pessoa ativada com sucesso.");
		return redirect(Pages.MANTER_PESSOA);
	}
	
	/**
	 * @param usuario
	 */
	public String inativarUsuario(Pessoa pessoa) {
		
		Usuario usu  = buscaUsuario(pessoa.getCpfCnpj().toString());
		
		daoPessoa.bloqueio(pessoa);
		daoUsuario.bloqueio(usu);
		messageInfo("Pessoa bloqueado com sucesso.");
		return redirect(Pages.MANTER_PESSOA);
	}
	
	/**
	 * 
	 * @param actionEvent
	 */
	public String inativarSelecionados(AjaxBehaviorEvent event) {
		
		if(modelPessoa.getSelecionados() == null || modelPessoa.getSelecionados().length <= 0){
			messageWarn("Nenhum registro foi selecionado.");
			return redirect(Pages.MANTER_PESSOA);
		}
		
		
		listarUsuarios();
		
		int qtd = modelPessoa.getUsuariosSelecionados().size();
		daoUsuario.bloqueio(modelPessoa.getUsuariosSelecionados());
		messageInfo("Foi(ram) Bloqueado(s) "+qtd+" registro(s) com sucesso.");
		
		return redirect(Pages.MANTER_PESSOA);
		
	}
	
	/**
	 * 
	 * @param actionEvent
	 */
	public String ativarSelecionados(AjaxBehaviorEvent event) {
		
		if(modelPessoa.getSelecionados() == null || modelPessoa.getSelecionados().length <= 0){
			messageWarn("Nenhum registro foi selecionado.");
			return redirect(Pages.MANTER_PESSOA);
		}
		
		listarUsuarios();
		
		int qtd = modelPessoa.getUsuariosSelecionados().size();
		daoUsuario.desbloqueio(modelPessoa.getUsuariosSelecionados());
		messageInfo("Foi(ram) Bloqueado(s) "+qtd+" registro(s) com sucesso.");
		
		return redirect(Pages.MANTER_PESSOA);
		
	} 
	
	public void listarUsuarios(){
		
		List <Usuario> usuarios = new ArrayList();		
	
		for(Pessoa pessoa:modelPessoa.getSelecionados()) {
			
			for(Usuario usuario: modelPessoa.getUsuarios()) {
				
				if(pessoa.getCpfCnpj().toString().equals(usuario.getId().toString())) {
					usuarios.add(usuario);
				}
			}
		}
		modelPessoa.setUsuariosSelecionados(usuarios);
	}
	
	public Usuario buscaUsuario(String id) {
		
		Usuario usu = null;
		
		for(Usuario usuario :modelPessoa.getUsuarios()) {
			
			if(usuario.getId().trim().length()<11 || usuario.getId().trim().length()>11 || usuario.getId().trim().length()<14) {
				usuario.setId(adicionaZero(usuario.getId()));
			}
			
			if(usuario.getId().toString().equals(id)) {
				usu = usuario;
				break;
			}
		}
		
		return usu;
	}
	
	public StatusEnum[] getListaStatus(){
		return StatusEnum.values();
	}
	
	public String adicionaZero(String cpfCnpj){
	
		while(cpfCnpj.trim().length() <= 10) {
			cpfCnpj = "0" + cpfCnpj.toString();
			
		}
		
		while(cpfCnpj.trim().length() > 11 && cpfCnpj.trim().length() <=13) {
			cpfCnpj = "0" + cpfCnpj.toString();
			
		}
		
		return cpfCnpj;
	}

}
