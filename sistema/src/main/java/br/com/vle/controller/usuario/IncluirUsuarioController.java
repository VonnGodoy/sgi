package br.com.vle.controller.usuario;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Usuario;
import br.com.vle.enuns.EstadosEnum;
import br.com.vle.model.ModelEndereco;
import br.com.vle.model.ModelUsuario;
import br.com.vle.servico.dao.DaoEndereco;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class IncluirUsuarioController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	@Inject 
	private transient Logger logger;

	@Inject
	private DaoUsuario daoUsuario;

	@Inject
	private DaoPessoa daoPessoa;
	
	@Inject
	private DaoEndereco daoEndereco;

	@Inject
	private ModelUsuario modelUsuario;

	@Inject
	private Conversation conversation;

	private EstadosEnum estado;
	
	private EstadosEnum[] estados(){
		return EstadosEnum.values();	
	}


	/**
	 * Initializing Data Access Service for LazyUserDataModel class role list for
	 * UserContoller class
	 */
	@PostConstruct
	public void init() {
		logger.info("=====================================");
		logger.info("=  Start IncluirUsuarioController  ==");
		logger.info("=  conversation = " + conversation.getId());
		logger.info("=====================================");
	}

	@PreDestroy
	public void destroi() {
		logger.info("=====================================");
		logger.info("=  Stop IncluirUsuarioController  ===");
		logger.info("=====================================");
	}
	
    public void prepara(){
    	
    	if(conversation.isTransient())
    		conversation.begin();
    	
    }

	public String salvar() {

		logger.info("nome atual = " + modelUsuario.getNome().toString());
		logger.info("Perfil atual = " + modelUsuario.getPerfilUsuario().getPerfil().toString());
		
		/**
		 * Salvar pessoa
		 */
		modelUsuario.setPessoa(new Pessoa());
		modelUsuario.getPessoa().setNome(modelUsuario.getNome().toString());
		modelUsuario.getPessoa().setEmail(modelUsuario.getEmail().toString());
		modelUsuario.getPessoa().setCpfCnpj(modelUsuario.getCpfCnpj().toString());
		modelUsuario.getPessoa().setRg(modelUsuario.getRg().toString());
		modelUsuario.getPessoa().setIe(modelUsuario.getIe());
		modelUsuario.getPessoa().setSsp(modelUsuario.getEstado().getUf());
		modelUsuario.getPessoa().setDtNascimento(modelUsuario.getDtNascimento());
		modelUsuario.getPessoa().setTelPrin(modelUsuario.getTelPrin().toString());
		modelUsuario.getPessoa().setTelSec(modelUsuario.getTelSec().toString());
		modelUsuario.getPessoa().setCep(modelUsuario.getCep().toString());
		modelUsuario.getPessoa().setLogradouro(modelUsuario.getLogradouro().toString());
		modelUsuario.getPessoa().setComplemento(modelUsuario.getComplemento().toString());
		modelUsuario.getPessoa().setNumero(modelUsuario.getNumero().toString());
		modelUsuario.getPessoa().setBairro(modelUsuario.getBairro().toString());
		modelUsuario.getPessoa().setLocalidade(modelUsuario.getLocalidade().toString());
		modelUsuario.getPessoa().setUf(modelUsuario.getUf().toString());
		modelUsuario.getPessoa().setRefNome(modelUsuario.getNomeRef().toString());
		modelUsuario.getPessoa().setRefTel(modelUsuario.getTelRef().toString());
		modelUsuario.getPessoa().setRefNome1(modelUsuario.getNomeRef1().toString());
		modelUsuario.getPessoa().setRefTel1(modelUsuario.getTelRef1().toString());
		modelUsuario.getPessoa().setDtCadastro(new Date());
		
		daoPessoa.create(modelUsuario.getPessoa());
		
		/**
		 * Salvar usuario
		 */
		modelUsuario.setNewUsuario(new Usuario());
		modelUsuario.getNewUsuario().setId(modelUsuario.getCpfCnpj().toString());
		modelUsuario.getNewUsuario().setPerfilAcesso(modelUsuario.getPerfilUsuario());
		modelUsuario.getNewUsuario().setAtivo(modelUsuario.getStatus().getStatus());
		modelUsuario.getNewUsuario().setPrimeiroAcesso(true);
		
		String value = modelUsuario.getCpfCnpj().toString().substring(0,6);
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			byte[] hash = messageDigest.digest(value.getBytes("UTF-8"));
			StringBuilder stringBuilder=  new StringBuilder();
			for (int i = 0; i < hash.length; i++) {
				stringBuilder.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
			}
			modelUsuario.getNewUsuario().setPassword(stringBuilder.toString());
		} catch (NoSuchAlgorithmException e) {
			
			messageErro("Erro ao Gerar Senha de usuario");
			modelUsuario.getNewUsuario().setPassword(null);
		} catch (UnsupportedEncodingException e) {
			
			messageErro("Erro ao Gerar Senha de usuario encoding errado");
			modelUsuario.getNewUsuario().setPassword(null);
		}
		
		daoUsuario.create(modelUsuario.getNewUsuario());
		
		
		modelUsuario.setNewUsuario(new Usuario());
		modelUsuario.setPessoa(new Pessoa());
		
		messageInfo("usuario_salvar_sucesso");

		return voltar(true);

	}
	

	public void buscarEndereco() {

		if (modelUsuario.getCep().toString()!=null && modelUsuario.getCep().toString().trim().length() == 8) {

			ModelEndereco endereco = daoEndereco.buscaEnderecoPorCep(modelUsuario.getCep().toString());

			if(endereco.getStatusConexao()) {

				messageErro("Erro ao Buscar CEP");
			}else {
				modelUsuario.setLogradouro(endereco.getLogradouro());
				modelUsuario.setComplemento(endereco.getComplemento());
				modelUsuario.setBairro(endereco.getBairro());
				modelUsuario.setLocalidade(endereco.getLocalidade());
				modelUsuario.setUf(endereco.getUf());

			}
			
		}else {
			
			messageErro("O Campo CEP é Obrigatório");
		}

		//return redirect(Pages.MANTER_USUARIO_INCLUIR);

	}

	public void validaDocumento() {

	}

	/**
	 * @return
	 */
//	public String doBuscarPerfil() {
//
//	modelUsuario.getNewUsuario().setId((String) modelUsuario.getPessoa().getCpfCnpj());
//
//		logger.info("doBuscarPerfil atual = " + perfilSelecionado);
//		return redirect(Pages.MANTER_PERFIL_SELECAO);
//	}

	public ModelUsuario getModelUsuario() {
		return modelUsuario;
	}

	public void setModelUsuario(ModelUsuario model) {
		this.modelUsuario = model;
	}

	public EstadosEnum[] getEstados() {
		return estados();
	}

	public EstadosEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadosEnum estado) {
		this.estado = estado;
	}

}
