package br.com.vle.controller.usuario;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.controller.perfilacesso.Eventos;
import br.com.vle.entidade.Usuario;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.model.ModelUsuario;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;
import br.com.vle.util.ValidatorUtils;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class UsuarioController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;
	
    @Inject
	private Conversation conversation;

	@Inject
	private transient Logger logger;
	
	@Inject
	private ModelUsuario modelUsuario;

	@Inject
	private ModelUsuario modelPessoa;
	
	@Inject
	private DaoUsuario daoUsuario;
	
	@Inject
	private DaoPessoa daoPessoa;
	
	@Inject 
	private Event<Eventos> events;
	
	@PostConstruct
	public void init() {
		logger.info("=====================================");
		logger.info("=====  Start UsuarioController  =====");
		logger.info("=====================================");
		
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("=====  Stop UsuarioController  =====");
		logger.info("====================================");
	}
	
	public StatusEnum[] getListaStatus(){
		return StatusEnum.values();
	}

	public void doLimpaConsulta(ActionEvent actionEvent){ 

		modelUsuario.setPerfilUsuario(null);
		modelUsuario.setStatus(null);
		modelUsuario.setCpfCnpj(null);
		modelUsuario.setNome(null);
		
	}
    
    /**
     * @return
     */
	public String criarUsuario() {
		
		if (validaPernchimento()) {

			if (ValidatorUtils.isValidDocument(modelUsuario.getCpfCnpj())) {
				Eventos e = new Eventos();
				e.setPerfilAcesso(modelUsuario.getPerfilUsuario());
				events.fire(e);
				return redirect(Pages.MANTER_USUARIO_INCLUIR);
			} else {
				messageErro("CPF/CNPJ Inválido.");
				return redirect(Pages.MANTER_USUARIO);
			}
			
		} else {
			return redirect(Pages.MANTER_USUARIO);
		}	

	}
    
	public boolean validaPernchimento() {
		boolean erro = true;
	
		if(modelUsuario.getCpfCnpj() == null){
			
			erro = false;
			messageErro("O Campo CPF/CNPJ é de Preenchimento Obrigatório");
		}
		
		if(modelUsuario.getNome() == null) {
			
			erro = false;
			messageErro("O Campo Nome é de Preenchimento Obrigatório");
		}
		
		if(modelUsuario.getPerfilUsuario() == null){
			
			erro = false;
			messageErro("O Campo Perfil é de Preenchimento Obrigatório");
		}
		if(modelUsuario.getStatus() == null || modelUsuario.getStatus().getStatus() == null) {
			
			erro = false;
			messageErro("O Campo Status é de Preenchimento Obrigatório");
		}

		return erro;
	}
    
    /**
     * @param usuario
     */
    public String abrirAlterar(Usuario usuario){
    	
    	modelUsuario.setUsuarioSelecionado(usuario);
    	Eventos e = new Eventos();
		e.setPerfilAcesso(usuario.getPerfilAcesso());
		events.fire(e);
    	return redirect(Pages.MANTER_USUARIO_ALTERAR);
    	
    }
 
    public void prepara(){
    	
    	if(conversation.isTransient())
    		conversation.begin();
    	
    }

	/**
	 * @param usuario
	 */
	public void doUnDelete(Usuario usuario) {
		daoUsuario.desbloqueio(usuario);
		//messageInfo("Usuário "+usuario.getNome()+" foi excluido com sucesso.");
	}
	
	/**
	 * @param usuario
	 */
	public void doDelete(Usuario usuario) {
		daoUsuario.bloqueio(usuario);
		//messageInfo("Usuário "+usuario.getNome()+" foi excluido com sucesso.");
	}
	
	/**
	 * 
	 * @param actionEvent
	 */
	public void doDeleteSelecionados(AjaxBehaviorEvent event) {
		
		if(modelUsuario.getSelecionados() == null || modelUsuario.getSelecionados().size() <= 0){
			messageWarn("Nenhum registro foi selecionado.");
			return;
		}
		
		int qtd = modelUsuario.getSelecionados().size();
		daoUsuario.bloqueio(modelUsuario.getSelecionados());
		messageInfo("Foi(ram) excluido(s) "+qtd+" registro(s) com sucesso.");
		
	}
	
	/**
	 * 
	 * @param actionEvent
	 */
	public void doAtivarSelecionados(AjaxBehaviorEvent event) {
		
		if(modelUsuario.getSelecionados() == null || modelUsuario.getSelecionados().size() <= 0){
			messageWarn("Nenhum registro foi selecionado.");
			return;
		}
		
		int qtd = modelUsuario.getSelecionados().size();
		daoUsuario.desbloqueio(modelUsuario.getSelecionados());
		messageInfo("Foi(ram) excluido(s) "+qtd+" registro(s) com sucesso.");
		
	}    
	

}
