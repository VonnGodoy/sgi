package br.com.vle.controller.contratos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.lowagie.text.Document;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.entidade.Contrato;
import br.com.vle.entidade.Financeiro;
import br.com.vle.entidade.Imoveis;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.entidade.StatusImoveis;
import br.com.vle.enuns.MesesEnum;
import br.com.vle.enuns.ResponsavelDespesaEnum;
import br.com.vle.enuns.SituacaoContratoEnum;
import br.com.vle.enuns.SituacaoFinancasEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.enuns.StatusImoveisEnum;
import br.com.vle.enuns.TipoContratosEnum;
import br.com.vle.enuns.TipoImovelEnum;
import br.com.vle.model.ModelContratos;
import br.com.vle.relatorios.contrato.ContratoAluguel;
import br.com.vle.servico.dao.DaoContratos;
import br.com.vle.servico.dao.DaoFinanceiro;
import br.com.vle.servico.dao.DaoImoveis;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoStatusImoveis;
import br.com.vle.util.ValidatorUtils;

/**
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class IncluirContratosController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	private @Inject transient Logger logger;

	@Inject
	private DaoContratos daoContratos;

	@Inject
	private DaoImoveis daoImovel;

	@Inject
	private DaoPessoa daoPessoa;
	
	@Inject
	private DaoFinanceiro daoFinanceiro;
	
	@Inject
	private DaoStatusImoveis daoStatus;

	@Inject
	private ModelContratos model;

	@Inject
	private Conversation conversation;
	
	@Inject
	private ContratoAluguel rel;

	/**
	 * Initializing Data Access Service for LazyImoveisDataModel class role list for
	 * ImoveisContoller class
	 */
	@PostConstruct
	public void init() {
		logger.info("=========================================");
		logger.info("==  Start Incluir Contratos Controller  ==");
		logger.info("==  conversation = " + conversation.getId());
		logger.info("=========================================");

		if (model.getContrato() == null) {
			model.setContrato(new Contrato());
		}
	}

	@PreDestroy
	public void destroi() {
		logger.info("===========================================");
		logger.info("===  Stop Incluir Contratos Controller  ===");
		logger.info("===========================================");
	}

	public void duraçao() {

	}

	public String salvar() throws Exception {

		if (validarDados()) {

			try {

				model.getContrato().setPessoa(model.getPessoaSelecionada());
				model.getContrato().setImovel(model.getImovelSelecionado());

				// Atualiza endereco do locatario
				model.getContrato().getPessoa().setCep(model.getContrato().getImovel().getCep());
				model.getContrato().getPessoa().setBairro(model.getContrato().getImovel().getBairro());
				model.getContrato().getPessoa().setComplemento(model.getContrato().getImovel().getComplemento());
				model.getContrato().getPessoa().setLocalidade(model.getContrato().getImovel().getLocalidade());
				model.getContrato().getPessoa().setLogradouro(model.getContrato().getImovel().getLogradouro());
				model.getContrato().getPessoa().setNumero(model.getContrato().getImovel().getNumero());
				model.getContrato().getPessoa().setUf(model.getContrato().getImovel().getUf());
				
				//Atualiza Status Imovel
				model.getContrato().getImovel().setStatusImovel(buscarStatus());

				logger.info("=========================================");
				logger.info("==========  Salvando ContratoAluguel  ==========");
				logger.info("=========================================");

				model.getContrato().setRespDespesas(model.getResponsavel().getId().toString());
				model.getContrato().setContratoStatus(SituacaoContratoEnum.VIGENTE.getId());
				model.getContrato().setTpcontrato(tpImovel());
				model.getContrato().setPeriodo(model.getDtIni() + " A " + model.getDtFim());
				model.getContrato().setValidade(model.getValidade().getNome());
				
				
				daoPessoa.update(model.getContrato().getPessoa());
				daoContratos.create(model.getContrato());
				daoImovel.update(model.getContrato().getImovel());
				
				model.setContrato(buscarContrato(model.getContrato()));

			} catch (Exception e) {

				messageErro("Erro ao Gerar ContratoAluguel");

			}

			messageInfo("Contrato Incluido Com Sucesso");

			gerarFinanceiro();
			gerarcontrato();

			return redirect(Pages.MANTER_CONTRATOS);

		}

		return redirect(Pages.MANTER_CONTRATOS_INCLUIR);

	}
	
	public StatusImoveis buscarStatus() {
		
		StatusImoveis status = null;
		
		List <StatusImoveis> list = daoStatus.buscarTudo(); 
		
		for (StatusImoveis s: list) {
			
			if(s.getId().toString().equals(StatusImoveisEnum.OCUPADO.getId())) {
				status = s;
			}
		}
		
		return status;
	}

	public boolean validarDados() {
		boolean retorno = true;

		if (model.getPessoaSelecionada() == null) {
			retorno = false;
			messageErro("nao foi possivel encontrar os dados de Pessoa");
		}
		if (model.getImovelSelecionado() == null) {
			retorno = false;
			messageErro("Nao foi possivel encontrar os dados do Imovel");
		}

		return retorno;
	}

	public String tpImovel() {
		String retorno = "";
		TipoImovelEnum[] tp = TipoImovelEnum.values();

		for (TipoImovelEnum tipo : tp) {

			if (tipo.getTipo().toString().equals(model.getImovelSelecionado().getTpImovel())) {

				retorno = tipo.getId().toString();
				break;
			}

		}
		return retorno;
	}
	
	
	public String StatusContrato() {
		String retorno = "";
		SituacaoContratoEnum[] tp = SituacaoContratoEnum.values();

		for (SituacaoContratoEnum tipo : tp) {

			if (tipo.getSituacao().toString().equals(model.getContrato().getContratoStatus())) {

				retorno = tipo.getId().toString();
				break;
			}

		}
		return retorno;
	}
	
	public void gerarcontrato() throws Exception {

		Document documento = rel.gerar(model.getContrato());
		
		model.setIdImovel("");
		model.setIdPessoa("");
		model.setContrato(null);
		model.setImovelSelecionado(null);
		model.setPessoaSelecionada(null);
		model.setResponsavel(ResponsavelDespesaEnum.SELECIONE);

		messageInfo("Impressao de contrato ainda nao disponivél");
	}

	public String voltar() {

		model.setIdContrato("");
		model.setIdImovel("");
		model.setIdPessoa("");
		model.setContrato(null);
		model.setImovelSelecionado(null);
		model.setPessoaSelecionada(null);
		model.setContratoAtivo(StatusEnum.SELECIONE);
		model.setTpContrato(TipoContratosEnum.SELECIONE);
		model.setResponsavel(ResponsavelDespesaEnum.SELECIONE);

		return redirect(Pages.MANTER_CONTRATOS);

	}

	public String selecionarImovel() {
		DTOFiltroBase filtro = new DTOFiltroBase();

		if (model.getIdImovel() != null && !model.getIdImovel().toString().equals("")) {
			filtro.addFiltro("id_imovel", model.getIdImovel().toString());
		} else {
			messageErro("O Campo Id Imovel é de Preenchimento Obrigatório");
		}
		
		Imoveis im = daoImovel.buscarImovel(filtro);
		
		if(im !=null) {
			if(im.getStatusImovel().getId().toString().equals(StatusImoveisEnum.LIVRE.getId())) {
			
			model.setImovelSelecionado(im);
		}else {
			messageErro("O imovel Selecionado não esta disponível ");
		}
		}else {
			messageErro("O Imovel Não Cadastrado ");
		}


		if (model.getPessoaSelecionada() != null && model.getImovelSelecionado()!=null) {

			model.setIdImovel(model.getImovelSelecionado().getId());
			model.setDsImovel(model.getImovelSelecionado().getDsImovel());

		}
		return redirect(Pages.MANTER_CONTRATOS_INCLUIR);
	}

	public String selecionarPessoa() {

		DTOFiltroBase filtro = new DTOFiltroBase();

		if (model.getIdPessoa() != null && !model.getIdPessoa().toString().equals("")) {
			if (ValidatorUtils.isValidDocument(model.getIdPessoa().toString())) {

				filtro.addFiltro("cpf_cnpj", model.getIdPessoa().toString());

				model.setPessoaSelecionada(daoPessoa.buscarPessoa(filtro));

				if (model.getPessoaSelecionada() != null) {

					model.setIdPessoa(model.getPessoaSelecionada().getCpfCnpj());
					model.setNomePessoa(model.getPessoaSelecionada().getNome());

				} else {

					messageErro("Nenhuma Pessoa Encontrado");
				}

			} else {
				messageErro("Cpf/Cnpj Inválido");
			}
		} else {

			messageErro("O Campo Cpf/Cnpj é de Preenchimento Obrigatório");
		}

		return redirect(Pages.MANTER_CONTRATOS_INCLUIR);

	}

	public void gerarFinanceiro() {

		MesesEnum[] meses = MesesEnum.values();
		List<Financeiro> mensalidades = new ArrayList();
		Financeiro mensalidade = null;
		String mesInicio = model.getDtIni().toString().substring(0,3);
		int ano = Integer.valueOf(model.getDtIni().toString().substring(5));
		String prox ="";

		for (int i = 0 ; i < model.getValidade().getParcelas(); i++) {
			mensalidade = new Financeiro();
			
			mensalidade.setContrato(model.getContrato());
			mensalidade.setImovel(model.getContrato().getImovel());
			mensalidade.setPessoa(model.getContrato().getPessoa());
			mensalidade.setDiaVencimento(model.getContrato().getDiaVencimento());
			mensalidade.setStatus(SituacaoFinancasEnum.PENDENTE.getId().toString());
			mensalidade.setTxMultaAtraso(model.getContrato().getTxMultaAtraso());
			mensalidade.setValorMensalidade(model.getContrato().getValorMensalidade());
			
			for(MesesEnum mes : meses) {
				
				if(mes.getNome().toString().equals(mesInicio)) {
					mensalidade.setMes(mes.getNome()+"-"+ano);
					mensalidades.add(mensalidade);
					mesInicio = "";
					prox = mes.getProx();
					break;
				}
				
			if (mes.getNome().toString().equals(prox)) {
				
				mensalidade.setMes(mes.getNome()+"-"+ano);
				mensalidades.add(mensalidade);
				prox = mes.getProx();
				
				if(prox.toString().equals("JAN")) {
					ano = ano + 1;
				}
				break;
			}
		}

		}
		
		daoFinanceiro.create(mensalidades);
	}
	
	public Contrato buscarContrato(Contrato con) {
		
		DTOFiltroBase filtro = new DTOFiltroBase();
    	
    	if(con.getPessoa().getCpfCnpj() != null 
    			&& !con.getPessoa().getCpfCnpj().toString().equals("")){
    		filtro.addFiltro("id_pessoa", con.getPessoa().getCpfCnpj());
    	}
    	
    	if(con.getImovel().getId() != null 
    			&& !con.getImovel().getId().toString().equals("")){
    		filtro.addFiltro("id_imovel", model.getIdImovel().toString());
    	}
    	
    	if(con.getContratoStatus() != null){
    		
    		filtro.addFiltro("contrato_status", StatusContrato().toString());
    	}
    	
    	if(con.getTpcontrato() !=null){
    		
    		filtro.addFiltro("tp_contrato", tpImovel().toString());
    	}
		return daoContratos.buscarContrato(filtro);
	}

	public ModelContratos getModel() {
		return model;
	}

	public void setModel(ModelContratos model) {
		this.model = model;
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

}
