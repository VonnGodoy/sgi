package br.com.vle.controller.pessoa;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.Pages;
import br.com.vle.controller.perfilacesso.Eventos;
import br.com.vle.entidade.Usuario;
import br.com.vle.model.ModelEndereco;
import br.com.vle.model.ModelPessoa;
import br.com.vle.servico.dao.DaoEndereco;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

@Named
@RequestScoped
public class AlterarPessoaController extends ControllerBase {

	private static final long serialVersionUID = 8776100090021916640L;

	private @Inject transient Logger logger;

	@Inject
	private DaoPessoa daoPessoa;

	@Inject
	private DaoUsuario daoUsuario;

	@Inject
	private DaoEndereco daoEndereco;

	@Inject
	private ModelPessoa model;

	@Inject
	private Conversation conversation;

	@Inject
	private Event<Eventos> events;

	@PostConstruct
	public void init() {
		logger.info("======================================");
		logger.info("==  Start Alterar Pessoa Controller  ==");
		logger.info("==  conversation = " + conversation.getId());
		logger.info("======================================");

		if (conversation.isTransient()) {
			logger.info("==  conversation begin             ==");
			conversation.begin();
		}

	}

	@PreDestroy
	public void destroi() {
		logger.info("=======================================");
		logger.info("===  Stop Alterar Pessoa Controller  ===");
		logger.info("=======================================");
	}

	public String alterar() {
		logger.info("nome atual = " + model.getPessoaSelecionado().getNome());

		if (!model.getUsuarioSelecionado().getPerfilAcesso().getId().toString()
				.equals(model.getPerfilUsuario().getId().toString())) {
			model.getUsuarioSelecionado().setPerfilAcesso(model.getPerfilUsuario());

			try {
				daoUsuario.update(model.getUsuarioSelecionado());

			} catch (Exception e) {
				messageErro("Erro ao Auterar Perfil de Acesso");
			}
		}

		daoPessoa.update(model.getPessoaSelecionado());

		messageInfo("Pessoa Alterada Com Sucesso");

		return voltar(true);

	}

	public String buscarEndereco() {

		if (model.getPessoaSelecionado().getCep().toString() != null
				&& model.getPessoaSelecionado().getCep().toString().trim().length() == 8) {

			ModelEndereco endereco = daoEndereco.buscaEnderecoPorCep(model.getPessoaSelecionado().getCep().toString());

			if (endereco != null && endereco.getStatusConexao()) {

				messageErro("Erro ao Buscar CEP");
			} else if (endereco != null) {
				model.getPessoaSelecionado().setLogradouro(endereco.getLogradouro());
				model.getPessoaSelecionado().setComplemento(endereco.getComplemento());
				model.getPessoaSelecionado().setBairro(endereco.getBairro());
				model.getPessoaSelecionado().setLocalidade(endereco.getLocalidade());
				model.getPessoaSelecionado().setUf(endereco.getUf());

			} else {
				messageErro("Cep Inválido");
			}
		} else {

			messageErro("O Campo CEP é Obrigatório");
		}
		return redirect(Pages.MANTER_PESSOA_ALTERAR);
	}

	public ModelPessoa getModel() {
		return model;
	}

	public void setModel(ModelPessoa model) {
		this.model = model;
	}

	public Usuario buscaUsuario(String id) {

		Usuario usu = null;

		for (Usuario usuario : model.getUsuarios()) {
			if (usuario.getId().toString().equals(id)) {
				usu = usuario;
				break;
			}
		}

		return usu;
	}

}
