package br.com.vle.controller.perfilacesso;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vle.comum.ControllerBase;
import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Role;
import br.com.vle.model.ModelPerfil;
import br.com.vle.servico.dao.DaoPerfilAcesso;

/**
 *
 * @author Victor Godoi
 */

@SuppressWarnings("serial")
@Named
@RequestScoped
public class IncluirPerfilController extends ControllerBase {

	private static final Logger logger = LoggerFactory.getLogger(IncluirPerfilController.class);

	@Inject
	private DaoPerfilAcesso servicePerfilAcesso;
	
	@Inject
	private PerfilAcesso newPerfilAcesso;
	
	@Inject
	private ModelPerfil model;
	

	/**
	 * Initializing Data Access Service for LazyUserDataModel class role list
	 * for UserContoller class
	 */
	@PostConstruct
	public void init() {
		if(logger.isInfoEnabled()){
			logger.info("=====================================");
			logger.info("==  Start IncluirPerfilController  ==");
			logger.info("=====================================");
		}
	}
	
	@PreDestroy
	public void destroi(){
		if(logger.isInfoEnabled()){
			logger.info("======================================");
			logger.info("===  Stop IncluirPerfilController  ===");
			logger.info("======================================");
		}
	}


	
	public String salvar() {
		List<Role> rolespais  = new ArrayList<Role>();
		List<Role> selecionados =  model.getRoles();
		for (Role role: selecionados) {
			
			Role rolepai = new Role(role.getId(), role.getRolePai());
			if(!rolespais.contains(rolepai)){
				rolespais.add(rolepai);
			}
		}
		 newPerfilAcesso.setPerfil(model.getNomePerfilConsulta());
		 newPerfilAcesso.getRoles().addAll(rolespais);
		
		servicePerfilAcesso.create(newPerfilAcesso);
		setNewPerfilAcesso(new PerfilAcesso());
		
		model.setNomePerfilConsulta("");
		messageInfo("Incluido com sucesso");
		return voltar(true);

	}

	public PerfilAcesso getNewPerfilAcesso() {
		return newPerfilAcesso;
	}

	public void setNewPerfilAcesso(PerfilAcesso newPerfilAcesso) {
		this.newPerfilAcesso = newPerfilAcesso;
	}

	public ModelPerfil getModel() {
		return model;
	}

	public void setModel(ModelPerfil model) {
		this.model = model;
	}
	
	
	

}
