package br.com.vle.controller.perfilacesso;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.model.ModelPerfil;
import br.com.vle.servico.dao.DaoPerfilAcesso;

/**
 *
 * @author Victor Godoi
 */
 
@SuppressWarnings("serial")
public class DataModelPerfilAcesso extends LazyDataModel<PerfilAcesso> implements Serializable{

  	
    private List<PerfilAcesso> datasource;
    
    private int pageSize;
    
    private int rowIndex;
    
    private int rowCount;
    
   
    
    private DaoPerfilAcesso dao;
    
    private ModelPerfil model;
    
    /**
     *
     * @param crudService
     */
    public DataModelPerfilAcesso(ModelPerfil model, DaoPerfilAcesso dao) {
        this.model = model;
        this.dao = dao;
    }

    /**
     * Lazy loading user list with sorting ability
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<User>
     */ 
    @Override
    public List<PerfilAcesso> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	DTOFiltroBase filtro = new DTOFiltroBase();
    	
    	if(model.getQtdRows()!=null){
    		filtro.setQtdRegistroPaginacao(model.getQtdRows().longValue());
    	}
    	
    	if (!sortOrder.equals(SortOrder.UNSORTED)) {
    		filtro.setOrdenacao(sortField, sortOrder.name());
		}
    	
    	filtro.setPrimeiroRegistroPage(first);
    	
    	if(model.getNomePerfilConsulta() != null && !"".equals(model.getNomePerfilConsulta())){
    		filtro.addFiltro("perfil", model.getNomePerfilConsulta());
    	}
    	
    	datasource = dao.buscarTodosPerfisAcesso(filtro);
        setRowCount(dao.totalPerfisAcesso(filtro));   
        return datasource;
    }
    
    /**
     * Checks if the row is available
     * @return boolean
     */
    @Override
    public boolean isRowAvailable() {
        if(datasource == null) 
            return false;
        int index = rowIndex % pageSize ; 
        return index >= 0 && index < datasource.size();
    }
    
    /**
     * Gets the user object's primary key
     * @param user
     * @return Object
     */
    @Override
    public Object getRowKey(PerfilAcesso user) {
        return user.getId().toString();
    }

    /**
     * Returns the user object at the specified position in datasource.
     * @return 
     */
    @Override
    public PerfilAcesso getRowData() {
        if(datasource == null)
            return null;
        int index =  rowIndex % pageSize;
        if(index > datasource.size()){
            return null;
        }
        return datasource.get(index);
    }
    
    /**
     * Returns the user object that has the row key.
     * @param rowKey
     * @return 
     */
    @Override
    public PerfilAcesso getRowData(String rowKey) {
        if(datasource == null)
            return null;
       for(PerfilAcesso perfil : datasource) {  
           if(perfil.getId().toString().equals(rowKey))  
           return perfil;  
       }  
       return null;  
    }
    
    
    /**
     *
     * @param pageSize
     */
    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Returns page size
     * @return int
     */
    @Override
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Returns current row index
     * @return int
     */
    @Override
    public int getRowIndex() {
        return this.rowIndex;
    }
    
    /**
     * Sets row index
     * @param rowIndex
     */
    @Override
    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    /**
     * Sets row count
     * @param rowCount
     */
    @Override
    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }
    
    /**
     * Returns row count
     * @return int
     */
    @Override
    public int getRowCount() {
        return this.rowCount;
    }
     
    /**
     * Sets wrapped data
     * @param list
     */
    @SuppressWarnings("unchecked")
	@Override
    public void setWrappedData(Object list) {
        this.datasource = (List<PerfilAcesso>) list;
    }
    
    /**
     * Returns wrapped data
     * @return
     */
    @Override
    public Object getWrappedData() {
        return datasource;
    }
}
                    
