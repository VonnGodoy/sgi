package br.com.vle.controller.perfilacesso;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vle.comum.ControllerBase;
import br.com.vle.comum.NegocioException;
import br.com.vle.comum.Pages;
import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Role;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.model.ModelPerfil;
import br.com.vle.servico.dao.DaoPerfilAcesso;
import br.com.vle.servico.dao.DaoRole;

/**
 *
 * @author Victor Godoi
 */

@SuppressWarnings("serial")
@Named
@RequestScoped
public class PerfilController extends ControllerBase {

	private static final Logger logger = LoggerFactory.getLogger(PerfilController.class);
	
	@Inject
	private DaoPerfilAcesso servicePerfilAcesso;
	
	@Inject
	private DaoRole daoRole;
	
	@Inject
	private ModelPerfil model;


	@PostConstruct
	public void init() {
		if(logger.isInfoEnabled()){
			logger.info("=====================================");
			logger.info("======  Start PerfilController  =====");
			logger.info("=====================================");
		}
		
	}
	
	@PreDestroy
	public void destroi(){
		if(logger.isInfoEnabled()){
			logger.info("=====================================");
			logger.info("======  Stop PerfilController  ======");
			logger.info("=====================================");
		}
	}
	
	/**
	 * @param actionEvent
	 */
	public void doLimpaConsulta(ActionEvent actionEvent){ 
		model.setNomePerfilConsulta(null);
	}

	/**
	 * 
	 * @param actionEvent
	 */
	public void doDelete(PerfilAcesso perfil) {
		try{
			servicePerfilAcesso.delete(perfil);
			messageInfo("perfil_msg_apagar", perfil.getId().toString());
		}catch(Exception e){
			throw new NegocioException(e, e.getMessage(),null);
		}
	}
	
	/**
	 * 
	 * @param actionEvent
	 */
	public void doDeleteSelecionados(AjaxBehaviorEvent event) {
		if(model.getSelecionados() == null || model.getSelecionados().length <= 0){
			messageWarn("sem_selecao");
			return;
		}
		int qtd = model.getSelecionados().length;
		servicePerfilAcesso.deleteItems(model.getSelecionados());
		messageInfo("perfil_msg_apagar_lote", +qtd+"");
	}
	
	/**
	 * 
	 * @param actionEvent
	 */
	public void doDeleteSelecionados() {
		doDeleteSelecionados(null);
	}
	
    /**
     * @param perfil
     */
    public String abrirAlterar(PerfilAcesso perfil){
    	logger.info("vai atualizar "+perfil.getPerfil());
    	
    	List<Role> roles = new ArrayList<Role>();
    	for (Role role : perfil.getRoles()) {
    		roles.add(new Role(role.getId(), role.getRole(), role.getRoledesc(), role.getRolePai()));
		}
    	perfil.setRoles(roles);
    	model.setPerfilSelecionado(perfil);
    	
    	return redirect(Pages.MANTER_PERFIL_ALTERAR);
    }
    
    /**
     * @param perfil
     */
    public String selecionar(){
    	
    	if(model.getPerfilSelecionado() == null){
    		messageErro("perfil_msg_selecionar");
    		return null;
    	}
    	
    	return voltar();
    }
    
    /**
     * @return
     */
    public String novoPerfil(){
    	
    	DTOFiltroBase filtro = new DTOFiltroBase();
    	filtro.setQtdRegistroPaginacao(0l);
    	model.setRoles(daoRole.buscarTudo(filtro));
    	
    	return redirect(Pages.MANTER_PERFIL_INCLUIR);
    }
   
}
