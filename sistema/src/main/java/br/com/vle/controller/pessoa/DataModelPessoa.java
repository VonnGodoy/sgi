package br.com.vle.controller.pessoa;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.model.ModelPessoa;
import br.com.vle.servico.dao.DaoPessoa;

/**
 *
 * @author Victor Godoi
 */

public class DataModelPessoa extends LazyDataModel<Pessoa> implements Serializable{

	private static final long serialVersionUID = -3960618974569641885L;
	
    private List<Pessoa> datasource;
    
    private int pageSize;
    
    private int rowIndex;
    
    private int rowCount;
    
   
    
    private DaoPessoa dao;
    
    private ModelPessoa model;
    
    /**
     *
     * @param crudService
     */
    public DataModelPessoa(ModelPessoa model, DaoPessoa dao) {
        this.model = model;
        this.dao = dao;
    }

    /**
     * Lazy loading user list with sorting ability
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<User>
     */ 
    @Override
    public List<Pessoa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	DTOFiltroBase filtro = new DTOFiltroBase();
    	filtro.setPrimeiroRegistroPage(first);
    	
    	if(model.getQtdRows()!=null){
    		
    		filtro.setQtdRegistroPaginacao(model.getQtdRows().longValue());
    	}
    	
    	if (!sortOrder.equals(SortOrder.UNSORTED)) {
    		
    		filtro.setOrdenacao(sortField, sortOrder.name());
		}
    	
    	if(model.getNome() != null 
    			&& !model.getNome().toString().equals("")){
    		
    		filtro.addFiltro("nome", model.getNome().toString());
    		
    	}
    	
    	if(model.getCpfCnpj() != null && !model.getCpfCnpj().toString().equals("")){
    		
    		filtro.addFiltro("cpf_cnpj", model.getCpfCnpj().toString());
    	}
    	
    	if(model.getStatus() != null && !model.getStatus().equals(StatusEnum.SELECIONE)){
    		
    		filtro.addFiltro("ativo", model.getStatus().getStatus());
    	}
    	
    	datasource = dao.buscarTudo(filtro);
        setRowCount(datasource.size());   
        return datasource;
    }
    
    /**
     * Checks if the row is available
     * @return boolean
     */
    @Override
    public boolean isRowAvailable() {
        if(datasource == null) 
            return false;
        int index = rowIndex % pageSize ; 
        return index >= 0 && index < datasource.size();
    }
    
    /**
     * Gets the user object's primary key
     * @param user
     * @return Object
     */
    @Override
    public Object getRowKey(Pessoa locat) {
        return locat.getCpfCnpj().toString();
    }

    /**
     * Returns the user object at the specified position in datasource.
     * @return 
     */
    @Override
    public Pessoa getRowData() {
        if(datasource == null)
            return null;
        int index =  rowIndex % pageSize;
        if(index > datasource.size()){
            return null;
        }
        return datasource.get(index);
    }
    
    /**
     * Returns the user object that has the row key.
     * @param rowKey
     * @return 
     */
    @Override
    public Pessoa getRowData(String rowKey) {
        if(datasource == null)
            return null;
       for(Pessoa locat : datasource) {  
           if(locat.getCpfCnpj().toString().equals(rowKey))  
           return locat;  
       }  
       return null;  
    }
    
    
    /**
     *
     * @param pageSize
     */
    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Returns page size
     * @return int
     */
    @Override
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Returns current row index
     * @return int
     */
    @Override
    public int getRowIndex() {
        return this.rowIndex;
    }
    
    /**
     * Sets row index
     * @param rowIndex
     */
    @Override
    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    /**
     * Sets row count
     * @param rowCount
     */
    @Override
    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }
    
    /**
     * Returns row count
     * @return int
     */
    @Override
    public int getRowCount() {
        return this.rowCount;
    }
     
    /**
     * Sets wrapped data
     * @param list
     */
    @SuppressWarnings("unchecked")
	@Override
    public void setWrappedData(Object list) {
        this.datasource = (List<Pessoa>) list;
    }
    
    /**
     * Returns wrapped data
     * @return
     */
    @Override
    public Object getWrappedData() {
        return datasource;
    }
}
                    
