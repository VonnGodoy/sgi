package br.com.vle.controller.pessoa.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.vle.model.dto.PessoaDTO;

/**
 *
 * @author Victor Godoi
 */

@Path("services/pessoa")
public interface PessoaRest {
	
	@GET
	@Path("/todos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	List<PessoaDTO> todos();

	@GET
	@Path("/porDocumento")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	PessoaDTO porDocumento(String CpfCnpj);

}
