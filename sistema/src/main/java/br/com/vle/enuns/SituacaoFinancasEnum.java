package br.com.vle.enuns;

/**
*
* @author Victor Godoi
*/

public enum SituacaoFinancasEnum {

		SELECIONE("0","SELECIONE"),
		PAGO("1","PAGO"),
		PENDENTE("2","PENDENTE"),
		ATRAZADO("3","EM ATRAZO");
		
		private String id;
		private String status;
		
		private SituacaoFinancasEnum(String id, String status) {
			this.id = id;
			this.status = status;
		}

		public String getId() {
			return id;
		}

		public String getStatus() {
			return status;
		}
		
		
	}
