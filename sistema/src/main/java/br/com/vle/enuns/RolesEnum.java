package br.com.vle.enuns;

/**
 *
 * @author Victor Godoi
 */

public enum RolesEnum {
	
	MANTER_PERFIL_ACESSO("manter_perfil_acesso","Manter perfil de acesso",null),
	MANTER_PERFIL_ACESSO_ALTERAR("manter_perfil_acesso_alterar","Alterar  perfil de acesso",MANTER_PERFIL_ACESSO),
	MANTER_PERFIL_ACESSO_APAGAR("manter_perfil_acesso_apagar","Excluir perfil de acesso",MANTER_PERFIL_ACESSO),
	MANTER_PERFIL_ACESSO_INCLUIR("manter_perfil_acesso_incluir","Incluir perfil de acesso",MANTER_PERFIL_ACESSO),
	
	MANTER_USUARIO("manter_usuario","Manter os usuário do sistema",null),
	MANTER_USUARIO_INCLUIR("manter_usuario_incluir","Incluir usuarios",MANTER_USUARIO),
	MANTER_USUARIO_ALTERAR("manter_usuario_alterar","Alterar usuarios",MANTER_USUARIO),
	MANTER_USUARIO_BLOQUEAR("manter_usuario_bloquear","Bloquear usuarios",MANTER_USUARIO),
	MANTER_USUARIO_DESBLOQUEAR("manter_usuario_desbloquear","Desbloquear usuarios",MANTER_USUARIO),
	
	MANTER_PESSOA("manter_pessoa","Manter os pessoa do sistema",null),
	MANTER_PESSOA_INCLUIR("manter_pessoa_incluir","Incluir pessoa",MANTER_PESSOA),
	MANTER_PESSOA_ALTERAR("manter_pessoa_alterar","Alterar pessoa",MANTER_PESSOA),
	MANTER_PESSOA_BLOQUEAR("manter_pessoa_bloquear","Bloquear pessoa",MANTER_PESSOA),
	MANTER_PESSOA_DESBLOQUEAR("manter_pessoa_desbloquear","Desbloquear pessoa",MANTER_PESSOA),
	
	MANTER_IMOVEIS("manter_imoveis","Manter os imoveis no sistema",null),
	MANTER_IMOVEIS_INCLUIR("manter_imoveis_incluir","Incluir imoveis",MANTER_IMOVEIS),
	MANTER_IMOVEIS_ALTERAR("manter_imoveis_alterar","Alterar imoveis",MANTER_IMOVEIS),
	MANTER_IMOVEIS_EXCLUIR("manter_imoveis_excluir","Excluir imoveis",MANTER_IMOVEIS),
	MANTER_IMOVEIS_DETALHAR("manter_imoveis_detalhar","Detalhar imoveis",MANTER_IMOVEIS),
	
	MANTER_CONTRATOS("manter_contratos","Manter Contratos",null),
	MANTER_CONTRATOS_INCLUIR("manter_contratos_incluir","Incluir ContratoAluguel",MANTER_CONTRATOS),
	MANTER_CONTRATOS_RENOVAR("manter_contratos_renovar","Renovar ContratoAluguel",MANTER_CONTRATOS),
	MANTER_CONTRATOS_DETALHAR("manter_contratos_detalhar","Detalhar ContratoAluguel",MANTER_CONTRATOS),
	MANTER_CONTRATOS_INATIVAR("manter_contratos_inativar","Inativar ContratoAluguel",MANTER_CONTRATOS),
	MANTER_CONTRATOS_EXCLUIR("manter_contratos_excluir","Excluir ContratoAluguel",MANTER_CONTRATOS),

	FINANCEIRO("financeiro","Financeiro",null),
	FINANCEIRO_LANCAMENTOS("financeiro_lancamentos","financeiro_lancamentos",FINANCEIRO),
	CONFIGURACAO("configuracoes","Configuracões do sistema",null);
	
	private String roleid;
	private String nome;
	private String descricao;
	private RolesEnum pai;
	
	private RolesEnum(String nome , String descricao, RolesEnum pai){
		this.descricao = descricao;
		this.nome = nome;
		this.pai = pai;
	}

	public String getNome() {
		return nome;
	}
	
	public String getRole() {
		return roleid;
	}

	public String getDescricao() {
		return descricao;
	}

	public RolesEnum getPai() {
		return pai;
	}

	
}
