package br.com.vle.enuns;

/**
 *
 * @author Victor Godoi
 */

public enum TipoImovelEnum {
	
	SELECIONE("0","SELECIONE"),
	RESIDENCIAL("1","RESIDENCIAL"),
	COMERCIAL("2","COMERCIAL");
	
	private String id;
	private String tipo;
	
	private TipoImovelEnum(String id, String tipo) {
		this.id = id;
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}
	
	public String getId() {
		return id;
	}
	
	
}
