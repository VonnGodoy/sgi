package br.com.vle.enuns;

/**
 *
 * @author Victor Godoi
 */

public enum StatusImoveisEnum {
	
	SELECIONE("1","SELECIONE"),
	REFORMA("2","EM REFORMA"),
	OCUPADO("3","OCUPADO"),
	LIVRE("4","LIVRE");
	
	private String id;
	private String status;
	
	private StatusImoveisEnum(String id, String status) {
		this.id = id;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
	
	public String getId() {
		return id;
	}
	
	
}
