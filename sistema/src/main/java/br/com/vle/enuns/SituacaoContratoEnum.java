package br.com.vle.enuns;

/**
 *
 * @author Victor Godoi
 */

public enum SituacaoContratoEnum {
	SELECIONE("0","SELECIONE"),
	VIGENTE("1","VIGENTE"),
	CANCELADO("2","CANCELADO"),
	VENCIDO("3","VENCIDO");
	
	private String id;
	private String situacao;
	
	private SituacaoContratoEnum(String id, String status) {
		this.id = id;
		this.situacao = status;
	}

	public String getId() {
		return id;
	}

	public String getSituacao() {
		return situacao;
	}
	
	
}
