package br.com.vle.enuns;

/**
 *
 * @author Victor Godoi
 */

public enum StatusEnum {
	SELECIONE("SELECIONE", null),
	ATIVO("ATIVO",Boolean.TRUE),
	INATIVO("INATIVO",Boolean.FALSE);
	
	private String nome;
	private Boolean status;
	
	private StatusEnum(String nome, Boolean status) {
		this.nome = nome;
		this.status = status;
	}

	public String getNome() {
		return nome;
	}

	public Boolean getStatus() {
		return status;
	}
	
	
}
