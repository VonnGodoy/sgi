package br.com.vle.enuns;

public enum MesesEnum {

	SELECIONE("SELECIONE", "0", " "),
	JAN("JAN","1", "FEV"),
	FEV("FEV","2", "MAR"),
	MAR("MAR","3", "ABR"),
	ABR("ABR","4", "MAI"),
	MAI("MAI","5", "JUN"),
	JUN("JUN","6", "JUL"),
	JUL("JUL","7", "AGO"),
	AGO("AGO","8", "SET"),
	SET("SET","9", "OUT"),
	OUT("OUT","10", "NOV"),
	NOV("NOV","11", "DEZ"),
	DEZ("DEZ","12", "JAN");
	
	private String nome;
	private String mes;
	private String prox;
	
	private MesesEnum(String nome, String mes, String prox) {
		this.nome = nome;
		this.mes = mes;
		this.prox = prox;
	}

	public String getNome() {
		return nome;
	}

	public String getMes() {
		return mes;
	}
	
	public String getProx() {
		return prox;
	}
}
