package br.com.vle.enuns;

/**
 *
 * @author Victor Godoi
 */

public enum TipoContratosEnum {
	
	SELECIONE("0","SELECIONE"),
	RESIDENCIAL("1","CONTRATO RESIDENCIAL"),
	COMERCIAL("2","CONTRATO COMERCIAL");
	
	private String id;
	private String tipo;
	
	private TipoContratosEnum(String id, String tipo) {
		this.id = id;
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}
	
	public String getId() {
		return id;
	}
	
	
}
