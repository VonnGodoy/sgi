package br.com.vle.enuns;

public enum PeriodosEnum {

	SELECIONE("0" , "SELECIONE",0),
	ONE("1" , "1 ANO",12),
	TWO("2" , "2 ANOS",24),
	TRE("3" , "3 ANOS",36);

	
	private String periodo;
	private String nome;
	private int parcelas;
	
	private PeriodosEnum( String periodo ,String nome,int parcelas) {
		this.nome = nome;
		this.periodo = periodo;
		this.parcelas = parcelas;
	}

	public String getNome() {
		return nome;
	}

	public String getPeriodo() {
		return periodo;
	}
	
	public Integer getParcelas() {
		return parcelas;
	}
}
