package br.com.vle.enuns;

/**
 *
 * @author Victor Godoi
 */

public enum ResponsavelDespesaEnum {
	
	SELECIONE("0","SELECIONE"),
	LOCADOR("1","LOCADOR"),
	LOCATARIO("2","LOCATARIO");
	
	private String id;
	private String responsavel;
	
	private ResponsavelDespesaEnum(String id, String responsavel) {
		this.id = id;
		this.responsavel = responsavel;
	}

	public String getResponsavel() {
		return responsavel;
	}
	
	public String getId() {
		return id;
	}
	
	
}
