package br.com.vle.component.jsf;

import java.io.Serializable;
import java.nio.file.Path;

/**
 *
 * @author Victor Godoi
 */

public class ImageUpload implements Serializable {

	private String nome;
	private String caminhoFile;
	private String nomeminiatura;
	private String caminhoFileMini;
	private String base;
	private Long size;
	private Path miniatura;
	private Path image;
	private Boolean existRepo = Boolean.FALSE;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Path getMiniatura() {
		return miniatura;
	}

	public void setMiniatura(Path miniatura) {
		this.miniatura = miniatura;
	}

	public Path getImage() {
		return image;
	}

	public void setImage(Path image) {
		this.image = image;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getNomeminiatura() {
		return nomeminiatura;
	}

	public void setNomeminiatura(String nomeminiatura) {
		this.nomeminiatura = nomeminiatura;
	}

	public String getCaminhoFile() {
		return caminhoFile;
	}

	public void setCaminhoFile(String caminhoFile) {
		this.caminhoFile = caminhoFile;
	}

	public String getCaminhoFileMini() {
		return caminhoFileMini;
	}

	public void setCaminhoFileMini(String caminhoFileMini) {
		this.caminhoFileMini = caminhoFileMini;
	}

	public Boolean getExistRepo() {
		return existRepo;
	}

	public void setExistRepo(Boolean existRepo) {
		this.existRepo = existRepo;
	}
	
	

}
