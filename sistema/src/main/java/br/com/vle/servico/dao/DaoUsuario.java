package br.com.vle.servico.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Usuario;
import br.com.vle.entidade.dto.DTOFiltroBase;

/**
 *
 * @author Victor Godoi
 */

@Stateless
public class DaoUsuario extends DaoGenerico<Usuario> {

	public DaoUsuario() {
		super(Usuario.class);
	}

	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Usuario> buscarTodosUsuarios(DTOFiltroBase filtro) {
		String sb = new String("FROM Usuario ");
		return buscarPorFiltroDTO(sb, filtro);
	}

	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int totalPerfisAcesso(DTOFiltroBase filtro) {
		String sb = new String("select count(u) FROM Usuario u  ");
        return totalPorFiltroDTO(sb, "u", filtro);
	}

	/**
	 * @param item
	 * @return
	 */
	@Override
	public Usuario update(Usuario item) {
		PerfilAcesso perfil = getEm().getReference(PerfilAcesso.class, item.getPerfilAcesso().getId());
		item.setPerfilAcesso(perfil);
		return super.update(item);
	}
	
	/**
	 * @param item
	 * @return
	 */
	public Usuario alterarSenha(Usuario item) {
		Usuario u = find(item.getId());
		u.setPassword(item.getPassword());
		u.setPrimeiroAcesso(false);
		return super.update(u);
	}

	/**
	 * @param t
	 * @return
	 */
	@Override
	public Usuario create(Usuario t) {
		String idPerfil = t.getPerfilAcesso().getId().toString();
		System.out.println(idPerfil);
		PerfilAcesso p = getEm().find(PerfilAcesso.class,idPerfil);
		t.setPerfilAcesso(p);
		return super.create(t);
	}
	
	/**
	 * @param usuario
	 */
	public void bloqueio(Usuario usuario) {
		Usuario p = getEm().getReference(Usuario.class,	usuario.getId());
		p.setAtivo(false);
	}
	
	/**
	 * @param usuario
	 */
	public void desbloqueio(Usuario usuario) {
		Usuario p = getEm().getReference(Usuario.class,	usuario.getId());
		p.setAtivo(true);
	}
	
	/**
	 * @param usuarios
	 */
	public void bloqueio(List<Usuario> usuarios) {
		if(usuarios!=null && usuarios.size()>0){
			for (Usuario usuario : usuarios) {
				bloqueio(usuario);
			}
		}
	}
	
	/**
	 * @param usuarios
	 */
	public void desbloqueio(List<Usuario> usuarios) {
		if(usuarios!=null && usuarios.size()>0){
			for (Usuario usuario : usuarios) {
				desbloqueio(usuario);
			}
		}
	}

}
