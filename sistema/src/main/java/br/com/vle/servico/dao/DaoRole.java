package br.com.vle.servico.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.com.vle.entidade.Imoveis;
import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Role;
import br.com.vle.entidade.dto.DTOFiltroBase;

/**
 * 
 * @author Victor Godoi
 */

@Stateless
public class DaoRole extends DaoGenerico<Role> {

	public DaoRole() {
		super(Role.class);
	}

	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Role> buscarTodos() {
		
		List<Role> buscaPorNamedQuery = buscaPorNamedQuery(Role.ALL);
				
		return buscaPorNamedQuery;
	}
	
	public List<Role> buscarTudo(DTOFiltroBase filtro) {
		String sb = new String("FROM Role ");
		return buscarPorFiltroDTO(sb, filtro);
	}
	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int totalRoles(DTOFiltroBase filtro) {
		String sb = new String("select count(p) FROM Role p  ");
        return totalPorFiltroDTO(sb, "p", filtro);
	}
	

}
