package br.com.vle.servico.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.dto.DTOFiltroBase;

/**
 *
 * @author Victor Godoi
 */

@Stateless
public class DaoPessoa extends DaoGenerico<Pessoa> {

	public DaoPessoa() {
		super(Pessoa.class);
	}

	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Pessoa> buscarTudo(DTOFiltroBase filtro) {
		String sb = new String("FROM Pessoa ");
		return buscarPorFiltroDTO(sb, filtro);
	}

	/**
	 * @param item
	 * @return
	 */
	@Override
	public Pessoa update(Pessoa item) {

		return super.update(item);
	}	

	/**
	 * @param t
	 * @return
	 */
	@Override
	public Pessoa create(Pessoa t) {
		String idPessoa = t.getCpfCnpj().toString();
		System.out.println(idPessoa);
		return super.create(t);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Pessoa buscarPessoa(DTOFiltroBase filtro) {
		Pessoa retorno = null;
		String sb = new String("FROM Pessoa ");
		List <Pessoa> pessoas = buscarPorFiltroDTO(sb, filtro);
		 
		for(Pessoa pessoa : pessoas) {
			
			retorno = pessoa;
		}
		
		return retorno;
	}

	public List<Pessoa> buscarPessoa(Pessoa Pessoa) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param pessoa
	 */
	public void delete(Pessoa pessoa) {
		super.delete(pessoa);
	}
	
	/**
	 * @param usuario
	 */
	public void bloqueio(Pessoa pessoa) {
		pessoa.setAtivo(false);
		update(pessoa);	
	}
	
	/**
	 * @param usuario
	 */
	public void desbloqueio(Pessoa pessoa) {
		pessoa.setAtivo(true);
		update(pessoa);
	}
	
	/**
	 * @param usuarios
	 */
	public void bloqueio(List<Pessoa> pessoas) {
		if(pessoas!=null && pessoas.size()>0){
			for (Pessoa pes : pessoas) {
				bloqueio(pes);
			}
		}
	}
	
	/**
	 * @param usuarios
	 */
	public void desbloqueio(List<Pessoa> pessoas) {
		if(pessoas!=null && pessoas.size()>0){
			for (Pessoa pes : pessoas) {
				desbloqueio(pes);
			}
		}
	}

}
