package br.com.vle.servico.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.vle.entidade.StatusImoveis;
import br.com.vle.entidade.dto.DTOFiltroBase;

/**
 *
 * @author Victor Godoi
 */

@Stateless
public class DaoStatusImoveis extends DaoGenerico<StatusImoveis> {

	public DaoStatusImoveis() {
		super(StatusImoveis.class);
	}

	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<StatusImoveis> buscarTudo() {
		DTOFiltroBase filtro = new DTOFiltroBase();
		String sb = new String("FROM StatusImoveis ");
		return buscarPorFiltroDTO(sb, filtro);
	}

}
