package br.com.vle.servico.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.vle.entidade.Financeiro;
import br.com.vle.entidade.dto.DTOFiltroBase;


/**
*
* @author Victor Godoi
*/

@Stateless
public class DaoFinanceiro extends DaoGenerico<Financeiro> {


		public DaoFinanceiro() {
			super(Financeiro.class);
		}

		@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
		public List<Financeiro> buscarTudo(DTOFiltroBase filtro) {
			String sb = new String("FROM Financeiro ");
			return buscarPorFiltroDTO(sb, filtro);
		}

		@Override
		public Financeiro update(Financeiro item) {

			return super.update(item);
		}	

		public void delete(Financeiro item) {
			super.delete(item);
		}

		@Override
		public Financeiro create(Financeiro t) {
			return super.create(t);
		}
		
		public void create(List<Financeiro> t) {
			for(Financeiro fin : t) {
				create(fin);
			}
		}

	}
