package br.com.vle.servico.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.vle.entidade.Imoveis;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.dto.DTOFiltroBase;

/**
 *
 * @author Victor Godoi
 */

@Stateless
public class DaoImoveis extends DaoGenerico<Imoveis> {

	public DaoImoveis() {
		super(Imoveis.class);
	}

	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Imoveis> buscarTudo(DTOFiltroBase filtro) {
		String sb = new String("FROM Imoveis ");
		return buscarPorFiltroDTO(sb, filtro);
	}
	
	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Imoveis buscarImovel(DTOFiltroBase filtro) {
		String sb = new String("FROM Imoveis ");
		Imoveis imovel = null;
		List <Imoveis> imoveis = buscarPorFiltroDTO(sb, filtro);
		
		
		for (Imoveis imo: imoveis) {
		
			imovel = imo;
			
		}
		
		return imovel;
	}

	/**
	 * @param item
	 * @return
	 */
	@Override
	public Imoveis update(Imoveis item) {

		return super.update(item);
	}	

	public void delete(Imoveis imovel) {
		super.delete(imovel);
	}
	/**
	 * @param t
	 * @return
	 */
	@Override
	public Imoveis create(Imoveis t) {
		String idImovel = t.getId();
		System.out.println(idImovel);
		return super.create(t);
	}


}
