package br.com.vle.servico.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.dto.DTOFiltroBase;

/**
 * 
 * @author Victor Godoi
 */

@Stateless
public class DaoPerfilAcesso extends DaoGenerico<PerfilAcesso> {

	public DaoPerfilAcesso() {
		super(PerfilAcesso.class);
	}

	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<PerfilAcesso> buscarTodosPerfisAcesso(DTOFiltroBase filtro) {
		String sb = new String(" select p FROM PerfilAcesso p inner join fetch  p.roles ");		
		return buscarPorFiltroDTO(sb, "p", filtro);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<PerfilAcesso> buscarPerfil(DTOFiltroBase filtro) {
		String sb = new String("FROM PerfilAcesso ");
		List <PerfilAcesso> ret = buscarPorFiltroDTO(sb, filtro);
		
		return ret;
	}
	
	/**
	 * @param filtro
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int totalPerfisAcesso(DTOFiltroBase filtro) {
		String sb = new String("select count(p) FROM PerfilAcesso p  ");
        return totalPorFiltroDTO(sb, "p", filtro);
	}
	
	
	/* (non-Javadoc)
	 * @see br.com.vle.servico.dao.DaoGenerico#update(java.lang.Object)
	 */
	@Override
	public PerfilAcesso update(PerfilAcesso item) {
		PerfilAcesso perfilBanco = find(item.getId());
		perfilBanco.setDescricao(item.getDescricao());
		perfilBanco.setRoles(item.getRoles());
		return super.update(perfilBanco);
	}
	

}
