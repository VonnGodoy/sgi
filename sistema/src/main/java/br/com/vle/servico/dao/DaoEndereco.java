package br.com.vle.servico.dao;

import javax.ejb.Stateless;

import br.com.vle.model.ModelEndereco;
import br.com.vle.util.ws.EnderecoWS;

/**
*
* @author Victor Godoi
*/

@Stateless
public class DaoEndereco {

	public DaoEndereco() {
		super();
	}

	public ModelEndereco buscaEnderecoPorCep(String cep) {
		
		return EnderecoWS.getEnderecoPorCep(cep);
	}
}
