package br.com.vle.servico.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.com.vle.entidade.Contrato;
import br.com.vle.entidade.dto.DTOFiltroBase;

/**
 *
 * @author Victor Godoi
 */

@Stateless
public class DaoContratos extends DaoGenerico<Contrato> {

	public DaoContratos() {
		super(Contrato.class);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Contrato> buscarTudo(DTOFiltroBase filtro) {
		String sb = new String("FROM Contrato ");
		return buscarPorFiltroDTO(sb, filtro);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Contrato buscarContrato(DTOFiltroBase filtro) {
		String sb = new String("FROM Contrato ");
		List <Contrato> contratos = buscarPorFiltroDTO(sb, filtro);
		Contrato contrato = null;
		
		for(Contrato con : contratos) {
			contrato = new Contrato();
			contrato = con;	
		}
		
		return contrato;
	}

	@Override
	public Contrato update(Contrato item) {

		return super.update(item);
	}	

	public void delete(Contrato item) {
		super.delete(item);
	}

	@Override
	public Contrato create(Contrato t) {
		return super.create(t);
	}

}
