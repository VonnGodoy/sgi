package br.com.vle.relatorios.financeiro;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import br.com.vle.entidade.Financeiro;
import br.com.vle.enuns.SituacaoFinancasEnum;

public class ListaFinanceiro {

	public Document gerar(List<Financeiro> ListFinanciro) throws Exception {
		Document doc = null;
		FileOutputStream os = null;

		Date data = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
		String dtAtual = formato.format(data);

		try {
			validaDiretorio();

			// cria o documento tamanho A4, margens de 2,54cm
			doc = new Document(PageSize.A4.rotate(), 72, 72, 72, 72);

			// cria a stream de saída
			os = new FileOutputStream("C:\\Relatorios\\Financeiro\\Relatorio_Financeiro" + dtAtual.toString() + ".pdf");

			// associa a stream de saída ao
			PdfWriter.getInstance(doc, os);

			// abre o documento
			doc.open();

			// adiciona o texto ao PDF

			Paragraph cabecalho = new Paragraph("RELATORIO FINANCEIRO     " + dtAtual.toString());
			cabecalho.setAlignment(Element.ALIGN_CENTER);
			cabecalho.setSpacingAfter(20);
			doc.add(cabecalho);

			PdfPTable table = new PdfPTable(new float[] { 0.3f, 0.2f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f });

			table.setWidthPercentage(100.0f);
			table.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(PdfPCell.RIGHT);

			PdfPCell header = new PdfPCell(new Paragraph(""));
			header.setBorder(Rectangle.NO_BORDER);
			header.setColspan(7);

			table.addCell(header);

			//table.addCell("Contrato Nº");
			table.addCell("Locatário");

			table.addCell("Telefone");
			table.addCell("Vencimento");

			table.addCell("Situaçao");
			table.addCell("Valor");

			table.addCell("Multa Por Atrazo");
			table.addCell("Valor Pago");

			BigDecimal Total = null;
			BigDecimal ValorTotal = null;
			BigDecimal ValorTotalPago = new BigDecimal("0.00");

			for (Financeiro fin : ListFinanciro) {

				if(ValorTotal !=null) {
					Total = ValorTotal;
				}

				//table.addCell(fin.getContrato().getId());
				table.addCell(fin.getPessoa().getNome());

				table.addCell(formataTel(fin.getPessoa().getTelPrin()));


				table.addCell(fin.getDiaVencimento() + " - " + fin.getMes());

				table.addCell(fin.getStatus());
				table.addCell(fin.getValorMensalidade().toString());

				table.addCell(fin.getTxMultaAtraso().toString());

				if(fin.getTotal_mes()!=null) {
					table.addCell(fin.getTotal_mes().toString());
				}else{
					table.addCell("");
				}
				if (Total == null) {
					Total = fin.getValorMensalidade();
				}else if (fin.getTotal_mes() != null) {
					ValorTotal= Total.add(fin.getTotal_mes());
				} else {
					ValorTotal= Total.add(fin.getValorMensalidade());
				}
				if(fin.getStatus().toString().equals(SituacaoFinancasEnum.PAGO.getStatus().toString())){

					if (fin.getTotal_mes() != null) {

						ValorTotalPago = ValorTotalPago.add(fin.getTotal_mes());

					} else {
						ValorTotalPago = ValorTotalPago.add(fin.getValorMensalidade());
					}

				}

			}

			//table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");

			table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");

			table.addCell("Valor Total");

			if(ValorTotal !=null) {
			table.addCell(ValorTotal.toString());
			}else if(Total !=null) {
				table.addCell(Total.toString());
			}else {
				table.addCell("");
			}
			table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");
			table.addCell("");

			table.addCell("Total Pago");

			table.addCell(ValorTotalPago.toString());


			doc.add(table);

		} catch (Exception e) {

			StackTraceElement[] erro = e.getStackTrace();

		} finally {

			if (doc != null) {
				// fechamento do documento
				doc.close();
			}
			if (os != null) {
				// fechamento da stream de saída
				os.close();
			}

		}

		return doc;
	}

	public void validaDiretorio() {

		validaDiretorioRaiz();
		File dir = new File("C:\\Relatorios\\Financeiro");

		if (!dir.exists()) {
			dir.mkdirs();
		}

	}

	public void validaDiretorioRaiz() {
		File dir = new File("C:\\Relatorios");

		if (!dir.exists()) {
			dir.mkdirs();
		}

	}

	public void abrirDoc(String doc) {

	}

	public String formataTel(String tel) {
		String retorno = "";

		if(tel.trim().length() == 10) {
			retorno = "("+tel.substring(0, 2)+") "
					+tel.substring(2,6)+"-"
					+tel.substring(6);


		}else if(tel.trim().length() == 11){

			retorno = "("+tel.substring(0, 2)+") "
					+tel.substring(2,7)+"-"
					+tel.substring(7);

		}else {
			retorno = tel;
		}


		return retorno;
	}

}
