package br.com.vle.relatorios.contrato;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import br.com.vle.entidade.Contrato;

public class ListaDeContratos {



		public Document gerar(List<Contrato> ListContratos) throws Exception {
			Document doc = null;
			FileOutputStream os = null;
			
			Date data = new Date();
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			String dtAtual = formato.format(data);

			try {
				validaDiretorio();

				// cria o documento tamanho A4, margens de 2,54cm
				doc = new Document(PageSize.A4.rotate(), 72, 72, 72, 72);

				// cria a stream de saída
				os = new FileOutputStream(
						"C:\\Relatorios\\ListaContratos\\Lista_Contratos_" + dtAtual.toString() + ".pdf");

				// associa a stream de saída ao
				PdfWriter.getInstance(doc, os);

				// abre o documento
				doc.open();

				// adiciona o texto ao PDF
				
				Paragraph cabecalho = new Paragraph("LISTA DE IMÓVEIS     " +dtAtual.toString());
				cabecalho.setAlignment(Element.ALIGN_CENTER);
				cabecalho.setSpacingAfter(20);
				doc.add(cabecalho);

				PdfPTable table = new PdfPTable(new float[] { 0.1f, 0.1f, 0.3f, 0.2f, 0.1f, 0.2f });

				table.setWidthPercentage(100.0f);
				table.setHorizontalAlignment(Element.ALIGN_LEFT);
				table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);

				PdfPCell header = new PdfPCell(new Paragraph(""));
				header.setBorder(Rectangle.NO_BORDER);
				header.setColspan(6);

				table.addCell(header);
				
				table.addCell("Contrato Nº");
				table.addCell("Imovel Nº");

				table.addCell("Locatário");
				table.addCell("Telefone");

				table.addCell("Situaçao Contrato");
				table.addCell("Tipo De Contrato");
				
				for(Contrato con : ListContratos) {

					table.addCell(con.getId());
					table.addCell(con.getImovel().getId());

					table.addCell(con.getPessoa().getNome());
					table.addCell(con.getPessoa().getTelPrin());

					table.addCell(con.getContratoStatus());
					table.addCell(con.getTpcontrato());
				
				}

				doc.add(table);


			} catch(Exception e){
				
				
			}finally {
				
				if (doc != null) {
					// fechamento do documento
					doc.close();
				}
				if (os != null) {
					// fechamento da stream de saída
					os.close();
				}

			}

			return doc;
		}

		public void validaDiretorio() {

			validaDiretorioRaiz();
			File dir = new File("C:\\Relatorios\\ListaContratos");

			if (!dir.exists()) {
				dir.mkdirs();
			}

		}

		public void validaDiretorioRaiz() {
			File dir = new File("C:\\Relatorios");

			if (!dir.exists()) {
				dir.mkdirs();
			}

		}

		public void abrirDoc(String doc) {

		}

	}
