package br.com.vle.relatorios.contrato;

import java.io.File;
import java.io.FileOutputStream;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import br.com.vle.entidade.Contrato;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.servico.dao.DaoPessoa;

public class ContratoAluguel {

	@Inject
	private DaoPessoa daoPessoa;

	public Pessoa buscaUsuario() {

		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();

		return buscarPessoa(request.getUserPrincipal().toString());

	}

	public Pessoa buscarPessoa(String id) {

		
		DTOFiltroBase filtro = new DTOFiltroBase();

		filtro.addFiltro("cpf_cnpj", id);

		return daoPessoa.buscarPessoa(filtro);
	}

	public Document gerar(Contrato con) throws Exception {
		Document doc = null;
		FileOutputStream os = null;

		try {
			Pessoa usu = buscaUsuario();
			validaDiretorio();

			// cria o documento tamanho A4, margens de 2,54cm
			doc = new Document(PageSize.A4, 72, 72, 72, 72);

			// cria a stream de saída
			os = new FileOutputStream(
					"C:\\Relatorios\\Contratos\\Contrato_" + con.getPessoa().getNome() + "_" + con.getId() + ".pdf");

			// associa a stream de saída ao
			PdfWriter.getInstance(doc, os);

			// abre o documento
			doc.open();

			// adiciona o texto ao PDF
			Paragraph cabecalho = new Paragraph("CONTRATO DE LOCAÇÃO DE IMOVEL DE Nº. " + con.getId());
			cabecalho.setAlignment(Element.ALIGN_CENTER);
			cabecalho.setSpacingAfter(20);
			doc.add(cabecalho);

			PdfPTable table = new PdfPTable(new float[] { 0.3f, 0.7f });

			table.setWidthPercentage(100.0f);
			table.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);

			PdfPCell header = new PdfPCell(new Paragraph(""));
			header.setBorder(Rectangle.NO_BORDER);
			header.setColspan(2);

			table.addCell(header);

			table.addCell("Locador: ");
			table.addCell(usu.getNome());

			table.addCell("Locatario: ");
			table.addCell(con.getPessoa().getNome());

			table.addCell("Endereço do Imóvel: ");
			table.addCell(con.getImovel().getLogradouro() + " " + con.getImovel().getNumero() + " "
					+ con.getImovel().getComplemento() + " " + con.getImovel().getBairro() + " "
					+ con.getImovel().getLocalidade() + " " + con.getImovel().getUf());

			table.addCell("Vigência do ContratoAluguel: ");
			table.addCell(con.getPeriodo());

			table.addCell("Valor do Aluguel: ");
			table.addCell(con.getValorMensalidade().toString() + ",00");

			table.addCell(header);
			table.addCell(header);

			doc.add(table);

			Paragraph p1 = new Paragraph("     Pelo presente instrumento particular de Locação o Locador: "
					+ usu.getNome() + ", inscrito no CPF/CNPJ: " + usu.getCpfCnpj() + " localizado no Endereco: "
					+ usu.getLogradouro() + " " + usu.getNumero() + " " + usu.getComplemento() + " " + usu.getBairro()
					+ " " + usu.getLocalidade() + " " + usu.getUf() + ",e o Locatário: " + con.getPessoa().getNome()
					+ ", inscrito no CPF/CNPJ: " + con.getPessoa().getCpfCnpj() + " e portador do RG: "
					+ con.getPessoa().getRg() + " residentes e domiciliando sito, " + con.getImovel().getLogradouro()
					+ " " + con.getImovel().getNumero() + " " + con.getImovel().getComplemento() + " "
					+ con.getImovel().getBairro() + " " + con.getImovel().getLocalidade() + " "
					+ con.getImovel().getUf() + " donominado mais adiante simplesmente o (os), a (as), "
					+ "tem entre si justo e contratado, por este e na melhor forma de direito, a presente locação mediante as cláusulas "
					+ "e condições abaixo discriminadas e disposições legais pertinentes, que voluntariamente aceitam e outorgam:");

			p1.setSpacingAfter(20);
			doc.add(p1);

			Paragraph claus1 = new Paragraph("  CLAUSULA PRIMEIRA - O LOCADOR dá em locação o imóvel sito "
					+ con.getImovel().getLogradouro() + " " + con.getImovel().getNumero() + " "
					+ con.getImovel().getComplemento() + " " + con.getImovel().getBairro() + " "
					+ con.getImovel().getLocalidade() + " " + con.getImovel().getUf() + " à pelo prazo de: "
					+ con.getValidade() + ", ao iniciar em " + con.getPeriodo().toString().substring(0, 8)
					+ " e terminar em " + con.getPeriodo().toString().substring(11)
					+ ". (Renováveis automaticamente por igual, caso não haja manifestação em contrário de uma das partes). "
					+ "Data em que o LOCATÁRIO ou (os, a(as), se abriga a restituir o imóvel locado no perfeito estado de conservação em que o recebeu, com uma nova pintura e contas em dia"
					+ " salvo as deteriorações decorrentes do uso normal, inteiramente livre e desocupado.");

			claus1.setSpacingAfter(20);
			doc.add(claus1);

			Paragraph clausEsp = new Paragraph(
					"  CLAUSULA ESPECÍFICA – O LOCATÁRIO (os), a (as), tem poder de transferir para seu nome junto aos órgãos competentes:"
							+ " (CAESB) e (CEB), as contas provenientes de fornecimento de água e energia elétrica, de acordo com as normas de alteração de titularidade fornecida"
							+ " pelas entidades acima descritas, o LOCATÁRIO fica ciente que é de sua responsabilidade todas as contas que forem feitas co período vigente deste contrato,"
							+ " ficando assim este documento em conformidade com as exigências atuais.");

			clausEsp.setSpacingAfter(20);
			doc.add(clausEsp);

			Paragraph claus1p1 = new Paragraph(
					"  Parágrafo primeiro – Antes do vencimento do prazo ajustado no caput desta cláusula não poderá o LOCADOR"
							+ " retomar o imóvel salvo se motivado por inflação contratual do (os), a (as), LOCATÁRIO. No caso de devolução do imóvel ao LOCADOR"
							+ " antes do prazo, o (os), a (as) LOCATÁRIO pagará a multa prevista na CLAUSULA DÉCIMA SÉTIMA. Estando o contrato vigente por tempo indeterminado,"
							+ " somente ficará isento o (os), a (as) LOCATÁRIO, do pagamento da multa contratual se avisar ao LOCADOR, por escrito, com antecedência mínima de 30 dias,"
							+ " Segundo os termos que o artigo 27 da Lei 8245/9, lei do inquilinato, fica claro que no caso de venda do imóvel, o LOCATÁRIO terá a preferência"
							+ " de compra nos mesmos termos e condições que terceiros, caso não se manifeste no prazo máximo de 30 dias deverá desocupar o mesmo sem nenhum dano as partes.");

			claus1p1.setSpacingAfter(20);
			doc.add(claus1p1);

			Paragraph claus1p2 = new Paragraph(
					"  Parágrafo segundo – Quando da devolução das chaves ao final do contrato, se as mesmas forem restituídas por"
							+ " preposto ou portador do o (os), a (as) LOCATÁRIO, fica este desde já autorizado a assinar o respectivo Termo de entrega de chaves, assim como,"
							+ " acompanhar e assinar o Termo de vistoria em nome daquele.");

			claus1p2.setSpacingAfter(20);
			doc.add(claus1p2);

			Paragraph claus1p3 = new Paragraph(
					"  Parágrafo terceiro – Na hipótese do (os, a (as) LOCATÁRIO abandonar o imóvel, fica o LOCADOR e ou o Administrador autorizado"
							+ " a imitir-se na sua posse, a fim de evitar a depredação ou invasão do mesmo. O termo de entrega de chaves será substituído por uma DECLARAÇÃO DE IMISSÃO"
							+ " DE POSSE, firmado pelo LOCADOR e 02 (duas) testemunhas idôneas.");

			claus1p3.setSpacingAfter(20);
			doc.add(claus1p3);

			Paragraph claus1p4 = new Paragraph(
					"  Parágrafo quarto – Em caso de falecimento do LOCADOR, a locação transmitir-se-á aos herdeiros. No caso de falecimento"
							+ " do LOCATÁRIO, ficarão sub-rogados nos seus direitos e obrigações, ao seu cônjuge ou a companheira e, sucessivamente, aos herdeiros, desde que"
							+ " residente no imóvel. Em caso de separação judicial ou de fato, a locação prosseguirá automaticamente com o cônjuge ou companheira que permanecer"
							+ " no imóvel; neste caso deverá a sub-rogação ser comunicada por escrito, ficando o LOCADOR e ou o administrador, no direito de exigir nova garantia"
							+ " de fiança.");

			claus1p4.setSpacingAfter(20);
			doc.add(claus1p4);

			Paragraph claus2 = new Paragraph(
					"  CLÁUSULA SEGUNDA – O valor mensal do aluguel, livremente pactuado será de "
							+ con.getValorMensalidade().toString() + ",00" + " a ser pago pelo LOCATÁRIO, no dia "
							+ con.getDiaVencimento()
							+ " de cada mês, em boleto nos bancos autorizados, somado ao custo do banco pelo boleto"
							+ " ou pagamento em mãos para o LOCATÁRIO e ou administrador autorizado.");

			claus2.setSpacingAfter(20);
			doc.add(claus2);

			Paragraph claus2p1 = new Paragraph(
					"  Parágrafo primeiro – O (os), a (as) LOCATÁRIO terá um desconto mensal a título de pontualidade de "
							+ con.getTxMultaAtraso() + "%"
							+ " sobre o valor do aluguel se efetuar o pagamento impreterivelmente até a data do vencimento; "
							+ "após 01 dia de atraso, será cobrado multa de" + con.getTxMultaAtraso()
							+ "%; após 10 dias os débitos serão corrigidos monetariamente;"
							+ " sobre o atraso serão cobrados juros de mora de 0,38% ao dia.");

			doc.add(claus2p1);

			// Pagina 2
			// doc.newPage();

			Paragraph claus3 = new Paragraph(
					"  CLÁUSULA TERCEIRA – O aluguel mensal pactuado na <b>CLÁUSULA SEGUNDA</b> será reajustado a cada 12 (doze) meses,"
							+ " pela variação do IGPM/FGV, em conformidade com o que determina a legislação vigente na MP 1.079 de 28/07/95.");

			claus3.setSpacingAfter(20);
			doc.add(claus3);

			Paragraph claus3pu = new Paragraph(
					"  Parágrafo único – Na falta do índice contratado, seja por motivo de extinção ou não divulgação,"
							+ " fica desde já estabelecidos que o índice substituto seja sucessivamente o IPC/FIPE, IGP/FGV e o INPC/IBGE, respectivamente pela ordem."
							+ " Caso estes índices sejam extintos ou não calculados, o reajuste será feito pela média de 03 índices a escolha da administradora que reflita"
							+ " a variação da inflação ocorrida no período.");

			claus3pu.setSpacingAfter(20);
			doc.add(claus3pu);

			Paragraph claus4 = new Paragraph(
					"  CLÁUSULA QUARTA – Se necessário à propositura de ações de despejo, consignações em pagamento de aluguéis e assessórios da locação,"
							+ " as citações, intimações e notificações, além das formas previstas no Código de Processo Civil – CPC, poderão ser feitas mediante correspondência com aviso"
							+ " de recebimento (AR). Tratando-se de pessoa jurídica ou firma individual, poderão também ser feitas ás citações, notificações ou intimações,"
							+ " por fac-símile (FAX).");

			claus4.setSpacingAfter(20);
			doc.add(claus4);

			Paragraph claus5 = new Paragraph(
					"  CLÁUSULA QUINTA – o (os), a (as) LOCATÁRIO declara haver visitado e examinado os imóveis locados,"
							+ " que se encontra conforme o termo de vistoria assinado pelas partes, que fará parte integrante do presente contrato,"
							+ " no qual se faz expressa referência aos eventuais defeitos existentes, aceitando-os, obrigando-se, a partir daí,"
							+ " a zelar pelo que nele contiver a fazer de imediato, e por sua conta, todas as reparações dos estragos provenientes do uso normal"
							+ " no curso da locação de modo especial as decorrentes de entupimentos e obstruções na rede de esgoto e água pluvial,"
							+ " sem direito a retenção ou indenização por quaisquer benfeitorias que tenham sido feitas com autorização assumindo"
							+ " a responsabilidade de devolver o imóvel objeto deste contrato, tal qual o consignado no aludido termo.");

			claus5.setSpacingAfter(20);
			doc.add(claus5);

			Paragraph claus5p1 = new Paragraph(
					"  Parágrafo primeiro – Havendo divergência nas vistorias de entrega e recebimento do imóvel, o (os), a (as) LOCATÁRIO"
							+ " autoriza o LOCADOR a efetuar os consertos, pintura, colocação de vidros, etc., e exigir a cobrança tão logo apresente as notas fiscais"
							+ " e recibos de mão-de-obra correspondentes, independente de coleta de preços de material e mão-de-obra");

			claus5p1.setSpacingAfter(20);
			doc.add(claus5p1);

			Paragraph claus5p2 = new Paragraph(
					"  Parágrafo segundo – É assegurado ao LOCADOR o direito de vistoriar o imóvel, sempre que julgar conveniente,"
							+ " desde que atento ao disposto no Art. 23, Inciso IX, da Lei 8.245/91.");

			claus5p2.setSpacingAfter(20);
			doc.add(claus5p2);

			Paragraph claus5p3 = new Paragraph(
					"  Parágrafo terceiro – Deverá o (os, a (as) LOCATÁRIO entregar imediatamente ao LOCADOR ou ao administrador,"
							+ " toda e qualquer correspondência, intimações, documentos de cobrança de tributos, carnês de pagamento de prestações, encargos condominiais,"
							+ " atas e convocações de assembléias do condomínio, ainda que dirigidas o (os), a (as) LOCATÁRIO Art. 23, Inciso VII, da Lei 8.245/91.");

			claus5p3.setSpacingAfter(20);
			doc.add(claus5p3);

			Paragraph claus6 = new Paragraph(
					"  CLÁUSULA SEXTA – Além do aluguel, compete o (os), a (as) LOCATÁRIO o pagamento das despesas ordinárias de:"
							+ " condomínio, consumo de água, luz, taxas de esgoto. 13° de funcionários, IPTU/TLP, taxa de pintura e saneamento,"
							+ " bem como todos e quaisquer tributos que incidam sobre o imóvel objeto deste contrato; todas as multas pecuniárias provenientes"
							+ " do não pagamento ou do atraso no pagamento de quantias sob sua responsabilidade; todos os emolumentos devidos a órgãos administrativos.");

			claus6.setSpacingAfter(20);
			doc.add(claus6);

			Paragraph claus6p1 = new Paragraph(
					"  Parágrafo primeiro – Os impostos, taxas e despesas ordinárias de condomínio, que incidam, ou venhas a incidir sobre o imóvel,"
							+ " serão pagos pelo (os), a (as) LOCATÁRIO aos agentes cobradores e/ou órgãos responsáveis pela cobrança, devendo os comprovantes de pagamento ser exibidos"
							+ " ao administrador mensalmente, que não obstante poderão ser feitos por correio ou fax, sempre mencionando o número do contrato de locação.");

			claus6p1.setSpacingAfter(20);
			doc.add(claus6p1);

			Paragraph claus6p2 = new Paragraph(
					"  Parágrafo segundo – Na hipótese  de serem os encargos pagos pelo LOCADOR, porque não o tinha feito o (os), a (as)"
							+ " LOCATÁRIO, dará ensejo à propositura de ação de despejo por infringir o contrato, sujeitando-se o (os), a (as) LOCATÁRIO, ainda, ao pagamento da multa"
							+ " prevista na CLÁUSULA DÉCIMA SÉTIMA, independentemente do tempo decorrido deste contrato, sem prejuízo do disposto no parágrafo Segundo desta Cláusula.");

			claus6p2.setSpacingAfter(20);
			doc.add(claus6p2);

			Paragraph claus7 = new Paragraph(
					"  CLÁUSULA SÉTIMA – No ato da devolução do imóvel o (os), a (as) LOCATÁRIO deverá apresentar os últimos pagamentos das contas"
							+ " de água, luz, telefone, taxa de pintura, 13° de funcionários, IPTU/TLP e condomínio do último mês. Não estando, entretanto, o mesmo ainda de posse"
							+ " dos talões de cobrança do último mês, tornar-se-á por base para efeito de recebimento, a média extraída da soma dos talões dos meses anteriores,"
							+ " acrescidos da correção monetária do mês");

			doc.add(claus7);

			// Pagina 3
			// doc.newPage();

			Paragraph claus8 = new Paragraph(
					" CLÁUSULA OITAVA – O (os), a (as) LOCATÁRIO obrigar-se-á a segurar o imóvel locado contra os riscos de fogo em companhia"
							+ " de absoluta idoneidade, mantendo-o segurado até o final do prazo contratual.");

			claus8.setSpacingAfter(20);
			doc.add(claus8);

			Paragraph claus8p1 = new Paragraph(
					"  Parágrafo primeiro – O seguro incêndio deverá ser renovado anualmente, até 15 dias antes do vencimento de cada período,"
							+ " incumbindo-se o (os), a (as) LOCATÁRIO, nos dias seguintes á efetivação do seguro, entregar ao LOCADOR a apólice respectiva Art.22, Inciso VIII,"
							+ " Lei 8.245/91.");

			claus8p1.setSpacingAfter(20);
			doc.add(claus8p1);

			Paragraph claus8p2 = new Paragraph(
					"  Parágrafo segundo – Se o (os), a (as) LOCATÁRIO, no tempo devido, não cumprir a obrigação de segurar o prédio locado,"
							+ " o LOCADOR poderá efetivar o seguro por conta do (os), a (as) LOCATÁRIO, hipótese em que se acrescentará ao custo do prêmio à quantia equivalente"
							+ " ao valor de 01 (um) aluguel vigente à época, a titulo de multa; será cobrado o montante total juntamente com o aluguel que se vencer.");

			claus8p2.setSpacingAfter(20);
			doc.add(claus8p2);

			Paragraph claus8p3 = new Paragraph(
					"  Parágrafo terceiro – Se o (os), a (as) LOCATÁRIO vier contratar com a Companhia seguradora o pagamento parcelado"
							+ " do seguro incêndio, obrigar-se-á a apresentar ao LOCADOR, no ato do pagamento do aluguel, o comprovante de quitação da ultima parcela.");

			claus8p3.setSpacingAfter(20);
			doc.add(claus8p3);

			Paragraph claus8p4 = new Paragraph(
					" Parágrafo quarto – Constará sempre da apólice de seguro, disposição segundo a qual em caso de sinistro, a indenização"
							+ " será paga pela companhia seguradora, diretamente ao LOCADOR.");

			claus8p4.setSpacingAfter(20);
			doc.add(claus8p4);

			Paragraph claus8p5 = new Paragraph(
					"  Parágrafo quinto – Não obstante a feitura do seguro, o (os), a (as) LOCATÁRIO é vedado depositar no imóvel,"
							+ " matérias inflamáveis, explosivos ou corrosivos.");

			claus8p5.setSpacingAfter(20);
			doc.add(claus8p5);

			Paragraph claus9 = new Paragraph(
					"  CLÁUSULA NONA – No caso de desapropriação do imóvel locado, ficará o LOCADOR desobrigado de todas as cláusulas"
							+ " deste contrato reservando o (os), a (as) LOCATÁRIO tão somente a faculdade de haver do poder desapropriaste, a indenização que porventura tiver direito.");

			claus9.setSpacingAfter(20);
			doc.add(claus9);

			Paragraph claus10 = new Paragraph(
					"  CLÁUSULA DÉCIMA – Nenhuma intimação da Saúde publica será motivo para o (os), a (as) LOCATÁRIO abandonar o imóvel locado,"
							+ " ou pedir rescisão do contrato, salvo procedentes vistorias judiciais, que provem estar à construção ameaçada de ruína.");

			claus10.setSpacingAfter(20);
			doc.add(claus10);

			Paragraph claus11 = new Paragraph(
					"  CLÁUSULA DÉCIMA PRIMEIRA – Quaisquer tolerâncias ou concessões do LOCADOR, para com o (os), a (as) LOCATÁRIO,"
							+ " quando não manifestadas por escrito, não constituirão procedentes invocáveis por este e não terão a virtude de alterar obrigações contratuais.");

			claus11.setSpacingAfter(20);
			doc.add(claus11);

			Paragraph claus12 = new Paragraph(
					"  CLÁUSULA DÉCIMA SEGUNDA – O LOCADOR não responderá, em nenhum caso, por quaisquer danos que venha a sofrer o (os), a (as)"
							+ " LOCATÁRIO em razão de derramamento de liquido, água de rompimento de canos, de chuvas, de abertura de torneiras, defeitos de esgotos ou fossas,"
							+ " incêndios, arrombamentos, roubos, furtos, de casos fortuitos ou de força maior.");

			claus12.setSpacingAfter(20);
			doc.add(claus12);

			Paragraph claus13 = new Paragraph(
					"  CLÁUSULA DÉCIMA TERCEIRA – O (os), a (as) LOCATÁRIO não terá direito de reter o pagamento do aluguel ou de qualquer"
							+ " outra quantia devida ao LOCADOR, sob a alegação de não terem sido atendidas exigências porventura solicitadas.");

			claus13.setSpacingAfter(20);
			doc.add(claus13);

			Paragraph claus14 = new Paragraph(
					"  CLÁUSULA DÉCIMA QUARTA – O imóvel objeto do presente contrato destina-se exclusivamente para fim Locaçâo,"
							+ " ficando com o os), a (as) LOCATÁRIO proibido de mudar a destinação, ceder ou transferir a locação, sublocar ou emprestar o imóvel, no todo ou em parte,"
							+ " a qualquer título, sob pena de configurar infração contratual e possibilitar ao LOCADOR requerer a rescisão da locação.");

			claus14.setSpacingAfter(20);
			doc.add(claus14);

			Paragraph claus14pu = new Paragraph(
					"  Parágrafo único – A ocupação do imóvel por pessoa não referida neste contrato ou a permanência de qualquer pessoa,"
							+ " a partir do momento em que o (os), a (as) LOCATÁRIO deixar de usá-lo, caracterizará grave infração contratual que acarretará a rescisão da locação"
							+ " em qualquer época de sua vigência, sem prejuízo da aplicação da multa prevista na CLÁUSULA DÉCIMA SÉTIMA.");

			claus14pu.setSpacingAfter(20);
			doc.add(claus14pu);

			Paragraph claus15 = new Paragraph(
					"  CLÁUSULA DÉCIMA QUINTA – Em caso de venda do imóvel o (os), a (as) LOCATÁRIO será notificado do Direito de Preferência previsto"
							+ " na Lei do inquilinato, através de carta com aviso de recebimento (AR). Não se manifestando no prazo legal de 30 dias, será considerado como não interessado."
							+ " Não efetuando a compra do imóvel o (os), a (as) LOCATÁRIO autoriza o LOCADOR ou o administrador mostrar o imóvel aos futuros pretendentes,"
							+ " desde que acompanhado do LOCATRARIO e ou Administrador autorizado.");

			claus15.setSpacingAfter(20);
			doc.add(claus15);

			Paragraph claus16 = new Paragraph(
					"  CLÁUSULA DÉCIMA SEXTA – Como o LOCATÁRIO não apresentara os FIADORES concorda desde já, em entregar a título de fiança e como caução"
							+ "$P{contrato_valor_caucao}"
							+ " equivalente aos 12 (doze) meses de aluguel que se propõe a ficar no imóvel objeto deste contrato, conforme Artigo 38 § 2° Lei 8.245"
							+ " de 18/10/1991 (Lei do inquilinato). Os cheques ficarão sob responsabilidade do LOCATARIO onde poderá ser depositado em data correspondente ou resgatados em mãos,"
							+ " deixando claro e acordado que no caso de atraso dos LOCATÁRIOS autorizam desde já a inserir seus respectivos nomes no <b>SPC</b> ou entidades semelhantes,"
							+ " ficando todos os bônus e custas processuais e advocatícias por conta dos LOCATÁRIOS.");

			claus16.setSpacingAfter(20);
			doc.add(claus16);

			Paragraph claus16p1 = new Paragraph(
					"  Parágrafo primeiro – Na hipótese de ser o FIADOR casado, seu respectivo cônjuge, igualmente assina o presente contrato,"
							+ " na qualidade, também de FIADOR da locação. O consentimento conjugal, portanto, obrigado o cônjuge na solidariedade da garantia fidejussória prestada"
							+ " ao afiançado, até o termo final da locação.");

			claus16p1.setSpacingAfter(20);
			doc.add(claus16p1);

			Paragraph claus16p2 = new Paragraph(
					"  Parágrafo segundo – O FIADOR declara também solidariamente responsável pelo pagamento das obrigações assumidas pelo o (os),"
							+ " a (as) LOCATÁRIO, se este vier celebrar acordos de reajustes espontâneos de aluguéis, ainda que superior ao estabelecido ou permitido por Lei,"
							+ " assim como os oriundos de sentenças judiciais, conversões compulsórias exigidas pela Lei, aí incluídas as ações revisionais.");

			doc.add(claus16p2);

			// Pagina 4
			// doc.newPage();

			Paragraph claus16p3 = new Paragraph(
					"  Parágrafo terceiro – Nas hipóteses previstas nos incisos I a VII do Art. 40 da Lei 8.245/91 poderá"
							+ " o LOCADOR exigir novo fiador ou a substituição da modalidade de garantia. Nessas circunstâncias se notificado o (os), a (as)"
							+ " LOCATÁRIO, no prazo de 30 dias, não providenciar novo fiador ou substituir a modalidade da garantia, sujeitará este a responder"
							+ " pela competente cão de despejo por infringir o contrato, que haverá de ser cumulado com a cobrança da multa pecuniária da"
							+ " CLÁUSULA DÉCIMA SÉTIMA qualquer que seja o tempo decorrido deste contrato.");

			claus16p3.setSpacingAfter(20);
			doc.add(claus16p3);

			Paragraph claus17 = new Paragraph(
					"  CLÁUSULA DÉCIMA SÉTIMA – Fica estipulada a multa de 03 meses de aluguel vigente na data da ocorrência,"
							+ " na qual ocorrerá à parte que infringir quaisquer clausulas deste contrato, havendo faculdade para a parte inocente de considerar"
							+ " rescindida a locação, independentemente de qualquer que seja o tempo decorrido do presente contrato, e promover o despejo do imóvel."
							+ " A mesma penalidade sujeitará o (os), a (as) LOCATÁRIO se não efetuar o pagamento do aluguel no prazo previsto na CLÁUSULA PRIMEIRA,"
							+ " obrigando o LOCADOR propor ação de despejo.");

			claus17.setSpacingAfter(20);
			doc.add(claus17);

			Paragraph claus18 = new Paragraph(
					"  CLÁUSULA DÉCIMA OITAVA – Se o (os), a (as) LOCATÁRIO não for encontrado nos endereços constantes do cadastro"
							+ " que serviu para elaboração deste contrato, desde já e por instrumento, constitui como sua procuradora, o FIADOR da locação, citado na CLÁUSULA DÉCIMA SEXTA,"
							+ " para o fim especifico de, em conjunto ou separadamente, independente da ordem em nomeação, representá-lo em quaisquer ações judiciais relacionadas com este contrato,"
							+ " conferindo-lhes poderes amplos e especiais para receber citações, inclusive a inicial, notificações e intimações, bem como para confessar e reconhecer a procedência"
							+ " do pedido, transigir, desistir o imóvel no caso de abandono ainda que seja necessária a retirada de móveis que ocupam, sendo notificado conforme prevista na CLÁUSULA QUARTA.");

			claus18.setSpacingAfter(20);
			doc.add(claus18);

			Paragraph claus19 = new Paragraph(
					"  CLÁUSULA DÉCIMA NONA (CLÁUSULA ESPECIAL) – Este contrato poderá ser rescindido depois de 12 meses de locação, desde que avisado com 30 dias de antecedência"
							+ " por escrito, sem oneração de multa contratual. O locatório terá que prestar conta mensalmente do comprovante de condomínio pago para o administrador.");

			claus19.setSpacingAfter(20);
			doc.add(claus19);

			Paragraph claus20 = new Paragraph(
					" CLÁUSULA VIGÉSIMA – Elegem as partes contratantes o foro da Circunscrição Judiciária do Gama – DF, para dirimir as questões oriundas"
							+ " da interpretação ou aplicação deste contrato, com exclusão dos demais, por mais privilegiados que sejam.");

			claus20.setSpacingAfter(20);
			doc.add(claus20);

			Paragraph p2 = new Paragraph(
					"  E assim por estarem justas e convencionadas, as partes assinam o presente instrumento particular de CONTRATO DE LOCAÇÃO,"
							+ " em 02 (duas) vias de igual teor, juntamente com os FIADORES e com as duas testemunhas abaixo, a tudo presentes.");

			p2.setSpacingAfter(20);
			doc.add(p2);

			PdfPTable table1 = new PdfPTable(new float[] { 0.5f, 0.5f });

			table1.setWidthPercentage(100.0f);
			table1.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell header1 = new PdfPCell(new Paragraph(
					con.getPessoa().getLocalidade() + "-" + con.getPessoa().getUf() + "_____/_____/________"));
			table1.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			header1.setBorder(Rectangle.NO_BORDER);
			header1.setHorizontalAlignment(Element.ALIGN_CENTER);
			header1.setColspan(2);

			table1.addCell(header1);

			table1.addCell("");
			table1.addCell("");

			table1.addCell("______________________________");
			table1.addCell("______________________________");

			table1.addCell(usu.getNome());
			table1.addCell(con.getPessoa().getNome());

			table1.addCell("");
			table1.addCell("");

			table1.addCell("______________________________");
			table1.addCell("______________________________");

			table1.addCell("RG:________________");
			table1.addCell("RG:________________");

			table1.addCell("TESTEMUNHA");
			table1.addCell("TESTEMUNHA");

			doc.add(table1);

		} finally {
			if (doc != null) {
				// fechamento do documento
				doc.close();
			}
			if (os != null) {
				// fechamento da stream de saída
				os.close();
			}

		}

		return doc;
	}

	public void validaDiretorio() {

		validaDiretorioRaiz();
		File dir = new File("C:\\Relatorios\\Contratos");

		if (!dir.exists()) {
			dir.mkdirs();
		}

	}

	public void validaDiretorioRaiz() {
		File dir = new File("C:\\Relatorios");

		if (!dir.exists()) {
			dir.mkdirs();
		}

	}

	public void abrirDoc(String doc) {

	}

}