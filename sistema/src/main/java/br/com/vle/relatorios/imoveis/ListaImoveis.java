package br.com.vle.relatorios.imoveis;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import com.ibm.icu.text.SimpleDateFormat;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import br.com.vle.entidade.Imoveis;

public class ListaImoveis {

	public Document gerar(List<Imoveis> ListImoveis) throws Exception {
		Document doc = null;
		FileOutputStream os = null;

		Date data = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
		String dtAtual = formato.format(data);

		try {
			validaDiretorio();

			// cria o documento tamanho A4, margens de 2,54cm
			doc = new Document(PageSize.A4.rotate(), 72, 72, 72, 72);

			// cria a stream de saída
			os = new FileOutputStream("C:\\Relatorios\\Imoveis\\Relatorio_De_Imoveis" + dtAtual.toString() + ".pdf");

			// associa a stream de saída ao
			PdfWriter.getInstance(doc, os);

			// abre o documento
			doc.open();

			// adiciona o texto ao PDF

			Paragraph cabecalho = new Paragraph("RELATORIO DE IMOVEIS " + dtAtual.toString());
			cabecalho.setAlignment(Element.ALIGN_CENTER);
			cabecalho.setSpacingAfter(20);
			doc.add(cabecalho);

			PdfPTable table = new PdfPTable(new float[] { 0.1f, 0.2f, 0.2f, 0.1f, 0.4f });

			table.setWidthPercentage(100.0f);
			table.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setBorder(PdfPCell.RIGHT);

			PdfPCell header = new PdfPCell(new Paragraph(""));
			header.setBorder(Rectangle.NO_BORDER);
			header.setColspan(5);

			table.addCell(header);

			table.addCell("Imovel Nº");
			table.addCell("Tipo");
			table.addCell("Status");
			table.addCell("CEP");
			table.addCell("Endereco");

			for (Imoveis im : ListImoveis) {


				table.addCell(im.getId());
				table.addCell(im.getTpImovel());
				table.addCell(im.getStatusImovel().getStatus());
				table.addCell(formatCep(im.getCep()));
				table.addCell(im.getLogradouro() + " " + im.getNumero() + " " + im.getComplemento() + " " + im.getBairro()
				+ " " + im.getLocalidade() + " " + im.getUf());


			}

			doc.add(table);

		} catch (Exception e) {

			StackTraceElement[] erro = e.getStackTrace();

		} finally {

			if (doc != null) {
				// fechamento do documento
				doc.close();
			}
			if (os != null) {
				// fechamento da stream de saída
				os.close();
			}

		}

		return doc;
	}

	public void validaDiretorio() {

		validaDiretorioRaiz();
		File dir = new File("C:\\Relatorios\\Imoveis");

		if (!dir.exists()) {
			dir.mkdirs();
		}

	}

	public void validaDiretorioRaiz() {
		File dir = new File("C:\\Relatorios");

		if (!dir.exists()) {
			dir.mkdirs();
		}

	}

	public String formatCep(String cep) {
		String retorno;

		retorno = cep.substring(0,5)+"-"+cep.substring(5);

		return retorno;
	}

}
