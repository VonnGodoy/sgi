package br.com.vle.entidade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * @author Victor Godoi
 */

@Entity
@NamedQueries({
    @NamedQuery(name = PerfilAcesso.ALL, query = "select p FROM PerfilAcesso p"),
    @NamedQuery(name = PerfilAcesso.TOTAL, query = "SELECT COUNT(p) FROM PerfilAcesso p")})
public class PerfilAcesso extends BaseEntity<String> implements Serializable {

	private static final long serialVersionUID = 1659657120670180581L;
	
	public final static String ALL = "perfil.todos";
    public final static String TOTAL = "perfil.quantidade";
       
    @Id
    @Column(name = "id_perfil" ,nullable = false, length = 8, unique=true)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private String id;
    
    @Column(nullable = false, length = 255)
    private String perfil;
    
    @Column(length = 255)
    private String descricao;
   
    
    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "perfilroles", joinColumns = {
    		@JoinColumn(name = "perfil", referencedColumnName= "id_perfil")
    	}, 
    	inverseJoinColumns = { 
			@JoinColumn(name = "role", referencedColumnName="id_role")
		}
    )
    private List<Role> roles;
    
    @Transient
    private Role rolePai;

    public PerfilAcesso(String id, String descricao) {
        this.roles = new ArrayList<Role>();
        this.descricao = descricao;
        this.id = id;
    }
    
    public PerfilAcesso() {
    	this.roles = new ArrayList<Role>();
    }


	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}
	
	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public Role getRolePai() {
		return rolePai;
	}

	public void setRolePai(Role rolePai) {
		this.rolePai = rolePai;
	}

	public String getRolesdesc(){
		String st = "";
		for (Role role : getRoles()) {
			st+= " "+role.getRole();
		}
		return st;
	}
	
}