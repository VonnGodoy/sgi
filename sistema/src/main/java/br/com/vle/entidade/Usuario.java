package br.com.vle.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 * @author Victor Godoi
 */

@Entity
@NamedQueries({
    @NamedQuery(name = Usuario.ALL, query = "select u FROM Usuario u where u.ativo = true "),
    @NamedQuery(name = Usuario.TOTAL, query = "SELECT COUNT(u) FROM Usuario u")})
public class Usuario extends BaseEntity<String> implements Serializable {

	private static final long serialVersionUID = -1244864113615967277L;
	
	public final static String ALL = "usuario.todos";
    public final static String TOTAL = "usuario.quantidade";

//    @OneToOne(fetch=FetchType.EAGER)
//    @JoinColumn(name = "id_pessoa")
//    private Pessoa pessoa;
       
    @Id
    @Column(name="id_usuario",nullable = false, unique=true, length = 14)
    private String id;
    
    @Column(length = 164, nullable = false)
    private String password;
    
    @Column(nullable = false)
    private Boolean ativo;
    
    @Column(name = "primeiro_acesso" ,nullable = false)
    private Boolean primeiroAcesso;
    
   @OneToOne(fetch=FetchType.EAGER)
   @JoinColumn(name = "id_perfil")
    private PerfilAcesso perfilAcesso;
    
   
    public Usuario() {
        perfilAcesso = new PerfilAcesso();
        ativo = Boolean.TRUE;
    }
    
//	public Pessoa getPessoa() {
//		return pessoa;
//	}

//	public void setPessoa(Pessoa pessoa) {
//		this.pessoa = pessoa;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public PerfilAcesso getPerfilAcesso() {
		return perfilAcesso;
	}


	public void setPerfilAcesso(PerfilAcesso perfilAcesso) {
		this.perfilAcesso = perfilAcesso;
	}

	public Boolean getAtivo() {
		return ativo;
	}


	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getPrimeiroAcesso() {
		return primeiroAcesso;
	}


	public void setPrimeiroAcesso(Boolean primeiroAcesso) {
		this.primeiroAcesso = primeiroAcesso;
	}    

}