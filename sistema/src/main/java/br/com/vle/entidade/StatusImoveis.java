package br.com.vle.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * @author Victor Godoi
 */

@Entity
@NamedQueries({
    @NamedQuery(name = StatusImoveis.ALL, query = "select s FROM StatusImoveis s ")})
public class StatusImoveis extends BaseEntity<String> implements Serializable {

	private static final long serialVersionUID = -1244864113615967277L;
	public final static String ALL = "StatusImoveis.todos";
	
    @Id
    @Column(name = "id_status_imovel", nullable = false, unique=true)
    private String id;
    
    @Column
    private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String dsStatusImovel) {
		this.status = dsStatusImovel;
	}
    
}