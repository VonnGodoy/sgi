package br.com.vle.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import br.com.vle.enuns.TipoImovelEnum;

/**
 * @author Victor Godoi
 */

@Entity
@NamedQueries({
	@NamedQuery(name = Imoveis.ALL, query = "select u FROM Imoveis u ")})
public class Imoveis extends BaseEntity<String> implements Serializable {


	private static final long serialVersionUID = -1244864113615967277L;
	public final static String ALL = "imoveis.todos";

	@Id
	@Column(name="id_imovel", unique=true, nullable=false, length = 8)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String id;
	
	@Column(name="tp_imovel",nullable = false)
	private String tpImovel;

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_status_imovel",nullable = false)
	private StatusImoveis statusImovel;
	
	@Column(name="ds_imovel",nullable = false, length = 2000)
	private String dsImovel;
	
	@Column(name="ds_observacoes",nullable = true, length = 2000)
	private String dsObservacoes;
	
	@Column(nullable = false, length = 8)
	private String cep;
	
	@Column(nullable = false, length = 60)
	private String logradouro;
	
	@Column(nullable = true, length = 60)
	private String complemento;
	
	@Column(nullable = false, length = 10)
	private String numero;
	
	@Column(nullable = false, length = 40)
	private String bairro;
	
	@Column(nullable = false, length = 30)
	private String localidade;
	
	@Column(nullable = false, length = 2)
	private String uf;
	
	public String getId() {
		return id;
	}
	public void setId(String idImovel) {
		this.id = idImovel;
	}
	public StatusImoveis getStatusImovel() {
		return statusImovel;
	}
	public void setStatusImovel(StatusImoveis statusImovel) {
		this.statusImovel = statusImovel;
	}
	public String getDsImovel() {
		return dsImovel;
	}
	public void setDsImovel(String dsImovel) {
		this.dsImovel = dsImovel;
	}
	public String getDsObservacoes() {
		return dsObservacoes;
	}
	public void setDsObservacoes(String dsObservacoes) {
		this.dsObservacoes = dsObservacoes;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getLocalidade() {
		return localidade;
	}
	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getTpImovel() {
		TipoImovelEnum[] tipo = TipoImovelEnum.values();
		if(this.tpImovel != null && this.tpImovel.trim().length() == 1) {
			
			for(TipoImovelEnum sta: tipo) {
				if(sta.getId().toString().equals(this.tpImovel.toString())) {
					this.tpImovel = sta.getTipo().toString();
					break;
				}
			}
		}
		
		return tpImovel;
	}
	public void setTpImovel(String tpImovel) {
		
		TipoImovelEnum[] tipo = TipoImovelEnum.values();
		if(tpImovel != null) {
			
			if(tpImovel.trim().length() == 1) {
				this.tpImovel = tpImovel;
				
			}else{
			for(TipoImovelEnum sta: tipo) {
				if(sta.getTipo().toString().equals(tpImovel.toString())) {
					this.tpImovel = sta.getId().toString();
					break;
				}
			}
		}
		}
	}
	
	
}
