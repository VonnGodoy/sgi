package br.com.vle.entidade;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import br.com.vle.enuns.SituacaoFinancasEnum;

/**
 * @author Victor Godoi
 */

@Entity
@NamedQueries({
	@NamedQuery(name = Financeiro.ALL, query = "select s FROM Financeiro s ")})
public class Financeiro extends BaseEntity<String> implements Serializable {

	private static final long serialVersionUID = -1244864113615967277L;
	public final static String ALL = "Financeiro.todos";

	@Id
	@Column(name="id_financeiro", unique=true, nullable=false, length = 8)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String id;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_contrato",nullable = false)
	private Contrato contrato;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_pessoa",nullable = false)
	private Pessoa pessoa;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_imovel",nullable = false)
	private Imoveis imovel;	

	@Column(name="mes",nullable = false)
	private String  mes;

	@Column(name="dia_vencimento",nullable = false, length = 2)
	private String diaVencimento;

	@Column(name="valor_mensalidade",nullable = false, length = 6 , precision = 2)
	private BigDecimal valorMensalidade;

	@Column(name="tx_multa_atrazo",nullable = false, length = 5 , precision = 2)
	private BigDecimal txMultaAtraso;

	@Column(name="total_mes",length = 6 , precision = 2)
	private BigDecimal total_mes;

	@Column(name="status",nullable = true)
	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Imoveis getImovel() {
		return imovel;
	}

	public void setImovel(Imoveis imovel) {
		this.imovel = imovel;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(String diaVencimento) {
		this.diaVencimento = diaVencimento;
	}

	public BigDecimal getValorMensalidade() {
		return valorMensalidade;
	}

	public void setValorMensalidade(BigDecimal valorMensalidade) {
		this.valorMensalidade = valorMensalidade;
	}

	public BigDecimal getTxMultaAtraso() {
		return txMultaAtraso;
	}

	public void setTxMultaAtraso(BigDecimal txMultaAtraso) {
		this.txMultaAtraso = txMultaAtraso;
	}

	public BigDecimal getTotal_mes() {
		return total_mes;
	}

	public void setTotal_mes(BigDecimal total_mes) {
		this.total_mes = total_mes;
	}

	public String getStatus() {
		SituacaoFinancasEnum[] lista = SituacaoFinancasEnum.values();

		if(status.trim().length() == 1) {
			for(SituacaoFinancasEnum s : lista) {

				if(s.getId().toString().equals(this.status)) {

					this.status = s.getStatus();
				}

			}
		}
		return status;

	}

	public void setStatus(String status) {
		SituacaoFinancasEnum[] lista = SituacaoFinancasEnum.values();

		if(status.trim().length() == 1) {

			this.status = status;
		}else{
			for(SituacaoFinancasEnum s : lista) {

				if(s.getStatus().toString().equals(status)) {

					this.status = s.getId().toString();
				}

			}
		}
	}


}