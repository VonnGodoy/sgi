package br.com.vle.entidade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * @author Victor Godoi
 */

@Entity
@NamedQueries({
    @NamedQuery(name = Pessoa.ALL, query = "select u FROM Pessoa u ")})
public class Pessoa extends BaseEntity<String> implements Serializable {


	private static final long serialVersionUID = -1244864113615967277L;
	public final static String ALL = "pessoa.todos";
	public final static String DOC = "pessoa.pordoc";

	@Column(name="id_pessoa" , unique=true, nullable=false, length = 8)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;

	@Id
    @Column(name="cpf_cnpj",nullable = false, length = 14)
	private String cpfCnpj;
    
    @Column(name="ie",nullable = true, length = 16)
    private String ie;
    
    @Column(name="nome",nullable = false, length = 80)
	private String nome;
    
    @Column(name="rg",nullable = true, length = 16)
	private String rg;
    
    @Column(name="ssp",nullable = false, length = 10)
    private String ssp;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dt_nasc",nullable = true)
	private Date dtNascimento;
    
    @Column(name="email",nullable = false, length = 60)
	private String email;
    
    @Column(name="tel_princ",nullable = false, length = 11)
	private String telPrin;
    
    @Column(name="tel_sec",nullable = true, length = 11)
	private String telSec;
    
    @Column(name="cep",nullable = false, length = 8)
	private String cep;
    
    @Column(name="logradouro",nullable = false, length = 60)
	private String logradouro;
    
    @Column(name="complemento",nullable = true, length = 60)
	private String complemento;
    
    @Column(name="numero",nullable = true, length = 10)
	private String numero;
    
    @Column(name="bairro",nullable = false, length = 40)
	private String bairro;
    
    @Column(name="localidade",nullable = false, length = 30)
	private String localidade;
    
    @Column(name="uf",nullable = false, length = 2)
	private String uf;
    
    @Column(name="ref_nome",nullable = false, length = 60)
	private String refNome;
    
    @Column(name="ref_tel",nullable = false, length = 11)
	private String refTel;
    
    @Column(name="ref_nome1",nullable = true, length = 60)
	private String refNome1;
    
    @Column(name="ref_tel1",nullable = true, length = 11)
	private String refTel1;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dt_cadastro",nullable = false)
	private Date dtCadastro;
    
    @Column(name = "ativo" ,nullable = false)
    private Boolean ativo;
    
	public String getId(){
		return this.id;
	}

	public void setId(String idPessoa){
		this.id = idPessoa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpfCnpj() {
		
		while(this.cpfCnpj.trim().length() <= 10) {
			this.cpfCnpj = "0" + cpfCnpj.toString();
			
		}
		
		while(this.cpfCnpj.trim().length() > 11 && this.cpfCnpj.trim().length() <=13) {
			this.cpfCnpj = "0" + cpfCnpj.toString();
			
		}
		
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getSsp() {
		return ssp;
	}

	public void setSsp(String ssp) {
		this.ssp = ssp;
	}
	
	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelPrin() {
		return telPrin;
	}

	public void setTelPrin(String telPrin) {
		this.telPrin = telPrin;
	}

	public String getTelSec() {
		return telSec;
	}

	public void setTelSec(String telSec) {
		this.telSec = telSec;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		if(this.complemento == null) {
			this.complemento = "";
		}
		
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getRefNome() {
		return refNome;
	}

	public void setRefNome(String refNome) {
		this.refNome = refNome;
	}

	public String getRefTel() {
		return refTel;
	}

	public void setRefTel(String refTel) {
		this.refTel = refTel;
	}

	public String getRefNome1() {
		return refNome1;
	}

	public void setRefNome1(String refNome1) {
		this.refNome1 = refNome1;
	}
	
	public String getRefTel1() {
		return refTel1;
	}

	public void setRefTel1(String refTel1) {
		this.refTel1 = refTel1;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}