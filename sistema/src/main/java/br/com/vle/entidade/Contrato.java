package br.com.vle.entidade;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import br.com.vle.enuns.SituacaoContratoEnum;
import br.com.vle.enuns.TipoContratosEnum;

/**
 * @author Victor Godoi
 */

@Entity
@NamedQueries({
	@NamedQuery(name = Contrato.ALL, query = "select s FROM Contrato s ")})
public class Contrato extends BaseEntity<String> implements Serializable {

	private static final long serialVersionUID = -1244864113615967277L;
	public final static String ALL = "ContratoAluguel.todos";

	@Id
	@Column(name="id_contrato", unique=true, nullable=false, length = 8)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String id;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_pessoa",nullable = false)
	private Pessoa pessoa;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_imovel",nullable = false)
	private Imoveis imovel;

	@Column(name="periodo",nullable = false)
	private String   periodo;

	@Column(name="validade",nullable = false)
	private String   validade;

	@Column(name="dia_vencimento",nullable = false, length = 2)
	private String diaVencimento;

	@Column(name="valor_mensalidade",nullable = false, length = 6 , precision = 2)
	private BigDecimal valorMensalidade;

	@Column(name="tx_multa_atrazo",nullable = false, length = 5 , precision = 2)
	private BigDecimal txMultaAtraso;

	@Column(name="contrato_status",nullable = false)
	private String contratoStatus;

	@Column(name="tp_contrato",nullable = false)
	private String tpcontrato;

	@Column(name="resp_despesas",nullable = false, length = 1)
	private String respDespesas;


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public Imoveis getImovel() {
		return imovel;
	}
	public void setImovel(Imoveis imovel) {
		this.imovel = imovel;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getValidade() {
		return validade;
	}
	public void setValidade(String val) {
		this.validade = val;
	}
	public String getDiaVencimento() {
		return diaVencimento;
	}
	public void setDiaVencimento(String diaVencimento) {
		this.diaVencimento = diaVencimento;
	}
	public BigDecimal getValorMensalidade() {

		//		if(this.valorMensalidade != null) {

		//		String valor = this.valorMensalidade.toString()+".00";
		//		
		//		this.valorMensalidade = new BigDecimal(valor);
		//		}
		return valorMensalidade;
	}
	public void setValorMensalidade(BigDecimal valorMensalidade) {
		this.valorMensalidade = valorMensalidade;
	}
	public BigDecimal getTxMultaAtraso() {
		return txMultaAtraso;
	}
	public void setTxMultaAtraso(BigDecimal txMultaAtraso) {
		this.txMultaAtraso = txMultaAtraso;
	}
	public String getContratoStatus() {

		SituacaoContratoEnum[] sit = SituacaoContratoEnum.values();

		for(SituacaoContratoEnum s : sit) {

			if(s.getId().toString().equals(this.contratoStatus.toString())) {

				this.contratoStatus = s.getSituacao();
				break;
			}

		}

		return contratoStatus;
	}

	public void setContratoStatus(String contratoStatus) {

		SituacaoContratoEnum[] sit = SituacaoContratoEnum.values();

		if(contratoStatus.toString().trim().length() > 1) {

			for(SituacaoContratoEnum s : sit) {

				if(s.getSituacao().toString().equals(this.contratoStatus.toString())) {

					this.contratoStatus = s.getId();
					break;
				}

			}
		}else {

			this.contratoStatus = contratoStatus;
		}
	}

	public String getTpcontrato() {
		TipoContratosEnum[] list = TipoContratosEnum.values();

		for(TipoContratosEnum tipo: list) {
			if(this.tpcontrato.equals(tipo.getId())) {
				this.tpcontrato = tipo.getTipo();
				break;
			}
		}

		return tpcontrato;
	}
	public void setTpcontrato(String tpcontrato) {
	
		if(tpcontrato.toString().trim().length() > 1) {
			TipoContratosEnum[] list = TipoContratosEnum.values();

			for(TipoContratosEnum tipo: list) {
				if(this.tpcontrato.equals(tipo.getTipo())) {
					this.tpcontrato = tipo.getId();
					break;
				}
			}
		
	}else {
		this.tpcontrato = tpcontrato;
	}
	}
	public String getRespDespesas() {
		return respDespesas;
	}
	public void setRespDespesas(String respDespesas) {
		this.respDespesas = respDespesas;
	}

}
