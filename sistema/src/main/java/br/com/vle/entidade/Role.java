package br.com.vle.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import br.com.vle.enuns.RolesEnum;

/**
 * @author Victor Godoi
 */

@Entity
@NamedQueries({@NamedQuery(name = Role.ALL, query = "SELECT r FROM Role r")})
public class Role extends BaseEntity<String> implements Serializable {

	private static final long serialVersionUID = -8321287957871843232L;

	public final static String ALL = "Role.populateRoles";
   
    @Id
	@Column(name="id_role" , unique=true, nullable=false, length = 8)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    
    @Column
    private String role;
    
    @Column
    private String roleDesc;
    
    @Column
    private String rolePai;
    

    public Role() {
    }

    public Role(String roleid, String role) {
    	this.id = roleid;
    	this.role = role;
    }
    
    public Role(String roleid,String role, String roledesc, String rolePai) {
        this.id = roleid;
        this.role = role;
        this.roleDesc = roledesc;
        this.rolePai = rolePai;
    }
    
    public Role(RolesEnum role) {
        this.role = role.getNome();
        this.roleDesc = role.getDescricao();
        if(role.getPai()!=null){
        	this.rolePai = role.getPai().getNome();
        }
    }

  
    public String getRoledesc() {
        return this.roleDesc;
    }

    public void setRoledesc(String roledesc) {
        this.roleDesc = roledesc;
    }
    
    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRolename() {
        return this.role;
    }

	public String getRolePai() {
		return rolePai;
	}

	public void setRolePai(String rolePai) {
		this.rolePai = rolePai;
	}
	
	

}