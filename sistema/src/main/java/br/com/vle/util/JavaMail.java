package br.com.vle.util;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import br.com.vle.model.ModelEmail;

public class JavaMail {
	
      public String Enviar(ModelEmail email){
    	  String retorno = "";
    	  
            Properties props = MontaSMTP(email);

           Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
           		
        	   protected PasswordAuthentication getPasswordAuthentication() {
       		
       		return new PasswordAuthentication(email.getRemetente(),email.getPassword());      
           		}
       	});

            /** Ativa Debug para sessão */
            session.setDebug(true);

            try {

                  Message message = new MimeMessage(session);
                  message.setFrom(new InternetAddress(email.getRemetente())); //Remetente

                  Address[] toUser = InternetAddress //Destinatário(s)
                             .parse(email.getDestinatario());  

                  message.setRecipients(Message.RecipientType.TO, toUser);
                  message.setSubject(email.getAssunto());//Assunto
                  message.setText(email.getMensagem());//Mensagem
                  /**Método para enviar a mensagem criada*/
                  Transport.send(message);

                  retorno = "Email Com Sucesso";

             } catch (MessagingException e) {
                  retorno = "Erro ao enviar E-mail";
            }
            return retorno;
      }

	public Properties MontaSMTP(ModelEmail email) {
    	  
    	  Properties props = new Properties();

    	  if (email.getDestinatario().toString().contains("GMAIL")) {
          
          /** Parâmetros de conexão com servidor Gmail */
          props.put("mail.smtp.host", "smtp.gmail.com");
          props.put("mail.smtp.socketFactory.port", "465");
          props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
          props.put("mail.smtp.auth", "true");
          props.put("mail.smtp.port", "465");
          
    	  } else if (email.getDestinatario().toString().contains("HOTMAIL")) {

          /** Parâmetros de conexão com servidor Hotmail */
          props.put("mail.transport.protocol", "smtp");
          props.put("mail.smtp.host", "smtp.live.com");
          props.put("mail.smtp.socketFactory.port", "587");
          props.put("mail.smtp.socketFactory.fallback", "false");
          props.put("mail.smtp.starttls.enable", "true");
          props.put("mail.smtp.auth", "true");
          props.put("mail.smtp.port", "587");
          
    	  } else if (email.getDestinatario().toString().contains("OUTLOOK")) {

    	  /** Parâmetros de conexão com servidor Outlook */
    	  props.put("mail.smtp.auth", "true");
    	  props.put("mail.smtp.starttls.enable", "true");
    	  props.put("mail.smtp.host", "outlook.office365.com");
    	  props.put("mail.smtp.port", "587");

    	  } else if (email.getDestinatario().toString().contains("YAHOO")) {
    	  
    	  /** Parâmetros de conexão com servidor yahoo */
    	  props.put("mail.transport.protocol", "smtp");
    	  props.put("mail.smtp.host", "smtp.mail.yahoo.com");
    	  props.put("mail.smtp.socketFactory.port", "465");
    	  props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    	  props.put("mail.smtp.starttls.enable", "true");
    	  props.put("mail.smtp.auth", "true");
    	  props.put("mail.smtp.port", "587");
    	  
    	  }
    	  
    	 return props; 
      }
	
}