package br.com.vle.util;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * status boolean conversion utility class
 * @author Victor Godoi
 */

@FacesConverter("br.com.vle.util.StatusConverter")
public class StatusConverter implements Converter {
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value != null && !value.trim().equals("")) {

			if (value.equalsIgnoreCase(getBundleMsg("ativo", null)) || 
					value.equalsIgnoreCase(getBundleMsg("sim", null)) ||
					value.equalsIgnoreCase("true")) {
				
				return Boolean.TRUE;
				
			} else if (value.equalsIgnoreCase(getBundleMsg("inativo", null))
					|| value.equalsIgnoreCase(getBundleMsg("nao", null))
					|| value.equalsIgnoreCase("false")) {
				
				return Boolean.FALSE;
				
			}
			
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		Boolean object = (Boolean) value;
		String valor = "";
		if (object != null) {
			
			if(object){
				valor =  getBundleMsg("ativo", null);
			}else{
				valor =  getBundleMsg("inativo", null);
			}
			
		}
		return valor ;
	}
	
	protected String getBundleMsg(String msg, String... parametros) {
		ResourceBundle bundle = getContext().getApplication().getResourceBundle(
				getContext(), "msg");
		String msgbundle = msg;
		if (bundle.containsKey(msg)) {
			msgbundle = bundle.getString(msg);
			if (parametros != null) {
				msgbundle = MessageFormat.format(msgbundle, parametros);
			}
		}
		return msgbundle;
	}
	
	private FacesContext getContext() {
		FacesContext context = FacesContext.getCurrentInstance();
		return context;
	}
}
