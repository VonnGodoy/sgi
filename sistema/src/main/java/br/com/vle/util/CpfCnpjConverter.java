	package br.com.vle.util;

	import javax.faces.component.UIComponent;
	import javax.faces.context.FacesContext;
	import javax.faces.convert.Converter;
	import javax.faces.convert.FacesConverter;

	@FacesConverter("CpfCnpjConverter")
	public class CpfCnpjConverter implements Converter {
		
		private static final int CPF = 11; 
		private static final int CNPJ = 14; 

	    @Override
	    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	        String[] parts = value.split("-\\.");
	        return Integer.parseInt(join(parts));
	    }

	    @Override
	    public String getAsString(FacesContext context, UIComponent component, Object value) {
	    	
	        String valueAsString = value.toString();
	        String retorno = valueAsString;
	        
	        if(valueAsString.trim().length() == CPF) {
	        	
	        	retorno = formatCPF(valueAsString.substring(0, 3), valueAsString.substring(3, 6), valueAsString.substring(6,9), valueAsString.substring(9));
	        } else if(valueAsString.trim().length() == CNPJ){
	        	
	        	retorno  = formatCNPJ(valueAsString.substring(0, 2), valueAsString.substring(2, 5), valueAsString.substring(5,8), valueAsString.substring(8,12), valueAsString.substring(12));
	        }
	        return retorno;
	    }

	    private String formatCPF(String part1, String part2, String part3, String part4) {
	        return part1.concat(".").concat(part2).concat(".").concat(part3).concat("-").concat(part4);
	    }
	    
	    private String formatCNPJ(String part1, String part2, String part3, String part4, String part5) {
	        return part1.concat(".").concat(part2).concat(".").concat(part3).concat("/").concat(part4).concat("-").concat(part5);
	    }

	    private String join(String[] parts) {
	        StringBuilder sb = new StringBuilder();

	        for(String part : parts) {
	            sb.append(part);
	        }

	        return sb.toString();
	    }
	}
