package br.com.vle.util;

import java.io.Serializable;
import java.util.HashMap;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.vle.entidade.BaseEntity;

/**
 *
 * @author Victor Godoi
 */
 
@FacesConverter("br.com.vle.util.ObjectConverter")
public class ObjectConverter implements Converter {

    private static HashMap<Serializable, Object> map = new HashMap<Serializable, Object>();
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return map.get(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    	BaseEntity<?> object = (BaseEntity<?>) value;
    	if(value!=null && object.getId()!=null ){
    		map.put(object.getId(), object);
    		return object.getId().toString();
    	}
    	return "";
    }
}
