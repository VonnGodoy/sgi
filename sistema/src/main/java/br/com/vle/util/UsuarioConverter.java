package br.com.vle.util;

import java.io.Serializable;
import java.util.HashMap;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.vle.entidade.Usuario;

/**
 *
 * @author Victor Godoi
 */

@FacesConverter("br.com.vle.util.UsuarioConverter")
public class UsuarioConverter implements Converter {

	private static HashMap<Serializable, Serializable> map2 = new HashMap<Serializable, Serializable>();
	private static HashMap<Serializable, Usuario> map = new HashMap<Serializable, Usuario>();

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Usuario obj = map.get(value);
		if (obj == null) {
			obj = map.get(map2.get(value));
		}
		return obj;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Usuario) {
			Usuario object = (Usuario) value;
			if( object.getId() != null){
				map.put(object.getId(), object);
				map2.put("", object.getId());
				//map2.put(object.getNomeCompleto(), object.getId());
				return object.getId().toString();
			}
		}
		return "";
	}
}
