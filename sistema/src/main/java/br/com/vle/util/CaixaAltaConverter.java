package br.com.vle.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Victor Godoi
 */

@FacesConverter("br.com.vle.util.CaixaAltaConverter")
public class CaixaAltaConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(value != null && !value.trim().equals("")){
        	return value.trim().toUpperCase();
        }
    	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    	String object = (String) value;
    	 if(object != null && !object.trim().equals("")){
         	return object.trim().toUpperCase();
         }
     	return "";
    }
}
