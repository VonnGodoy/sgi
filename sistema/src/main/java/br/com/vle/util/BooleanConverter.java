package br.com.vle.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Victor Godoi
 */

@FacesConverter("br.com.vle.util.BooleanConverter")
public class BooleanConverter extends StatusConverter {

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Boolean object = (Boolean) value;
		String valor = "";
		if (object != null) {
			
			if(object){
				valor = getBundleMsg("sim", null);
			}else{
				valor =  getBundleMsg("nao", null);
			}
			
		}
		return valor;
	}
}
