package br.com.vle.util.ws;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import br.com.vle.model.ModelEndereco;

/**
 *
 * @author Victor Godoi
 */

public class EnderecoWS {


	private static String WEB_SERVICE = "https://viacep.com.br/ws/" ;
	
	private static String RETONO_JSON = "/json" ;
	
	private static String RETONO_XML = "/xml" ;

	private static boolean CONEXAO = true ;
	
	private static final Set<String> CAMPOS = new HashSet<String>(Arrays.asList(
	    "cep",
	    "logradouro",
	    "complemento",
	    "bairro",
	    "localidade",
	    "uf",
	    "unidade",
	    "ibge",
	    "gia"
	    ));

	    /**
	     * Recupera objeto Endereco pelo CEP
	     * @param cep String no formato 00000000
	     * @return instancia de br.com.viacep.Endereco
	     * 
	     */
	public static ModelEndereco getEnderecoPorCep(String cep) {

		JsonObject jsonObject = getCepResponse(cep);
		ModelEndereco endereco = null;

		if (jsonObject !=null) {

			JsonValue erro = jsonObject.get("erro");

			if (erro == null) {

				endereco = new ModelEndereco()
						.setCep(jsonObject.getString("cep"))
						.setLogradouro(jsonObject.getString("logradouro"))
						.setComplemento(jsonObject.getString("complemento"))
						.setBairro(jsonObject.getString("bairro"))
						.setLocalidade(jsonObject.getString("localidade"))
						.setUf(jsonObject.getString("uf"))
						.setUnidade(jsonObject.getString("unidade"))
						.setIbge(jsonObject.getString("ibge"))
						.setGia(jsonObject.getString("gia"));

			}
		}else if(!CONEXAO){
			endereco = new ModelEndereco().setStatusConexao(CONEXAO);
		}

		return endereco;
	}

	    /**
	     * Recupera Map<String,String> pelo CEP
	     * @param cep String no formato 00000000
	     * @return instancia de Map<String,String>
	     */
	    public static Map<String, String> getMapPorCep(String cep) {

	        JsonObject jsonObject = getCepResponse(cep);

	        JsonValue erro = jsonObject.get("erro");

	        Map<String, String> mapa = null;
	        if (erro == null) {
	            mapa = new HashMap<String, String>();

	            for (Iterator<Map.Entry<String,JsonValue>> it = jsonObject.entrySet().iterator(); it.hasNext();) {
	                Map.Entry<String,JsonValue> entry = it.next();
	                mapa.put(entry.getKey(), entry.getValue().toString());
	            }
	        }

	        return mapa;
	    }

	    private static JsonObject getCepResponse(String cep) {

	        JsonObject responseJO = null;

	    try {

	            HttpClient client = HttpClientBuilder.create().build(); 
	            HttpGet httpGet = new HttpGet(WEB_SERVICE+""+cep+""+RETONO_JSON); 
	            HttpResponse response = client.execute(httpGet);

	            HttpEntity entity = response.getEntity();

	            responseJO = Json.createReader(entity.getContent()).readObject();

	        } catch (Exception e) {
	        		
	        	CONEXAO = false;
	        }

	        return responseJO;
	    }

	}
