package br.com.vle.util;

/**
 *
 * @author Victor Godoi
 */

public class ValidatorUtils {

	private static int CPF = 11;
	private static int CNPJ = 14;

	public static boolean isValidCEP(String Cep) {
		boolean retorno = false;
		if (isValidNumbers(Cep)) {
			String padrao = "^\\d{5}[-]\\d{3}$";

			if (Cep.matches(padrao)) {
				retorno = false;

			} else {
				retorno = true;
			}
		} else {
			retorno = false;
		}
		return retorno;
	}
		
	

	public static boolean isValidNumbers(String number) {
		char[] digitos = number.toString().toCharArray();
		for (int i = 1; i < digitos.length; i++) {
			if (digitos[0] != digitos[i])
				return true;
		}
		return false;
	}

	public static boolean isValidDocument(String number) {

		if (number !=null && isValidNumbers(number)) {

			int soma = 0;

			try {
				Long.parseLong(number);

			} catch (Exception e) {
				return false;
			}
			if (number.trim().length() == CPF) {

				for (int i = 0; i < 9; i++) {
					soma += (10 - i) * (number.charAt(i) - '0');
				}
				soma = 11 - soma % 11;
				if (soma > 9) {
					soma = 0;
				}
				if (soma == number.charAt(9) - '0') {
					soma = 0;
					for (int i = 0; i < 10; i++) {
						soma += (11 - i) * (number.charAt(i) - '0');
					}
					soma = 11 - soma % 11;
					if (soma > 9) {
						soma = 0;
					}
					if (soma == number.charAt(10) - '0') {
						return true;
					}
				}

			} else if (number.trim().length() == CNPJ) {

				int i = 0;
				for (int j = 5; i < 12; i++) {
					soma += j-- * (number.charAt(i) - '0');
					if (j < 2) {
						j = 9;
					}
				}
				soma = 11 - soma % 11;
				if (soma > 9) {
					soma = 0;
				}
				if (soma == number.charAt(12) - '0') {
					soma = 0;
					int k = 0;
					for (int j = 6; k < 13; k++) {

						soma += j-- * (number.charAt(k) - '0');
						if (j < 2) {
							j = 9;
						}
					}
					soma = 11 - soma % 11;
					if (soma > 9) {
						soma = 0;
					}
					if (soma == number.charAt(13) - '0') {
						return true;
					}
				}

			}
			return false;

		} else {
			return false;

		}

	}

}
