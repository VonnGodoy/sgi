package br.com.vle.model.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Victor Godoi
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UsuarioDTO implements Serializable{
	
    private String id;
    private String nome;
    

	public UsuarioDTO() {
		super();
	}

	public UsuarioDTO(String id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
    
    

}
