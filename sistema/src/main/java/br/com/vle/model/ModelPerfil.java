package br.com.vle.model;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;

import br.com.vle.controller.perfilacesso.DataModelPerfilAcesso;
import br.com.vle.controller.perfilacesso.Eventos;
import br.com.vle.controller.perfilacesso.ProducePerfilAcessoSelecionado;
import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Role;
import br.com.vle.servico.dao.DaoPerfilAcesso;

/**
 *
 * @author Victor Godoi
 */

@SuppressWarnings("serial")
@Named
@ConversationScoped
public class ModelPerfil implements Serializable {

	
	@Inject
	private Conversation conversation;
	
	@Inject
	private transient Logger logger;
	
	@Inject
	private DaoPerfilAcesso servicePerfilAcesso;
	
	private PerfilAcesso perfilSelecionado;
	
	private PerfilAcesso[] selecionados;
	
	private List<Role> roles;
	
	private String nomePerfilConsulta;
	
	private LazyDataModel<PerfilAcesso> lazyModel;
	
	private Integer pageSelecionada;
	
	private Integer qtdRows = 15;
	
	
	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=  Start ModelPerfil  ==");
		logger.info("=  conversation = "+conversation.getId());
		
		if(conversation.isTransient()){
			logger.info("=  conversation begin             ==");
			conversation.begin();
			//1 hora
			conversation.setTimeout(60 * 60 * 1000);
		}
		
		logger.info("=  conversation = "+conversation.getId());
		logger.info("====================================");
		
		lazyModel = new DataModelPerfilAcesso(this, servicePerfilAcesso);
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("========  Stop ModelPerfil  ========");
		
		if(!conversation.isTransient()){
			logger.info("==  conversation end             ==");
			conversation.end();
		}
		
		logger.info("====================================");
	}
	
	public PerfilAcesso getPerfilSelecionado() {
		return perfilSelecionado;
	}
	
	@Produces
	@Named("perfilSelecionado")
	@RequestScoped
	@ProducePerfilAcessoSelecionado
	public PerfilAcesso getPerfilEscolhido() {
		if (perfilSelecionado == null) {
			return  new PerfilAcesso();
		}
		return perfilSelecionado;
	}
	
	
	

	public void setPerfilSelecionado(PerfilAcesso perfilSelecionado) {
		this.perfilSelecionado = perfilSelecionado;
	}

	public String getNomePerfilConsulta() {
		return nomePerfilConsulta;
	}

	public void setNomePerfilConsulta(String nomePerfilConsulta) {
		this.nomePerfilConsulta = nomePerfilConsulta;
	}
	
	public LazyDataModel<PerfilAcesso> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<PerfilAcesso> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public Integer getPageSelecionada() {
		return pageSelecionada;
	}

	public void setPageSelecionada(Integer pageSelecionada) {
		this.pageSelecionada = pageSelecionada;
	}

	public PerfilAcesso[] getSelecionados() {
		return selecionados;
	}

	public void setSelecionados(PerfilAcesso[] selecionados) {
		this.selecionados = selecionados;
	}
	
	public Integer getQtdRows() {
		return qtdRows;
	}

	public void setQtdRows(Integer qtdRows) {
		this.qtdRows = qtdRows;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public void listenEvento(@Observes Eventos evento){
	    	logger.info("=====================================");
			logger.info("====  @Observes PerfilController ====");
			logger.info("=  perfil: " + evento.getPerfilAcesso());
	        logger.info("=====================================");
	        setPerfilSelecionado(evento.getPerfilAcesso());
	    }

	
	    
	

}
