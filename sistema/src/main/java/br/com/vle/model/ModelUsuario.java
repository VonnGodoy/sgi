package br.com.vle.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;

import br.com.vle.controller.pessoa.DataModelPessoa;
import br.com.vle.controller.usuario.DataModelUsuario;
import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Usuario;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.enuns.EstadosEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.model.dto.UsuarioPessoaDTO;
import br.com.vle.servico.dao.DaoPerfilAcesso;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */

@Named
@ConversationScoped
public class ModelUsuario implements Serializable {

	private static final long serialVersionUID = 1110248501652831575L;
	
	@Inject
	private Conversation conversation;
	
	@Inject
	private transient Logger logger;
	
	private LazyDataModel<Usuario> lazyModel;
	
	private Usuario usuarioSelecionado;
	
	private List<Usuario> selecionados;
	
	@Inject
	private DaoUsuario daoUsuario;
	
	@Inject
	private DaoPerfilAcesso daoPerfil;

	@Inject
	private DaoPessoa daoPessoa;
	
	@Inject
	private ModelPessoa modelPessoa;

	private Usuario newUsuario;
	
	private Pessoa pessoa = new Pessoa();

	private UsuarioPessoaDTO usuarioPessoa;

	private List<UsuarioPessoaDTO> listaUsuarioPessoa;
	
	private List<PerfilAcesso> perfis;
	
	private List<Usuario> listUsuario;

	private List<Pessoa> listPessoa;
	
	private Integer pageSelecionada;
	
	private Integer qtdRows = 10;
	
	private String id;
	
	private String nome;
	
	private String email;
	
	private String cpfCnpj;
	
	private String rg;
	
	private String ie;
	
	private EstadosEnum estado;
	
	private Date dtNascimento;
	
	private String telPrin;
	
	private String telSec;

	private String cep;
	
    private String logradouro;
    
    private String complemento;
    
    private String numero;

    private String bairro;
    
    private String localidade;
    
    private String uf;
    
    private String nomeRef;

    private String telRef;
	    
	private String nomeRef1;
	
	private String telRef1;
	
	private StatusEnum status;
	
	private PerfilAcesso perfilUsuario;
	
	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=======  Start ModelUsuario  =======");
		logger.info("=  conversation = "+conversation.getId());
		
		if(conversation.isTransient()){
			logger.info("==  conversation begin             ==");
			conversation.begin();
			conversation.setTimeout(60 * 60 * 1000);
		}
		
		logger.info("=  conversation = "+conversation.getId());
		logger.info("====================================");
		
		lazyModel = new DataModelUsuario(this, daoUsuario);

		DTOFiltroBase filtro = new DTOFiltroBase();
		
		setListPessoa(daoPessoa.buscarTudo(filtro));
		setListUsuario(daoUsuario.buscarTodosUsuarios(filtro));
		setPerfis(daoPerfil.buscarPerfil(filtro));
		
		PrencherUsuarioPessoa();
		
		logger.info("====================================");
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("=====================================");
		logger.info("========  Stop ModelUsuario  ========");
		
		if(!conversation.isTransient()){
			logger.info("==  conversation end             ==");
			conversation.end();
		}
		
		logger.info("====================================");
	}
	
	public void PrencherUsuarioPessoa(){

		List <UsuarioPessoaDTO> list = new ArrayList();
		UsuarioPessoaDTO dto ;

			for (Usuario usu :this.listUsuario){
				dto = new UsuarioPessoaDTO();
				dto.setUsuario(usu);

				for(Pessoa pessoa:this.listPessoa){

					if(pessoa.getCpfCnpj().toString().equals(usu.getId().toString())){
					
						dto.setPessoa(pessoa);
						list.add(dto);
						this.listaUsuarioPessoa = list;
					}	
				}

			}
			this.qtdRows = listaUsuarioPessoa.size();
	}
	
	
	
	public ModelPessoa getModelPessoa() {
		return modelPessoa;
	}

	public void setModelPessoa(ModelPessoa modelPessoa) {
		this.modelPessoa = modelPessoa;
	}

	public LazyDataModel<Usuario> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Usuario> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public List<Usuario> getListUsuario() {
		return listUsuario;
	}

	public void setListUsuario(List<Usuario> lazyModel) {
		this.listUsuario = lazyModel;
	}
	
	public List<Pessoa> getListPessoa() {
		return listPessoa;
	}

	public void setListPessoa(List<Pessoa> lazyModel) {
		this.listPessoa = lazyModel;
	}

	public Integer getPageSelecionada() {
		return pageSelecionada;
	}

	public void setPageSelecionada(Integer pageSelecionada) {
		this.pageSelecionada = pageSelecionada;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

	public List <Usuario> getSelecionados() {
		return selecionados;
	}

	public void setSelecionados(List<Usuario> selecionados) {
		this.selecionados = selecionados;
	}


	public Integer getQtdRows() {
		return qtdRows;
	}

	public void setQtdRows(Integer qtdRows) {
		this.qtdRows = qtdRows;
	}

	public PerfilAcesso getPerfilUsuario() {
		return perfilUsuario;
	}

	public void setPerfilUsuario(PerfilAcesso perfil) {
		this.perfilUsuario = perfil;
	}

	public List<PerfilAcesso> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<PerfilAcesso> perfis) {
		this.perfis = perfis;
	}

	public EstadosEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadosEnum estado) {
		this.estado = estado;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public StatusEnum getStatus(){
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getTelPrin() {
		return telPrin;
	}

	public void setTelPrin(String telPrin) {
		this.telPrin = telPrin;
	}

	public String getTelSec() {
		return telSec;
	}

	public void setTelSec(String telSec) {
		this.telSec = telSec;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getNomeRef() {
		return nomeRef;
	}

	public void setNomeRef(String nomeRef) {
		this.nomeRef = nomeRef;
	}

	public String getTelRef() {
		return telRef;
	}

	public void setTelRef(String telRef) {
		this.telRef = telRef;
	}

	public String getNomeRef1() {
		return nomeRef1;
	}

	public void setNomeRef1(String nomeRef1) {
		this.nomeRef1 = nomeRef1;
	}

	public String getTelRef1() {
		return telRef1;
	}

	public void setTelRef1(String telRef1) {
		this.telRef1 = telRef1;
	}
	
	public Usuario getNewUsuario() {
		return newUsuario;
	}

	public void setNewUsuario(Usuario newUsuario) {
		this.newUsuario = newUsuario;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa newPessoa) {
		this.pessoa = newPessoa;
	}
	
	public UsuarioPessoaDTO getUsuarioPessoa() {
		return usuarioPessoa;
	}

	public void setUsuarioPessoa(UsuarioPessoaDTO usu) {
		this.usuarioPessoa = usu;
	}

	public List<UsuarioPessoaDTO> getListaUsuarioPessoa() {
		return listaUsuarioPessoa;
	}

	public void setListaUsuarioPessoa(List<UsuarioPessoaDTO> usu) {
		this.listaUsuarioPessoa = usu;
	}
}
