package br.com.vle.model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
*
* @author Victor Godoi
*/
@Named
@ConversationScoped
public class ModelEndereco implements Serializable {

	private static final long serialVersionUID = 1110248501652831575L;
	
	
	@Inject
	private transient Logger logger;
	
    private String cep;
    private String logradouro;
    private String complemento;
    private String bairro;
    private String numero;
    private String localidade;
    private String uf;
    private String unidade;
    private String ibge;
    private String gia;
    private boolean statusConexao;
    
	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=======  Start ModelEndereco  ======");
		logger.info("====================================");

	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("=======  Stop ModelEndereco  =======");	
		logger.info("====================================");
	}

    public String getCep() {
        return cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public String getNumero() {
        return numero;
    }

    public String getLocalidade() {
        return localidade;
    }

    public String getUf() {
        return uf;
    }

    public String getUnidade() {
        return unidade;
    }

    public String getIbge() {
        return ibge;
    }

    public String getGia() {
        return gia;
    }
    
    public boolean getStatusConexao() {
        return statusConexao;
    }

    public ModelEndereco setCep(String cep) {
        this.cep = cep;
        return this;
    }

    public ModelEndereco setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public ModelEndereco setComplemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public ModelEndereco setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public ModelEndereco setNumero(String numero) {
        this.numero = numero;
        return this;
    }

    public ModelEndereco setLocalidade(String localidade) {
        this.localidade = localidade;
        return this;
    }

    public ModelEndereco setUf(String uf) {
        this.uf = uf;
        return this;
    }

    public ModelEndereco setUnidade(String unidade) {
        this.unidade = unidade;
        return this;
    }

    public ModelEndereco setIbge(String ibge) {
        this.ibge = ibge;
        return this;
    }

    public ModelEndereco setGia(String gia) {
        this.gia = gia;
        return this;
    }
    
    public ModelEndereco setStatusConexao(boolean status) {
        this.statusConexao = status;
        return this;
    }

    @Override
    public String toString() {
        return "Endereco{" +
                "cep='" + cep + '\'' +
                ", logradouro='" + logradouro + '\'' +
                ", complemento='" + complemento + '\'' +
                ", bairro='" + bairro + '\'' +
                ", numero='" + numero + '\'' +
                ", localidade='" + localidade + '\'' +
                ", uf='" + uf + '\'' +
                ", unidade='" + unidade + '\'' +
                ", ibge='" + ibge + '\'' +
                ", gia='" + gia + '\'' +
                ", statusConexao='" + gia + '\'' +
                '}';
    }
}