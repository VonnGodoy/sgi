package br.com.vle.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;

import br.com.vle.controller.pessoa.DataModelPessoa;
import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Usuario;
import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.enuns.EstadosEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.servico.dao.DaoPerfilAcesso;
import br.com.vle.servico.dao.DaoPessoa;
import br.com.vle.servico.dao.DaoUsuario;

/**
 *
 * @author Victor Godoi
 */
@Named
@ConversationScoped
public class ModelPessoa implements Serializable {

	private static final long serialVersionUID = 1110248501652831575L;
	
	@Inject
	private Conversation conversation;
	
	@Inject
	private transient Logger logger;
	
	private Pessoa pessoaSelecionado;
	
	private Pessoa[] selecionados;
	
	private List<Usuario> usuariosSelecionados;
	
	private Usuario usuarioSelecionado;
	
	@Inject
	private DaoPessoa daoPessoa;
	
	@Inject
	private DaoUsuario daoUsuario;
	
	@Inject
	private DaoPerfilAcesso daoPerfil;
	
	private LazyDataModel<Pessoa> lazyModel;
	
	private List<Usuario> usuarios;
	
	private Integer pageSelecionada;
	
	private Integer qtdRows = 10;
	
	@Inject
	private Pessoa pessoa ;
	
	private PerfilAcesso perfilUsuario;
	
	private List<PerfilAcesso> perfis;
	
	private StatusEnum status;
	
	private EstadosEnum estado;
	
	private EstadosEnum[] estados;
	
	private String nome;
	
	private String cpfCnpj;
	
    private String ie;

	private String rg;
	
	private String uf;
    
	private Date dtNascimento;
    
	private String email;
    
	private String telPrin;
    
	private String telSec;
    
	private String cep;
    
	private String logradouro;
    
	private String complemento;
    
	private String numero;
    
	private String bairro;
    
	private String localidade;
    
	private String refNome;
    
	private String refTel;
    
	private String refNome1;
    
	private String refTel1;
	
	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=======  Start ModelPessoa  =======");
		logger.info("=  conversation = "+conversation.getId());
		
		if(conversation.isTransient()){
			logger.info("==  conversation begin             ==");
			conversation.begin();
			conversation.setTimeout(60 * 60 * 1000);
		}
		
		logger.info("=  conversation = "+conversation.getId());
		logger.info("====================================");
		
		DTOFiltroBase filtro = new DTOFiltroBase();
		
		lazyModel = new DataModelPessoa(this, daoPessoa);
		
		setPerfis(daoPerfil.buscarPerfil(filtro));
		setUsuarios(daoUsuario.buscarTodosUsuarios(filtro));
		
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("=====================================");
		logger.info("========  Stop ModelPessoa  ========");
		
		if(!conversation.isTransient()){
			logger.info("==  conversation end             ==");
			conversation.end();
		}
		
		logger.info("====================================");
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	public Pessoa getPessoaSelecionado() {
		return pessoaSelecionado;
	}

	public void setPessoaSelecionado(Pessoa pessoaSelecionado) {
		this.pessoaSelecionado = pessoaSelecionado;
	}

	public Pessoa[] getSelecionados() {
		return selecionados;
	}

	public void setSelecionados(Pessoa[] selecionados) {
		this.selecionados = selecionados;
	}

	public DaoPessoa getDaoPessoa() {
		return daoPessoa;
	}

	public void setDaoPessoa(DaoPessoa daoPessoa) {
		this.daoPessoa = daoPessoa;
	}

	public LazyDataModel<Pessoa> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Pessoa> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public Integer getPageSelecionada() {
		return pageSelecionada;
	}

	public void setPageSelecionada(Integer pageSelecionada) {
		this.pageSelecionada = pageSelecionada;
	}

	public Integer getQtdRows() {
		return qtdRows;
	}

	public void setQtdRows(Integer qtdRows) {
		this.qtdRows = qtdRows;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa newPessoa) {
		this.pessoa = newPessoa;
	}	
	
	public PerfilAcesso getPerfilUsuario() {
		return perfilUsuario;
	}

	public void setPerfilUsuario(PerfilAcesso perfil) {
		this.perfilUsuario = perfil;
	}

	public List<PerfilAcesso> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<PerfilAcesso> perfis) {
		this.perfis = perfis;
	}
	
	public StatusEnum getStatus(){
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}
	
	public EstadosEnum getEstado() {
		return estado;
	}
	
	public void setEstados(EstadosEnum[] estados) {
		this.estados = estados;
	}

	public EstadosEnum[] getEstados(){
		return estados = EstadosEnum.values();	
	}

	public void setEstado(EstadosEnum estado) {
		this.estado = estado;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuario) {
		this.usuarios = usuario;
	}

	public List<Usuario> getUsuariosSelecionados() {
		return usuariosSelecionados;
	}

	public void setUsuariosSelecionados(List<Usuario> usuariosSelecionados) {
		this.usuariosSelecionados = usuariosSelecionados;
	}
	
	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

	public DaoUsuario getDaoUsuario() {
		return daoUsuario;
	}

	public void setDaoUsuario(DaoUsuario daoUsuario) {
		this.daoUsuario = daoUsuario;
	}

	public DaoPerfilAcesso getDaoPerfil() {
		return daoPerfil;
	}

	public void setDaoPerfil(DaoPerfilAcesso daoPerfil) {
		this.daoPerfil = daoPerfil;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public String getUf() {
		return uf;
	}

	public void setUf(String Uf) {
		this.uf = Uf;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelPrin() {
		return telPrin;
	}

	public void setTelPrin(String telPrin) {
		this.telPrin = telPrin;
	}

	public String getTelSec() {
		return telSec;
	}

	public void setTelSec(String telSec) {
		this.telSec = telSec;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getRefNome() {
		return refNome;
	}

	public void setRefNome(String refNome) {
		this.refNome = refNome;
	}

	public String getRefTel() {
		return refTel;
	}

	public void setRefTel(String refTel) {
		this.refTel = refTel;
	}

	public String getRefNome1() {
		return refNome1;
	}

	public void setRefNome1(String refNome1) {
		this.refNome1 = refNome1;
	}

	public String getRefTel1() {
		return refTel1;
	}

	public void setRefTel1(String refTel1) {
		this.refTel1 = refTel1;
	}
	
	
	
}