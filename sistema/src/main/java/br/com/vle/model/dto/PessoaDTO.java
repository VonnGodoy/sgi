package br.com.vle.model.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Victor Godoi
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PessoaDTO implements Serializable{
	
	private String id;
	private String ie;
	private String nome;
	private String cpfCnpj;
	private String rg;
	private String ssp;
	private Date dtNascimento;
	private String email;
	private String telPrin;
	private String telSec;
	private String cep;
	private String logradouro;
	private String complemento;
	private String numero;
	private String bairro;
	private String localidade;
	private String uf;
	private String refNome;
	private String refTel;
	private String refNome1;
	private String refTel1;
	private Date dtCadastro;
	
	
	public String getId(){
		return this.id;
	}

	public void setId(String idPessoa){
		this.id = idPessoa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getSsp() {
		return ssp;
	}

	public void setSsp(String ssp) {
		this.ssp = ssp;
	}
	
	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelPrin() {
		return telPrin;
	}

	public void setTelPrin(String telPrin) {
		this.telPrin = telPrin;
	}

	public String getTelSec() {
		return telSec;
	}

	public void setTelSec(String telSec) {
		this.telSec = telSec;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getRefNome() {
		return refNome;
	}

	public void setRefNome(String refNome) {
		this.refNome = refNome;
	}

	public String getRefTel() {
		return refTel;
	}

	public void setRefTel(String refTel) {
		this.refTel = refTel;
	}

	public String getRefNome1() {
		return refNome1;
	}

	public void setRefNome1(String refNome1) {
		this.refNome1 = refNome1;
	}
	
	public String getRefTel1() {
		return refTel1;
	}

	public void setRefTel1(String refTel1) {
		this.refTel1 = refTel1;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}
}
