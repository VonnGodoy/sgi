package br.com.vle.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;

import br.com.vle.controller.contratos.DataModelContratos;
import br.com.vle.entidade.Contrato;
import br.com.vle.entidade.Imoveis;
import br.com.vle.entidade.Pessoa;
import br.com.vle.enuns.MesesEnum;
import br.com.vle.enuns.PeriodosEnum;
import br.com.vle.enuns.ResponsavelDespesaEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.enuns.TipoContratosEnum;
import br.com.vle.servico.dao.DaoContratos;

/**
 *
 * @author Victor Godoi
 */

@Named
@ConversationScoped
public class ModelContratos implements Serializable {

	private static final long serialVersionUID = 1110248501652831575L;

	@Inject
	private Conversation conversation;

	@Inject
	private transient Logger logger;

	@Inject
	private DaoContratos daoContratos;

	private Contrato contrato;

	private Contrato contratoSelecionado;

	private List <Contrato> contratosSelecionados;

	private Imoveis imovelSelecionado;

	private Pessoa pessoaSelecionada;

	private LazyDataModel<Contrato> lazyModel;

	private Integer pageSelecionada;

	private Integer qtdRows = 10;

	private String idContrato;

	private String idImovel;

	private String dsImovel;

	private String idPessoa;

	private String nomePessoa;

	private StatusEnum contratoAtivo;

	private StatusEnum[] listStatus;

	private TipoContratosEnum tpContrato;

	private TipoContratosEnum[] listTpContratos;

	private ResponsavelDespesaEnum responsavel;

	private ResponsavelDespesaEnum[] listResponsaveis;

	private MesesEnum mes;

	private MesesEnum[] meses;

	private String anos[];

	private String ano;

	private PeriodosEnum validade ;

	private String dtIni;

	private String dtFim;

	private PeriodosEnum[] periodos;

	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=======  Start Model Imoveis  =======");
		logger.info("=  conversation = "+conversation.getId());

		if(conversation.isTransient()){
			logger.info("==  conversation begin             ==");
			conversation.begin();
			conversation.setTimeout(60 * 60 * 1000);
		}

		logger.info("=  conversation = "+conversation.getId());
		logger.info("====================================");


		lazyModel = new DataModelContratos(this, daoContratos);
	}

	@PreDestroy
	public void destroi(){
		logger.info("=====================================");
		logger.info("========  Stop Model Imoveis  ========");

		if(!conversation.isTransient()){
			logger.info("==  conversation end             ==");
			conversation.end();
		}

		logger.info("====================================");
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	public Imoveis getImovelSelecionado() {
		return imovelSelecionado;
	}

	public void setImovelSelecionado(Imoveis imovelSelecionado) {
		this.imovelSelecionado = imovelSelecionado;
	}

	public LazyDataModel<Contrato> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Contrato> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public Integer getPageSelecionada() {
		return pageSelecionada;
	}

	public void setPageSelecionada(Integer pageSelecionada) {
		this.pageSelecionada = pageSelecionada;
	}

	public Integer getQtdRows() {
		return qtdRows;
	}

	public void setQtdRows(Integer qtdRows) {
		this.qtdRows = qtdRows;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Contrato getContratoSelecionado() {
		return contratoSelecionado;
	}

	public void setContratoSelecionado(Contrato contratoSelecionado) {
		this.contratoSelecionado = contratoSelecionado;
	}

	public List<Contrato> getContratosSelecionados() {
		return contratosSelecionados;
	}

	public void setContratosSelecionados(List<Contrato> contratosSelecionados) {
		this.contratosSelecionados = contratosSelecionados;
	}

	public Pessoa getPessoaSelecionada() {
		return pessoaSelecionada;
	}

	public void setPessoaSelecionada(Pessoa pessoaSelecionada) {
		this.pessoaSelecionada = pessoaSelecionada;
	}

	public String getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(String id) {
		idContrato = id;
	}

	public String getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(String id) {
		idImovel = id;
	}

	public String getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(String id) {
		idPessoa = id;
	}

	public String getDsImovel() {
		return dsImovel;
	}

	public void setDsImovel(String dsImovel) {
		this.dsImovel = dsImovel;
	}

	public String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}

	public StatusEnum[] getListStatus() {
		return listStatus = StatusEnum.values();
	}

	public StatusEnum getContratoAtivo() {
		return contratoAtivo;
	}

	public void setContratoAtivo(StatusEnum status) {
		this.contratoAtivo = status;
	}

	public TipoContratosEnum[] getListTpContratos() {
		return listTpContratos = TipoContratosEnum.values();
	}

	public TipoContratosEnum getTpContrato() {
		return tpContrato;
	}

	public void setTpContrato(TipoContratosEnum tpContrato) {
		this.tpContrato = tpContrato;
	}

	public ResponsavelDespesaEnum getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(ResponsavelDespesaEnum responsavel) {
		this.responsavel = responsavel;
	}

	public ResponsavelDespesaEnum[] getListResponsaveis() {
		return listResponsaveis = ResponsavelDespesaEnum.values();
	}

	public MesesEnum getMes() {
		return mes;
	}

	public void setMes(MesesEnum mes) {
		this.mes = mes;
	}

	public MesesEnum[] getMeses() {
		return meses = MesesEnum.values();
	}


	public PeriodosEnum[] getPeriodos() {
		return periodos = PeriodosEnum.values();
	}

	public PeriodosEnum getValidade() {
		return validade;
	}

	public void setValidade(PeriodosEnum validade) {
		this.validade = validade;
		atualizaDtFim();
	}

	public String getDtFim() {
		return dtFim;
	}

	public void setDtFim(String dtFim) {
		this.dtFim = dtFim;
	}

	public String getDtIni() {
		return dtIni;
	}

	public void setDtIni(String dtIni) {
		this.dtFim = dtIni;
	}

	public void atualizaDtFim() {

		if(this.dtFim ==null) {

			if(this.validade!=null && !this.validade.equals(PeriodosEnum.SELECIONE)) {
				Calendar cal = Calendar.getInstance();

				int mes = cal.get(Calendar.MONTH ) + 1;
				int ano = cal.get(Calendar.YEAR);
				int anofim = ano + Integer.valueOf(validade.getPeriodo());
				String m = "";

				MesesEnum[] meses = MesesEnum.values();
				for(MesesEnum um: meses) {

					if(um.getMes().equals(String.valueOf(mes))) {
						m = um.getNome();
						break;
					}

				}
				this.dtIni = m +"-"+ ano;
				this.dtFim = m +"-"+ anofim;
			}
		}

	}


}