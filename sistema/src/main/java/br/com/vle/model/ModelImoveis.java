package br.com.vle.model;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.UploadedFile;

import br.com.vle.controller.imoveis.DataModelImoveis;
import br.com.vle.entidade.Imoveis;
import br.com.vle.enuns.StatusImoveisEnum;
import br.com.vle.enuns.TipoImovelEnum;
import br.com.vle.servico.dao.DaoImoveis;

/**
 *
 * @author Victor Godoi
 */

@Named
@ConversationScoped
public class ModelImoveis implements Serializable {

	private static final long serialVersionUID = 1110248501652831575L;
	
	@Inject
	private Conversation conversation;
	
	@Inject
	private transient Logger logger;
	
	private List<UploadedFile>  Imagens;
	
	private UploadedFile  Imagen;
	
	private Imoveis imovelSelecionado;
	
	private Imoveis imovel;

	private List <Imoveis> imoveis;
	
	@Inject
	private DaoImoveis daoImoveis;
	
	private LazyDataModel<Imoveis> lazyModel;
	
	private Integer pageSelecionada;
	
	private Integer qtdRows = 10;
	
	private TipoImovelEnum tpImovel;
	
	private StatusImoveisEnum statusImovel;
	
	private String cep;
	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=======  Start Model Imoveis  =======");
		logger.info("=  conversation = "+conversation.getId());
		
		if(conversation.isTransient()){
			logger.info("==  conversation begin             ==");
			conversation.begin();
			conversation.setTimeout(60 * 60 * 1000);
		}
		
		logger.info("=  conversation = "+conversation.getId());
		logger.info("====================================");
		
		if(this.imovel == null) {
			this.imovel =  new Imoveis();
		}
		
		lazyModel = new DataModelImoveis(this, daoImoveis);
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("=====================================");
		logger.info("========  Stop Model Imoveis  ========");
		
		if(!conversation.isTransient()){
			logger.info("==  conversation end             ==");
			conversation.end();
		}
		
		logger.info("====================================");
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	public Imoveis getImovelSelecionado() {
		return imovelSelecionado;
	}

	public void setImovelSelecionado(Imoveis imovelSelecionado) {
		this.imovelSelecionado = imovelSelecionado;
	}

	public List<UploadedFile> getImagens() {
		return Imagens;
	}

	public void setImagens(List<UploadedFile> img) {
		Imagens = img;
	}
	
	public UploadedFile getImagen() {
		return Imagen;
	}

	public void setImagen(UploadedFile img) {
		Imagen = img;
	}

	public List<Imoveis> getImoveis() {
		return imoveis;
	}

	public void setImoveis(List<Imoveis> imoveis) {
		this.imoveis = imoveis;
	}

	public Imoveis getImovel() {
		return imovel;
	}

	public void setImovel(Imoveis imovel) {
		this.imovel = imovel;
	}

	public LazyDataModel<Imoveis> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Imoveis> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public Integer getPageSelecionada() {
		return pageSelecionada;
	}

	public void setPageSelecionada(Integer pageSelecionada) {
		this.pageSelecionada = pageSelecionada;
	}

	public Integer getQtdRows() {
		return qtdRows;
	}

	public void setQtdRows(Integer qtdRows) {
		this.qtdRows = qtdRows;
	}

	public TipoImovelEnum[] getListatpImoveis() {
		return TipoImovelEnum.values();
	}


	public TipoImovelEnum getTpImovel() {
		return tpImovel;
	}

	public void setTpImovel(TipoImovelEnum tpImovel) {
		this.tpImovel = tpImovel;
	}

	public StatusImoveisEnum[] getListaStatusImoveis() {
		return StatusImoveisEnum.values();
	}

	public StatusImoveisEnum getStatusImovel() {
		return statusImovel;
	}

	public void setStatusImovel(StatusImoveisEnum statusImovel) {
		this.statusImovel = statusImovel;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	
	
	
}