package br.com.vle.model.dto;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Usuario;

/**
 *
 * @author Victor Godoi
 */

@Named
@ConversationScoped
public class UsuarioPessoaDTO implements Serializable {

	private static final long serialVersionUID = 1110248501652831575L;
	
	@Inject
	private Conversation conversation;
	
	@Inject
	private transient Logger logger;
	
	private Usuario usuario;
	private Pessoa pessoa;
	
	private Integer qtdRows = 10;

	
	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=======  Start UsuarioPessoa  =======");
		logger.info("=  conversation = "+conversation.getId());
		
		if(conversation.isTransient()){
			logger.info("==  conversation begin             ==");
			conversation.begin();
			conversation.setTimeout(60 * 60 * 1000);
		}

	}
	
	@PreDestroy
	public void destroi(){
		logger.info("=====================================");
		logger.info("========  Stop UsuarioPessoa   ========");
		
		if(!conversation.isTransient()){
			logger.info("==  conversation end             ==");
			conversation.end();
		}
		
		logger.info("====================================");
	}



	public Integer getQtdRows() {
		return qtdRows;
	}

	public void setQtdRows(Integer qtdRows) {
		this.qtdRows = qtdRows;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	
}
