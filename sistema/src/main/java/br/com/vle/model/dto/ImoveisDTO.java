package br.com.vle.model.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Victor Godoi
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ImoveisDTO implements Serializable{
	
		private static final long serialVersionUID = -1877149943191398991L;

		private String id;
		private Integer id_status_imovel;
		private String ds_imovel;
		private String ds_observacoes;
		private String cep;
		private String logradouro;
		private String complemento;
		private String numero;
		private String bairro;
		private String localidade;
		private String uf;
		
		public String getId() {
			return id;
		}
		public void setId(String id_imovel) {
			this.id = id_imovel;
		}

		public Integer getId_status_imovel() {
			return id_status_imovel;
		}
		public void setId_status_imovel(Integer id_status_imovel) {
			this.id_status_imovel = id_status_imovel;
		}
		public String getDs_imovel() {
			return ds_imovel;
		}
		public void setDs_imovel(String ds_imovel) {
			this.ds_imovel = ds_imovel;
		}
		public String getDs_observacoes() {
			return ds_observacoes;
		}
		public void setDs_observacoes(String ds_observacoes) {
			this.ds_observacoes = ds_observacoes;
		}
		public String getCep() {
			return cep;
		}
		public void setCep(String cep) {
			this.cep = cep;
		}
		public String getLogradouro() {
			return logradouro;
		}
		public void setLogradouro(String logradouro) {
			this.logradouro = logradouro;
		}
		public String getComplemento() {
			return complemento;
		}
		public void setComplemento(String complemento) {
			this.complemento = complemento;
		}
		public String getNumero() {
			return numero;
		}
		public void setNumero(String numero) {
			this.numero = numero;
		}
		public String getBairro() {
			return bairro;
		}
		public void setBairro(String bairro) {
			this.bairro = bairro;
		}
		public String getLocalidade() {
			return localidade;
		}
		public void setLocalidade(String localidade) {
			this.localidade = localidade;
		}
		public String getUf() {
			return uf;
		}
		public void setUf(String uf) {
			this.uf = uf;
		}
		
	}
