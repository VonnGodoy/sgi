package br.com.vle.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.management.StringValueExp;

import org.primefaces.model.LazyDataModel;

import br.com.vle.controller.financeiro.DataModelFinanceiro;
import br.com.vle.entidade.Financeiro;
import br.com.vle.entidade.Imoveis;
import br.com.vle.entidade.Pessoa;
import br.com.vle.enuns.MesesEnum;
import br.com.vle.enuns.PeriodosEnum;
import br.com.vle.enuns.ResponsavelDespesaEnum;
import br.com.vle.enuns.SituacaoFinancasEnum;
import br.com.vle.enuns.StatusEnum;
import br.com.vle.enuns.TipoContratosEnum;
import br.com.vle.servico.dao.DaoFinanceiro;

/**
 *
 * @author Victor Godoi
 */

@Named
@ConversationScoped
public class ModelFinanceiro implements Serializable {

	private static final long serialVersionUID = -1244864113615967277L;

	@Inject
	private Conversation conversation;

	@Inject
	private transient Logger logger;

	@Inject
	private DaoFinanceiro daoFinanceiro;

	private Financeiro contrato;

	private Financeiro selecionado;

	private List <Financeiro> selecionados;

	private Imoveis imovelSelecionado;

	private Pessoa pessoaSelecionada;

	private LazyDataModel<Financeiro> lazyModel;

	private Integer pageSelecionada;

	private Integer qtdRows = 100;

	private String idContrato;

	private String idImovel;

	private String dsImovel;

	private String idPessoa;

	private String nomePessoa;

	private StatusEnum contratoAtivo;

	private StatusEnum[] listStatus;
	
	private SituacaoFinancasEnum statusFinanceiro;

	private SituacaoFinancasEnum[] listStatusFinanceiro;

	private TipoContratosEnum tpContrato;

	private TipoContratosEnum[] listTpContratos;

	private ResponsavelDespesaEnum responsavel;

	private ResponsavelDespesaEnum[] listResponsaveis;

	private MesesEnum mes;

	private MesesEnum[] meses;

	private String anos[];

	private String ano;

	private PeriodosEnum validade ;

	private String dtIni;

	private String dtFim;

	private PeriodosEnum[] periodos;
	
	private BigDecimal valorReceber;
	
	private BigDecimal valorRecebido;
	
	private BigDecimal valorMulta  = new BigDecimal("0.00");
	
	private BigDecimal desconto  = new BigDecimal("0.00");
	
	private BigDecimal percent = new BigDecimal("100");
	
	private BigDecimal vlr ;

	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=======  Start Model Imoveis  =======");
		logger.info("=  conversation = "+conversation.getId());

		if(conversation.isTransient()){
			logger.info("==  conversation begin             ==");
			conversation.begin();
			conversation.setTimeout(60 * 60 * 1000);
		}

		logger.info("=  conversation = "+conversation.getId());
		logger.info("====================================");


		lazyModel = new DataModelFinanceiro(this, daoFinanceiro);
	}

	@PreDestroy
	public void destroi(){
		logger.info("=====================================");
		logger.info("========  Stop Model Imoveis  ========");

		if(!conversation.isTransient()){
			logger.info("==  conversation end             ==");
			conversation.end();
		}

		logger.info("====================================");
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	public Imoveis getImovelSelecionado() {
		return imovelSelecionado;
	}

	public void setImovelSelecionado(Imoveis imovelSelecionado) {
		this.imovelSelecionado = imovelSelecionado;
	}

	public LazyDataModel<Financeiro> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Financeiro> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public Integer getPageSelecionada() {
		return pageSelecionada;
	}

	public void setPageSelecionada(Integer pageSelecionada) {
		this.pageSelecionada = pageSelecionada;
	}

	public Integer getQtdRows() {
		return qtdRows;
	}

	public void setQtdRows(Integer qtdRows) {
		this.qtdRows = qtdRows;
	}

	public Financeiro getContrato() {
		return contrato;
	}

	public void setContrato(Financeiro contrato) {
		this.contrato = contrato;
	}

	public Financeiro getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Financeiro Selecionado) {
		this.selecionado = Selecionado;
	}

	public List <Financeiro> getSelecionados() {
		return selecionados;
	}

	public void setSelecionados(List <Financeiro> Selecionados) {
		this.selecionados = Selecionados;
	}

	public Pessoa getPessoaSelecionada() {
		return pessoaSelecionada;
	}

	public void setPessoaSelecionada(Pessoa pessoaSelecionada) {
		this.pessoaSelecionada = pessoaSelecionada;
	}

	public String getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(String id) {
		idContrato = id;
	}

	public String getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(String id) {
		idImovel = id;
	}

	public String getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(String id) {
		idPessoa = id;
	}

	public String getDsImovel() {
		return dsImovel;
	}

	public void setDsImovel(String dsImovel) {
		this.dsImovel = dsImovel;
	}

	public String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}

	public StatusEnum[] getListStatus() {
		return listStatus = StatusEnum.values();
	}

	public StatusEnum getContratoAtivo() {
		return contratoAtivo;
	}

	public void setContratoAtivo(StatusEnum status) {
		this.contratoAtivo = status;
	}

	public TipoContratosEnum[] getListTpContratos() {
		return listTpContratos = TipoContratosEnum.values();
	}

	public TipoContratosEnum getTpContrato() {
		return tpContrato;
	}

	public void setTpContrato(TipoContratosEnum tpContrato) {
		this.tpContrato = tpContrato;
	}

	public ResponsavelDespesaEnum getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(ResponsavelDespesaEnum responsavel) {
		this.responsavel = responsavel;
	}

	public ResponsavelDespesaEnum[] getListResponsaveis() {
		return listResponsaveis = ResponsavelDespesaEnum.values();
	}

	public MesesEnum getMes() {
		return mes;
	}

	public void setMes(MesesEnum mes) {
		this.mes = mes;
	}

	public MesesEnum[] getMeses() {
		return meses = MesesEnum.values();
	}


	public PeriodosEnum[] getPeriodos() {
		return periodos = PeriodosEnum.values();
	}

	public PeriodosEnum getValidade() {
		return validade;
	}

	public void setValidade(PeriodosEnum validade) {
		this.validade = validade;
		atualizaDtFim();
	}

	public String getDtFim() {
		return dtFim;
	}

	public void setDtFim(String dtFim) {
		this.dtFim = dtFim;
	}

	public String getDtIni() {
		return dtIni;
	}

	public void setDtIni(String dtIni) {
		this.dtFim = dtIni;
	}

	public void atualizaDtFim() {

		if(this.dtFim ==null) {

			if(this.validade!=null && !this.validade.equals(PeriodosEnum.SELECIONE)) {
				Calendar cal = Calendar.getInstance();

				int mes = cal.get(Calendar.MONTH ) + 1;
				int ano = cal.get(Calendar.YEAR);
				int anofim = ano + Integer.valueOf(validade.getPeriodo());
				String m = "";

				MesesEnum[] meses = MesesEnum.values();
				for(MesesEnum um: meses) {

					if(um.getMes().equals(String.valueOf(mes))) {
						m = um.getNome();
						break;
					}

				}
				this.dtIni = m +"-"+ ano;
				this.dtFim = m +"-"+ anofim;
			}
		}

	}

	public SituacaoFinancasEnum getStatusFinanceiro() {
		return statusFinanceiro;
	}

	public void setStatusFinanceiro(SituacaoFinancasEnum statusFinanceiro) {
		this.statusFinanceiro = statusFinanceiro;
	}

	public SituacaoFinancasEnum[] getListStatusFinanceiro() {
		return listStatusFinanceiro = SituacaoFinancasEnum.values();
	}

	public BigDecimal getValorReceber() {
		
		return valorReceber;
	}

	public void setValorReceber(BigDecimal valorReceber) {
		this.valorReceber = valorReceber;
	}

	public BigDecimal getDesconto() {
		return desconto;
	}

	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}
	

	public BigDecimal getValorMulta() {
		return valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	public int calculaDias() {
		Calendar cal = Calendar.getInstance();
		
		int diaVen = Integer.valueOf(this.selecionado.getDiaVencimento());
		int retorno = 0;
		@SuppressWarnings("deprecation")
		int dia = cal.get(cal.DAY_OF_MONTH);
		@SuppressWarnings("deprecation")
		int mes = cal.get(Calendar.MONTH ) + 1;
		
		MesesEnum[] lista = MesesEnum.values();
		
		for(MesesEnum m : lista) {
			
			if(m.getMes().toString().equals(String.valueOf(mes))) {
				
				if(this.selecionado.getMes().toString().contains(m.getNome().toString())) {
					retorno = diaVen - dia;
					break;
				}else {
					retorno = -1;
					break;
				}
			}
		}
		
		return retorno ;
	}
	
	public void calculaDesconto() {
		
		long ValorMensalidade = this.selecionado.getValorMensalidade().longValue(); 
		long txMulta = this.selecionado.getTxMultaAtraso().longValue() ;
		Long percen = 100l;
		
		long res = ValorMensalidade/percen * txMulta;
		
		this.desconto = new BigDecimal(String.valueOf(res)+".00");
		
		this.valorReceber = this.selecionado.getValorMensalidade().subtract(this.desconto);
	}
	
	public void calculaValorComMulta() {
		
		long ValorMensalidade = this.selecionado.getValorMensalidade().longValue(); 
		long txMulta = this.selecionado.getTxMultaAtraso().longValue() ;
		Long percen = 100l;
		
		long res = ValorMensalidade/percen * txMulta;
		
		this.valorMulta = new BigDecimal(String.valueOf(res)+".00");
		
		this.valorReceber = this.selecionado.getValorMensalidade().add(this.valorMulta);
	}

	public BigDecimal getVlr() {
		this.vlr = this.selecionado.getValorMensalidade();
		
		if(this.selecionado !=null) {
			
			int qtd = calculaDias();
			
			if(qtd >= 5) {
				calculaDesconto();
			}else if(qtd < 5 && qtd > 0){
				this.valorReceber = this.selecionado.getValorMensalidade();
			}else if(qtd < 0) {
				calculaValorComMulta();
			}
		}
		return vlr ;
	}

	public void setVlr(BigDecimal vlr) {
		this.vlr = vlr;
	}

	public BigDecimal getValorRecebido() {
		return valorRecebido;
	}

	public void setValorRecebido(BigDecimal valorRecebido) {
		
		this.valorRecebido = valorRecebido;
	}
	
	

}