package br.com.vle.model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.vle.controller.financeiro.DataModelFinanceiro;

/**
*
* @author Victor Godoi
*/
@Named
@ConversationScoped
public class ModelEmail implements Serializable {

	private static final long serialVersionUID = 1110248501652831575L;
	
	@Inject
	private Conversation conversation;

	@Inject
	private transient Logger logger;
	
	@PostConstruct
	public void init() {
		logger.info("====================================");
		logger.info("=======  Start Model Imoveis  =======");
		logger.info("=  conversation = "+conversation.getId());

		if(conversation.isTransient()){
			logger.info("==  conversation begin             ==");
			conversation.begin();
			conversation.setTimeout(60 * 60 * 1000);
		}

		logger.info("=  conversation = "+conversation.getId());
		logger.info("====================================");

	}

	@PreDestroy
	public void destroi(){
		logger.info("=====================================");
		logger.info("========  Stop Model Imoveis  ========");

		if(!conversation.isTransient()){
			logger.info("==  conversation end             ==");
			conversation.end();
		}

		logger.info("====================================");
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}
	
	private String password;
	private String remetente = "vleSistemasImobiliario@gmail.com";
	private String destinatario;
	private String assunto;
	private String mensagem;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRemetente() {
		return remetente;
	}
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	
}
