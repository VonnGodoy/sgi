package br.com.vle.cargainicial;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vle.entidade.PerfilAcesso;
import br.com.vle.entidade.Pessoa;
import br.com.vle.entidade.Role;
import br.com.vle.entidade.StatusImoveis;
import br.com.vle.entidade.Usuario;
import br.com.vle.enuns.RolesEnum;
import br.com.vle.enuns.StatusImoveisEnum;

/**
 *
 * @author Victor Godoi
 */

@Named
@Stateless
public class CargaInicial {
	
	private static final Logger log = LoggerFactory
			.getLogger(CargaInicial.class);
	
	@PersistenceContext
    private EntityManager em;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cargaInicial() {
		
		atualizaRoles();
		PerfilAcesso perfilAdm = atualizaPerfilAdministrador();
		criaStatusImoveis();
		criaRoot(perfilAdm);
		atualizaPerfilPadrao();
		
	
	}

	

	private void criaRoot(PerfilAcesso perfilAdm) {
		List<Usuario> admins = em.createQuery("FROM Usuario").getResultList();
		
		if(admins == null || admins.isEmpty()){
			
			Pessoa pessoa = new Pessoa();
			
			
			pessoa.setNome("ADMINISTRADOR PADRAO");
			pessoa.setCpfCnpj("12312312387");
			pessoa.setRg("1231231");
			pessoa.setSsp("SSPDF");
			pessoa.setDtNascimento(new Date());
			pessoa.setEmail("adm@vle.com.br");
			pessoa.setTelPrin("99999999999");
			pessoa.setTelSec(null);
			pessoa.setCep("12312123");
			pessoa.setLogradouro("TESTE");
			pessoa.setComplemento("TESTE");
			pessoa.setNumero("0");
			pessoa.setBairro("GAMA OESTE");
			pessoa.setLocalidade("BRASILIA");
			pessoa.setUf("DF");
			pessoa.setRefNome("TESTE");
			pessoa.setRefTel("99999999999");
			pessoa.setRefNome1(null);
			pessoa.setRefTel1(null);
			pessoa.setDtCadastro(new Date());
			
			Usuario usu = new Usuario();
			
			usu.setId(pessoa.getCpfCnpj());
			usu.setPerfilAcesso(perfilAdm);
			usu.setAtivo(true);
			usu.setPrimeiroAcesso(true);
			
			String value = pessoa.getCpfCnpj().toString().substring(0,6);
			try {
				MessageDigest messageDigest = MessageDigest.getInstance("MD5");
				byte[] hash = messageDigest.digest(value.getBytes("UTF-8"));
				StringBuilder stringBuilder=  new StringBuilder();
				for (int i = 0; i < hash.length; i++) {
					stringBuilder.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
				}
				usu.setPassword(stringBuilder.toString());
			} catch (NoSuchAlgorithmException e) {
				
				if(log.isWarnEnabled()){
					log.warn("não gerou senha md5 do admim", e);
				}
				usu.setPassword(null);
			} catch (UnsupportedEncodingException e) {
				if(log.isWarnEnabled()){
					log.warn("não gerou senha do admim encoding errado", e);
				}
				usu.setPassword(null);
			}
			try {
			em.merge(pessoa);
			}catch(Exception e){
				log.warn("erro ao cadastrar pessoa", e);
			}
			
			try {
			em.merge(usu);
			}catch(Exception e){
				log.warn("erro ao cadastrar usuario", e);
			}
		}
	}
	
	private void criaStatusImoveis() {

		List<StatusImoveis> listaStatus = listarStatus();
		List<StatusImoveis> listaBanco = em.createQuery("From StatusImoveis").getResultList();

		
		for (StatusImoveis status : listaStatus) {
			
			if(!listaBanco.contains(status)){
				em.merge(status);
				em.flush();
				em.clear();
			}
		}
	}
	

	private void atualizaRoles() {
		List<Role> paraGravarRole = new ArrayList<Role>();
		
		for (RolesEnum enumRole : RolesEnum.values()) {
			Role role = new Role(enumRole);
			paraGravarRole.add(role);
		}
		
		List<Role> listaBancoRole = em.createQuery("From Role").getResultList();
		
		List<Role> roles = atualizaLista(listaBancoRole, paraGravarRole);
		
		for (Role rolepg : roles) {
			if(!listaBancoRole.contains(rolepg) ){
				em.merge(rolepg);
				em.flush();
				em.clear();
			}
		}

	}

	private PerfilAcesso atualizaPerfilAdministrador() {
		List<Role> listaBancoRole;
		listaBancoRole = em.createQuery("From Role").getResultList();
		
		List<PerfilAcesso> paraGravarUser = new ArrayList<PerfilAcesso>();
		
		List<PerfilAcesso> admins = em.createQuery("FROM PerfilAcesso p where p.perfil = 'ADMINISTRADOR' ").getResultList();
		
		PerfilAcesso admin = null;
		
		if(admins == null || admins.isEmpty()){
			admin = new PerfilAcesso();
			admin.setPerfil("ADMINISTRADOR".toUpperCase());
		}else{
			admin = admins.get(0);
		}
		
		if(admin.getRoles().size() != listaBancoRole.size()){
			admin.setDescricao("Perfil de administrador do sistema (TODOS OS ACESSOS)".toUpperCase());
			admin.getRoles().addAll(listaBancoRole);
		
		try {	
			em.merge(admin);
		}catch(Exception e){
			log.warn("erro ao gerar perfil", e);
		}
			
		}
		
		return admin;
		
	}
	
	private void atualizaPerfilPadrao() {
		
		List<PerfilAcesso> paraGravarUser = new ArrayList<PerfilAcesso>();
		
		for (RolesEnum role : RolesEnum.values()) {
			if(role.getPai()==null){
				String nome = role.getNome().toUpperCase();
				nome = nome.replace("_", " ");
				List<PerfilAcesso> grupos = em.createQuery("FROM PerfilAcesso p where p.perfil = '"+nome+"' ").getResultList();
				
				PerfilAcesso perfil = null;
				
				if(grupos == null || grupos.isEmpty()){
					perfil = new PerfilAcesso();
					perfil.setPerfil(nome);
				}else{
					perfil = grupos.get(0);
				}
				
				criarPerfilEspecificos(role, perfil);
				
			}
		}
		
		
	}



	public void criarPerfilEspecificos(RolesEnum role, PerfilAcesso perfil) {
		List<Role> listaBancoRole;
		listaBancoRole = em.createQuery("From Role r where r.rolePai = '"+role.getNome()+"' or r.role = '"+role.getNome()+"' ").getResultList();
		
		if(perfil.getRoles().size() != listaBancoRole.size()){
			perfil.setDescricao(role.getDescricao().toUpperCase());
			perfil.getRoles().addAll(listaBancoRole);
			em.merge(perfil);
		}
	}
	

	public List<Role> atualizaLista(List<Role> listaBancoRole, List<Role> paraGravarRole){
		List<Role> role = new ArrayList<Role>();

		if(listaBancoRole != null && listaBancoRole.size() != 0){
			
			if(listaBancoRole.size() != paraGravarRole.size()) {
			
			for (Role roleDb : listaBancoRole) {
			
				if(paraGravarRole != null && paraGravarRole.size() != 0){
				
					for (Role roles : paraGravarRole) {
						
						if(roleDb.getRole().toString().equals(roles.getRole().toString())) {
							
							if(roleDb.getId() !=null) {
								roles.setId(roleDb.getId());
							}
							
							role.add(roles);
							
						}
						
					}
					
				}
			}
			}
		}else if(paraGravarRole != null && paraGravarRole.size() != 0){
			role.addAll(paraGravarRole);
		}
		
		return role;
		
	}

	public List <StatusImoveis> listarStatus() {
		
		StatusImoveisEnum[] status = StatusImoveisEnum.values();
		List<StatusImoveis> listaStatus = new ArrayList();

		for (StatusImoveisEnum statu : status) {

			StatusImoveis s = new StatusImoveis();
			s.setId(statu.getId().toString());
			s.setStatus(statu.getStatus().toString());

			listaStatus.add(s);

		}
		return listaStatus;
	}

	}
