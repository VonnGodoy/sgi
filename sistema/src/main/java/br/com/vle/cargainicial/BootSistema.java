package br.com.vle.cargainicial;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Victor Godoi
 */

public class BootSistema implements ServletContextListener{

	@Inject
	private CargaInicial cargaInicial;
	

	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("================================================================");
		System.out.println("================== Inicializando Sistema =======================");
		cargaInicial.cargaInicial();
		System.out.println("================================================================");
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("================================================================");
		System.out.println("================== Finalizando Sistema =========================");
		System.out.println("================================================================");
	}
	
}
