package br.com.vle.comum;

import java.util.HashMap;

import javax.ejb.ApplicationException;

/**
 *
 * @author Victor Godoi
 */

@ApplicationException
@SuppressWarnings("serial")
public class NegocioException extends RuntimeException {
	
	private HashMap<String, String[]> erros = new HashMap<String, String[]>();
	
	public NegocioException(Throwable e, String coderro, String... parametros){
		super(e.getMessage(), e);
		erros.put(coderro, parametros);
	}
	
	public NegocioException(String coderro, String... parametros){
		super(coderro);
		erros.put(coderro, parametros);
	}
	
	public NegocioException(){
	}
	
	public void addErro(String coderro, String... parametros){
		erros.put(coderro, parametros);
	}
	
	public void addErro(String coderro){
		erros.put(coderro, new String[]{});
	}
	
	public HashMap<String, String[]> getErros(){
		return erros;
	}
}
