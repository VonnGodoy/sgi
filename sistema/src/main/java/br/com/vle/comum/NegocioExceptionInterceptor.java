package br.com.vle.comum;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Victor Godoi
 */

@SuppressWarnings("serial")
@Interceptor
@NegocioExceptionBinding
public class NegocioExceptionInterceptor implements Serializable {

	private static final Logger log = LoggerFactory
			.getLogger(NegocioExceptionInterceptor.class);

	@AroundInvoke
	public Object validate(InvocationContext ctx) throws Exception {

		try {
			Object object = ctx.proceed();
			return (object);
		} catch (NegocioException e) {

			if (log.isInfoEnabled()) {
				log.info(e.getMessage(), e);
			}

			for (String key : e.getErros().keySet()) {
				String erro = UtilJSF.getBundleMsg(key, e.getErros().get(key));
				UtilJSF.getContext().addMessage(
						null,new FacesMessage(FacesMessage.SEVERITY_ERROR, erro, erro));
			}
		}catch (Exception e) {
			if(e instanceof ValidatorException){
				throw e;
			}
			log.error(e.getMessage(), e);
			StringBuilder sb = new StringBuilder("Erro inesperado do sistema. " + e.getMessage());
			if(e.getStackTrace()!=null){
				for (StackTraceElement element : e.getStackTrace()) {
					if(element.getClassName().contains("br.com.vle") && element.getLineNumber() > 0){
						sb.append("\n")
						.append(element.getClassName())
						.append(" - ")
						.append(element.getMethodName())
						.append(" linha ")
						.append(element.getLineNumber());
					}
				}
			}
			log.error("erro sistema. "+sb.toString());
			UtilJSF.getContext().addMessage(
					null,new FacesMessage(FacesMessage.SEVERITY_ERROR, sb.toString(), sb.toString()));

		}
		return null;
	}
}
