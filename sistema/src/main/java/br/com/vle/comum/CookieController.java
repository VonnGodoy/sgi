package br.com.vle.comum;

import static br.com.vle.comum.UtilJSF.getRequest;
import static br.com.vle.comum.UtilJSF.getResponse;

import javax.servlet.http.Cookie;

/**
 *
 * @author Victor Godoi
 */

public class CookieController {
	
	public static final String BTREMEMBER = "itremember";
	public static final String BTPASSWD = "tppasswd";
	public static final String BTUSER = "ituser";
	public static final String BTTHEMA = "itthemar";
	
	
	public CookieController() {
	}
	
	
	public void limparCookieLogin() {
		limparCookie(BTUSER);
		limparCookie(BTPASSWD);
		limparCookie(BTREMEMBER);
		gravaCookie(BTREMEMBER, "false");
	}
	
	public void gravaCookie(String chave, String valor) {
		Cookie campo = new Cookie(chave, valor);
		campo.setMaxAge(60 * 60 * 24 * 300);
		campo.setPath("/");
		getResponse().addCookie(campo);
	}
	
	public void limparCookie(String chave) {
		Cookie cookie = new Cookie(chave, "");
		cookie.setMaxAge(1000);
		cookie.setPath("/");
		getResponse().addCookie(cookie);
	}

	public String carregaCookie(String chave) {
		Cookie cookie[] = getRequest().getCookies();
		if (cookie != null && cookie.length > 0) {
			for (int i = 0; i < cookie.length; i++) {
				String cookieName =  cookie[i].getName();
				if (cookieName.equals(chave)) {
					//cookie[i].setValue("afterdark");
					return cookie[i].getValue();
				} 
			}
		} 
		return null;
	}

	

	
	
}
