package br.com.vle.comum;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

/**
 *
 * @author Victor Godoi
 */
@Named
@SessionScoped
public class ConfigController extends ControllerBase {

	private static final long serialVersionUID = 8266008415252943292L;

	private String theme = "afterdark";
	private String locale = "pt_BR";
	private Locale localeSelecionado = null;

	private static Map<String, Object> countries;
	private static List<String> themes;
	
	private transient CookieController cookieController = new CookieController();

	static {
		countries = new LinkedHashMap<String, Object>();
		countries.put("pt_BR", new Locale("pt_BR", "Brasil"));
		countries.put("en", Locale.ENGLISH);
		countries.put("es", new Locale("es", "Espanha"));

		themes = new ArrayList<String>();
		themes.add("afterdark");
		themes.add("afternoon");
		themes.add("afterwork");
		themes.add("bluesky");
		
		themes.add("bootstrap");
		
		themes.add("black-tie");
		themes.add("blitzer");
		themes.add("casablanca");
		themes.add("cruze");
		themes.add("cupertino");
		themes.add("dark-hive");
		themes.add("dot-luv");
		themes.add("eggplant");
		themes.add("excite-bike");
		themes.add("flick");
		themes.add("glass-x");
		themes.add("home");
		themes.add("hot-sneaks");
		themes.add("humanity");
		themes.add("le-frog");
		themes.add("midnight");
		themes.add("mint-choc");
		themes.add("midnight");
		themes.add("mint-choc");
		themes.add("overcast");
		themes.add("pepper-grinder");
		themes.add("redmond");
		themes.add("rocket");
		themes.add("sam");
		themes.add("smoothness");
		themes.add("south-street");
		themes.add("start");
		themes.add("sunny");
		themes.add("swanky-purse");
		themes.add("trontastic");
		themes.add("ui-darkness");
		themes.add("ui-lightness");
	}
	
	public ConfigController(){
		localeSelecionado = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		locale = localeSelecionado.toString();
		
		String themacookie = cookieController.carregaCookie(CookieController.BTTHEMA);
		if( themacookie != null){
			theme = themacookie;
		}
		
	}

	public void localeChanged(ValueChangeEvent e) {
		if (e.getNewValue() != null) {
			String newLocaleValue = e.getNewValue().toString();
			for (Map.Entry<String, Object> entry : countries.entrySet()) {
				if (entry.getValue() != null
						&& entry.getValue().toString().equals(newLocaleValue)) {
					FacesContext.getCurrentInstance().getViewRoot()
							.setLocale((Locale) entry.getValue());
					FacesContext.getCurrentInstance().getApplication()
							.setDefaultLocale((Locale) entry.getValue());
					localeSelecionado = (Locale) entry.getValue();

				}
			}
		}
	}
	
	public void themeChanged(ValueChangeEvent e) {
		if (e.getNewValue() != null) {
			String newValue = e.getNewValue().toString();
			
			cookieController.gravaCookie(CookieController.BTTHEMA, newValue);
		}
	}

	public Map<String, Object> getCountries() {
		return countries;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Locale getLocaleSelecionado() {
		return localeSelecionado;
	}

	public void setLocaleSelecionado(Locale localeSelecionado) {
		this.localeSelecionado = localeSelecionado;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public List<String> getThemes() {
		return themes;
	}

}
