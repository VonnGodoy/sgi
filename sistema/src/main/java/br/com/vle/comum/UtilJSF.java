package br.com.vle.comum;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Victor Godoi
 */

public class UtilJSF {
	
	public static String getBundleMsg(String msg, String... parametros) {
		ResourceBundle bundle = getContext().getApplication().getResourceBundle(
				getContext(), "msg");
		String msgbundle = msg;
		if (bundle.containsKey(msg)) {
			msgbundle = bundle.getString(msg);
		}
		if (parametros != null && parametros.length > 0) {
			msgbundle = MessageFormat.format(msgbundle, (Object[]) parametros);
		}
		return msgbundle;
	}
	
	public static FacesContext getContext() {
		FacesContext context = FacesContext.getCurrentInstance();
		return context;
	}
	
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = (HttpServletRequest) getContext().getExternalContext().getRequest();
		return request;
	}
	
	public static HttpServletResponse getResponse() {
		HttpServletResponse response  = (HttpServletResponse) getContext().getExternalContext().getResponse();
		return response;
	}
	
}
