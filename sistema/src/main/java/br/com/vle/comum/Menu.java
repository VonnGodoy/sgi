package br.com.vle.comum;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Victor Godoi
 */

@Named
@SessionScoped
public class Menu implements Serializable{

	private static final long serialVersionUID = -4609705035943306431L;
	
	@Inject
	private transient Logger logger;
	
	@Inject
	private Navegacao navegacao;
	
	@PostConstruct
	public void init(){
		logger.info("====================================");
		logger.info("===========  Start Menu  ===========");
		logger.info("====================================");
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("===========  Stop Menu  ============");
		logger.info("====================================");
	}
	
	public String menuManterPerfil(){
		return navegacao.redirect(Pages.HOME, Pages.MANTER_PERFIL);
	}
	
	public String menuManterUsuario(){
		return navegacao.redirect(Pages.HOME, Pages.MANTER_USUARIO);
	}
		
	public String menuAlterarSenha(){
		return navegacao.redirect(Pages.MANTER_USUARIO_ALTERAR_SENHA);
	}
	
	public String menuFinanceiro(){
		return navegacao.redirect(Pages.HOME, Pages.FINANCEIRO);
	}
	
	public String menuManterContratos(){
		return navegacao.redirect(Pages.HOME, Pages.MANTER_CONTRATOS);
	}
	
	public String menuManterImoveis(){
		return navegacao.redirect(Pages.HOME, Pages.MANTER_IMOVEIS);
	}
	
	public String menuManterPessoa(){
		return navegacao.redirect(Pages.HOME, Pages.MANTER_PESSOA);
	}
	
	public String menuConfiguracao(){
		return navegacao.redirect(Pages.HOME, Pages.CONFIGURACAO);
	}
	

}
