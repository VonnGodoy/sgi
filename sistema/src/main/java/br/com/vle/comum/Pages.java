package br.com.vle.comum;

/**
 *
 * @author Victor Godoi
 */

public enum Pages {
	
	LOGIN("idlogin","pretty:login","Principal"),
	RESET_SENHA("idreset_senha","pretty:reset_senha","Principal"),
	
	HOME("idhomemenu","pretty:home","Principal"),
	
	CONFIGURACAO("idconfiguracao","pretty:configuracao","configuracao_menu"),
	
	FINANCEIRO("idmanterfinanceiro","pretty:financeiro","financeiro"),
	FINANCEIRO_LANCAMENTOS("idlancamentosfinanceiro","pretty:lancamentos_financeiro","lancamentos_financeiro"),
	
	MANTER_CONTRATOS("idmantercontratos","pretty:manter_contratos","manter_contratos"),
	MANTER_CONTRATOS_INCLUIR("idmantercontratosincluir","pretty:manter_contratos_incluir","contratos_menu_incluir"),
	MANTER_CONTRATOS_RENOVAR("idmantercontratosrenovar","pretty:manter_contratos_renovar","contratos_menu_alterar"),
	MANTER_CONTRATOS_DETALHAR("idmantercontratosdetalhar","pretty:manter_contratos_detalhar","contratos_menu_detalhar"),
	
	MANTER_PESSOA("idmanterpessoa","pretty:manter_pessoa","pessoa_menu"),
	MANTER_PESSOA_INCLUIR("idmanterpessoaincluir","pretty:manter_pessoa_incluir","pessoa_menu_incluir"),
	MANTER_PESSOA_ALTERAR("idmanterpessoaalterar","pretty:manter_pessoa_alterar","pessoa_menu_alterar"),
	MANTER_PESSOA_SELECAO("idmanterpessoaselecionae","pretty:manter_pessoa_selecao","pessoa_menu_selecionar"),
	
	MANTER_PERFIL("idmanterperfil","pretty:manter_perfil_acesso","perfil_menu"),
	MANTER_PERFIL_INCLUIR("idmanterperfilincluir","pretty:manter_perfil_acesso_incluir","perfil_menu_incluir"),
	MANTER_PERFIL_ALTERAR("idmanterperfilalterar","pretty:manter_perfil_acesso_alterar","perfil_menu_alterar"),
	MANTER_PERFIL_SELECAO("idmanterperfilselecionae","pretty:perfil_acesso_selecao","perfil_menu_selecionar"),
	
	MANTER_USUARIO("idmanterusuario","pretty:manter_usuario","Manter usuário"),
	MANTER_USUARIO_ALTERAR_SENHA("idmanterusualtsenha","pretty:manter_usuario_alterar_senha","usuario_menu_alterar_senha"),
	MANTER_USUARIO_INCLUIR("idmanterusuarioincluir","pretty:manter_usuario_incluir","usuario_menu_incluir"),
	MANTER_USUARIO_ALTERAR("idmanterusuarioalterar","pretty:manter_usuario_alterar","usuario_menu_alterar"),
	
	MANTER_IMOVEIS("idmanterimoveis","pretty:manter_imoveis","manter_imoveis"),
	MANTER_IMOVEIS_INCLUIR("idmanterimoveisincluir","pretty:manter_imoveis_incluir","imoveis_menu_incluir"),
	MANTER_IMOVEIS_ALTERAR("idmanterimoveisalterar","pretty:manter_imoveis_alterar","imoveis_menu_alterar"),
	MANTER_IMOVEIS_EXCLUIR("idmanterimoveisexcluir","pretty:manter_imoveis_excluir","imoveis_menu_excluir"),
	MANTER_IMOVEIS_DETALHAR("idmanterimoveisdetalhar","pretty:manter_imoveis_detalhar","imoveis_menu_detalhar"),
	MANTER_IMOVEIS_SELECAO("idmanterimoveisselecionae","pretty:manter_imoveis_selecao","imoveis_menu_selecionar");
	
	
	private String id;
	private String url;
	private String nomePage;

	
	private Pages(String id, String url, String nome){
		this.id = id;
		this.url = url;
		this.nomePage = nome;
	}

	public String getId() {
		return id;
	}

	public String getUrl() {
		return url;
	}

	public String getNomePage() {
		return nomePage;
	}
	
}
