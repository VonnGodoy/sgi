package br.com.vle.comum;

import static br.com.vle.comum.UtilJSF.getBundleMsg;
import static br.com.vle.comum.UtilJSF.getRequest;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.component.breadcrumb.BreadCrumb;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DynamicMenuModel;
import org.primefaces.model.menu.MenuElement;

/**
 *
 * @author Victor Godoi
 */

@Named
@SessionScoped
public class Navegacao implements Serializable  {

	private static final int TIME_OUT_CONSERSATION = 2 * 60 * 60 * 1000;

	/**
	 * 
	 */
	private static final long serialVersionUID = -4609705035943306431L;
	
	@Inject 
	private Conversation conversation;
	
	private BreadCrumb breadCrumb; 
	
	@Inject
	private transient Logger logger;
	
	private Pages pageCorrente;
	
	private DynamicMenuModel breadcrumbmodel;
	
	@PostConstruct
	public void init(){
		logger.info("====================================");
		logger.info("=======  Start Navegacao  ==========");
		
		breadCrumb = new BreadCrumb();
		breadcrumbmodel = new DynamicMenuModel();
		
		logger.info("====================================");
	}
	
	@PreDestroy
	public void destroi(){
		logger.info("====================================");
		logger.info("=========  Stop Navegacao  =========");
		logger.info("====================================");
	}
	
	public void addItem(Pages pages) {
		DefaultMenuItem item = new DefaultMenuItem();
		
		item.setValue(getBundleMsg(pages.getNomePage(), new String[]{}));
		
		item.setDisabled(true);
		
		item.setUrl(pages.getUrl());
		item.setParam("id", pages.getId());
		item.setUrl(pages.getUrl());
		
		item.setId(pages.getId());
		this.breadcrumbmodel.addElement(item);
	}
	
	public String voltar(boolean endConversation){
		logger.info("======================================");
		logger.info("=========  voltar Navegacao  =========");
		
		List<MenuElement> list = this.breadcrumbmodel.getElements();
		MenuElement ultimaPage = list.get(list.size() - 1);
		this.breadcrumbmodel.getElements().remove(ultimaPage);
				
		DefaultMenuItem ultimaPage2 = (DefaultMenuItem) ultimaPage;
		List<String> idMenu =  ultimaPage2.getParams().get("id");
		for(Pages p : Pages.values()){
			if(p.getId().equals(idMenu.get(0))){
				pageCorrente = p;
			}
		}
		
		if(endConversation){
			endConversation();
			beginConversation();
		}
		
		
		getRequest().setAttribute("nocid", "true");
		
		logger.info("==  " + ultimaPage2.getUrl() );
		logger.info("======================================");
		return ultimaPage2.getUrl();
	}
	
	public String voltar(){
		return voltar(false);
	}
	
	/**
	 * só para menu, navegação entre uc usar o redirect abaixo
	 * @param atual
	 * @param proxima
	 * @return
	 */
	public String redirect(Pages atual, Pages proxima) {
		
		String novaurl = proxima.getUrl();
		
		HttpServletRequest request = getRequest();
		request.setAttribute("faces-redirect", "true");
		request.setAttribute("nocid", "true");
		
		if(!conversation.isTransient()){
			logger.info("====================================");
			logger.info("=  indo para "+ proxima.getId());
			logger.info("=  Navegacao end conversation  id = "+conversation.getId());
			conversation.end();
		}
		
		limparBreadCrum();
			
		addItem(atual);
		
		pageCorrente = proxima;
		
		return novaurl;
	}
	
	/**
	 * só para  navegação entre uc 
	 * @param proxima
	 * @return
	 */
	public String redirect(Pages proxima, boolean conversacao) {
		
		logger.info("====================================");
		logger.info("=  indo para "+ proxima.getId());
		
		limpaParcial(pageCorrente);
		
		if(conversacao){
			beginConversation();
		}else{
			endConversation();
			getRequest().setAttribute("nocid", "true");
		}
		
		
		if(pageCorrente!=null){
			addItem(pageCorrente);
		}
		
		pageCorrente = proxima;
		
		logger.info("====================================");
		
		return proxima.getUrl();
		
	}

	/**
	 * @param proxima
	 * @return
	 */
	public String redirect(Pages proxima) {
		return redirect(proxima, true);
	}


	
	/**
	 * só para  navegação entre uc 
	 * @param proxima
	 * @return
	 */
	public String facesRedirect(Pages proxima) {
		
		logger.info("====================================");
		logger.info("= indo para "+ proxima.getId());
		
		HttpServletRequest request = getRequest();
		request.setAttribute("faces-redirect", "true");
		request.setAttribute("nocid", "true");
		
		endConversation();
		
		limparBreadCrum();
		
		if(pageCorrente!=null){
			addItem(pageCorrente);
		}
		
		pageCorrente = proxima;
		
		logger.info("====================================");
		
		return proxima.getUrl();
		
	}

	private void limpaParcial(Pages atual) {
		List<MenuElement> list = this.breadcrumbmodel.getElements();
		
		int index = 0;
		boolean limpaParcial = false;
		for (MenuElement menuElement : list) {
			if(atual.getUrl().equals(((DefaultMenuItem)menuElement).getUrl())){
				if(index < list.size() - 1){
					limpaParcial = true;
				}
				break;
			}
			index++;
		}
		if(limpaParcial){
			this.breadcrumbmodel.getElements().removeAll(list.subList(index, list.size()));
		}
	}
	
	private void propagaConversation() {
		HttpServletRequest request = getRequest();
		request.setAttribute("cdi", conversation.getId());
		logger.info("=  propaga conversation id = "+ conversation.getId());
	}

	private void beginConversation() {
		if(conversation.isTransient()){
			conversation.begin();
			conversation.setTimeout(TIME_OUT_CONSERSATION);
			logger.info("=  cria conversation "+ conversation.getId());
		}
	}
	
	private void endConversation() {
		if(!conversation.isTransient()){
			logger.info("=  fim conversation id " + conversation.getId());
			conversation.end();
		}
	}
	
	public void limparBreadCrum(){
		this.breadcrumbmodel = new DynamicMenuModel();
	}
	
	public Boolean getRendered(){
		return !this.breadcrumbmodel.getElements().isEmpty();
	}
	
	public DynamicMenuModel getBreadcrumbmodel() {
		return breadcrumbmodel;
	}

	public void setBreadcrumbmodel(DynamicMenuModel breadcrumbmodel) {
		this.breadcrumbmodel = breadcrumbmodel;
	}

	public BreadCrumb getBreadCrumb() {
		return breadCrumb;
	}

	public void setBreadCrumb(BreadCrumb breadCrumb) {
		this.breadCrumb = breadCrumb;
	}
	
	 /*
     * Hack to pass cid to prettyfaces so it can add it to URL
     * DONT remove.
     */
    public String getCid() {
        return this.conversation.getId();
    }
    
    public void setCid(String cid) {
    }
	
    public boolean isPageCorrente(Pages page){
    	boolean retorno = false;
    	if(pageCorrente !=null){
    		pageCorrente.equals(page);
    	}
    	return retorno;
    }
}
