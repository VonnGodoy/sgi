package br.com.vle.comum;

import static br.com.vle.comum.UtilJSF.getBundleMsg;
import static br.com.vle.comum.UtilJSF.getContext;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import br.com.vle.controller.login.ProduceUsuarioLogado;
import br.com.vle.entidade.Usuario;

/**
 *
 * @author Victor Godoi
 */

@ManagedBean
@NegocioExceptionBinding
public class ControllerBase implements Serializable {

	private static final long serialVersionUID = -3294197389326442036L;

	@Inject
	private Navegacao navegacao;

	@Inject
	@ProduceUsuarioLogado
	private Usuario usuarioLogado;

	/**
	 * Sempre fecha conversacao se tiver aberta
	 * e seta o  faces redirect como true
	 * 
	 * 
	 * @param proxima
	 * @return
	 */
	public String facesRedirect(Pages proxima) {
		getContext().getExternalContext().getFlash().setKeepMessages(true);
		return navegacao.facesRedirect(proxima);
	}

	/**
	 * redireciona para proxima page  abrindo uma conversacao
	 * e a propaga para a page seguinte
	 * 
	 * @param proxima
	 * @return
	 */
	public String redirect(Pages proxima) {
		getContext().getExternalContext().getFlash().setKeepMessages(true);
		return navegacao.redirect(proxima);
	}
	
	/**
	 * redireciona para proxima page com opcao de
	 * abrindo uma conversacao ou nao
	 * 
	 * @param proxima
	 * @param conversacao
	 * @return
	 */
	public String redirect(Pages proxima, boolean conversacao) {
		getContext().getExternalContext().getFlash().setKeepMessages(true);
		return navegacao.redirect(proxima, conversacao);
	}

	/**
	 * volta para page anterior mantendo conversacao 
	 * se tive aberta
	 * @return
	 */
	public String voltar() {
		getContext().getExternalContext().getFlash().setKeepMessages(true);
		return navegacao.voltar();
	}
	
	/**
	 * volta para page anterior com opcao de fechar conversacao 
	 * se tive aberta
	 * 
	 * @param endConversacao
	 * @return
	 */
	public String voltar(boolean endConversacao) {
		getContext().getExternalContext().getFlash().setKeepMessages(true);
		return navegacao.voltar(endConversacao);
	}

	public void messageFatal(String msg) {
		String msgbundle = getBundleMsg(msg, new String[]{});
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
				msgbundle, msgbundle));
	}

	public void messageFatal(String msg, String... parametros) {
		String msgbundle = getBundleMsg( msg, parametros);
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
				msgbundle, msgbundle));
	}

	public void messageErro(String msg) {
		String msgbundle = getBundleMsg( msg, new String[]{});
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				msgbundle, msgbundle));
	}

	public void messageErro(String msg, String... parametros) {
		String msgbundle = getBundleMsg( msg, parametros);
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				msgbundle, msgbundle));
	}

	public void messageWarn(String msg) {
		String msgbundle = getBundleMsg( msg, new String[]{});
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				msgbundle, msgbundle));
	}

	public void messageWarn(String msg, String... parametros) {
		String msgbundle = getBundleMsg( msg, parametros);
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				msgbundle, msgbundle));
	}

	public void messageInfo(String msg) {
		String msgbundle = getBundleMsg( msg, new String[]{});
		getContext().addMessage("msggeral", new FacesMessage(FacesMessage.SEVERITY_INFO,
				msgbundle, msgbundle));
	}

	public void messageInfo(String msg, String... parametros) {
		FacesContext context = getContext();
		String msgbundle = getBundleMsg( msg, parametros);
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				msgbundle, msgbundle));
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	

}
