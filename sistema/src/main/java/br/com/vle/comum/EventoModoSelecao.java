package br.com.vle.comum;

import br.com.vle.entidade.PerfilAcesso;

/**
 *
 * @author Victor Godoi
 */

public class EventoModoSelecao {

	private Boolean modoSelecao;
	private Boolean multiplasSelecoes;
	
	/**
	 * @param modoSelecao
	 * @param multiplasSelecoes
	 */
	public EventoModoSelecao(Boolean modoSelecao, Boolean multiplasSelecoes) {
		super();
		this.modoSelecao = modoSelecao;
		this.multiplasSelecoes = multiplasSelecoes;
	}

	public Boolean getModoSelecao() {
		return modoSelecao;
	}

	public void setModoSelecao(Boolean modoSelecao) {
		this.modoSelecao = modoSelecao;
	}

	public Boolean getMultiplasSelecoes() {
		return multiplasSelecoes;
	}

	public void setMultiplasSelecoes(Boolean multiplasSelecoes) {
		this.multiplasSelecoes = multiplasSelecoes;
	}

}
