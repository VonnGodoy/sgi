package br.com.vle.servico.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.vle.entidade.dto.DTOFiltroBase;
import br.com.vle.entidade.dto.OperadorQuery;

/**
 *
 * @author Victor Godoi
 */

@Stateless
public class DaoUniversal {

    @PersistenceContext
    private EntityManager em;

    public DaoUniversal() {
    }

    /**
     * Criar entidade
     * 
     * @param T Object
     * @return t Object
     */
    public <T> T create(T t) {
        this.em.persist(t);
        flush();
        // this.em.refresh(t);
        return t;
    }

    public void flush() {
        this.em.flush();
    }

    /**
     * Buscar objeto
     * 
     * @param T Object
     * @param id Object
     * @return
     */
    public <T> T find(Class<T> type, Object id) {
        return this.em.find(type, id);
    }

    /**
     * Remover objeto
     * 
     * @param t Object
     * @param id Object
     */
    public <T> void delete(Class<T> type, Object id) {
        Object ref = this.em.find(type, id);
        this.em.remove(ref);
        flush();
    }

    /**
     * Atualizar entidade
     * 
     * @param T object
     * @return objeto atualizado
     */
    public <T> T update(T item) {
        T merge = (T) this.em.merge(item);
        flush();
        return merge;
    }

    /**
     * Retorna Lista de itens de acordo com os filtros
     * 
     * @param String namedQueryName
     * @return List
     */
    @SuppressWarnings("unchecked")
    public <W> List<W> buscaPorNamedQuery(String namedQueryName) {
        return this.em.createNamedQuery(namedQueryName).getResultList();
    }

    /**
     * Retorna Lista de itens de acordo com os filtros
     * 
     * @param String namedQueryName
     * @param Map parameters
     * @return List
     */
    @SuppressWarnings("unchecked")
    public <W> List<W> buscaPorNamedQuery(String namedQueryName, Map parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.em.createNamedQuery(namedQueryName);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }

    /**
     * Retorna Lista de itens de acordo com os filtros usando lazy loading / pagination
     * 
     * @param String namedQueryName
     * @param int start
     * @param int end
     * @return List
     */
    @SuppressWarnings("unchecked")
    public <W> List<W> buscaPorNamedQuery(String namedQueryName, int start, int end) {
        Query query = this.em.createNamedQuery(namedQueryName);
        query.setMaxResults(end - start);
        query.setFirstResult(start);
        return query.getResultList();
    }

    /**
     * Retorna Lista de itens de acordo com os filtros
     * 
     * @param String hql
     * @param DTOFiltroBase filtro
     * @return List
     */
    @SuppressWarnings("rawtypes")
    public List buscarPorFiltroDTO(String hql, DTOFiltroBase filtro) {
        return buscarPorFiltroDTO(hql, null, filtro);
    }

    /**
     * Retorna Lista de itens de acordo com os filtros
     * 
     * @param String hql
     * @param String apelidoTabela
     * @param DTOFiltroBase filtro
     * @return List
     */
    @SuppressWarnings("rawtypes")
    public List buscarPorFiltroDTO(String hql, String apelidoTabela, DTOFiltroBase filtro) {

        String montaQuery = filtro.montaQuery(hql, apelidoTabela);

        Query query = createQuery(montaQuery);

        if (filtro.getMaxResults() != 0) {
            query.setMaxResults(filtro.getMaxResults());
        }
        if (filtro.getFirstResult() != 0) {
            query.setFirstResult(filtro.getFirstResult());
        }

        if (!filtro.getFiltros().isEmpty()) {
            for (Iterator<String> iterator = filtro.getFiltros().keySet().iterator(); iterator.hasNext();) {
                OperadorQuery field = filtro.getFiltros().get(iterator.next());
                query.setParameter(field.getAtributo(), field.getValor());
            }
        }

        return query.getResultList();
    }

    /**
     * Retorna Quantidade de itens de acordo com os filtros
     * 
     * @param String hql
     * @param DTOFiltroBase filtro
     * @return int
     */
    public int totalPorFiltroDTO(String hql, DTOFiltroBase filtro) {
        return totalPorFiltroDTO(hql, null, filtro);
    }

    /**
     * Retorna Quantidade de itens de acordo com os filtros
     * 
     * @param String hql
     * @param String apelidoTabela
     * @param DTOFiltroBase filtro
     * @return int
     */
    public int totalPorFiltroDTO(String hql, String apelidoTabela, DTOFiltroBase filtro) {

        filtro.setOrdenacao(null);

        Query query = createQuery(filtro.montaQuery(hql, apelidoTabela));

        if (!filtro.getFiltros().isEmpty()) {
            for (Iterator iterator = filtro.getFiltros().keySet().iterator(); iterator.hasNext();) {
                OperadorQuery field = filtro.getFiltros().get(iterator.next());
                query.setParameter(field.getAtributo(), field.getValor());
            }
        }

        Number result = (Number) query.getSingleResult();
        return result.intValue();
    }

    /**
     * Criador de Query
     * 
     * @param String query
     * @return Query
     */
    public Query createQuery(String query) {
        return this.em.createQuery(query);
    }

    /**
     * Criador de Query
     * 
     * @param String query
     * @return Query
     */
    public Query createNamedQuery(String query) {
        return this.em.createNamedQuery(query);
    }

    /**
     * Criar conexao
     * 
     * @return EntityManager
     */
    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

}