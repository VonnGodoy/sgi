package br.com.vle.entidade.dto;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Victor Godoi
 */

@SuppressWarnings("serial")
public class DTOFiltroBase extends DTOBase {
	
	public static final String ASCENDING_JSF = "ASCENDING";
	public static final String DESCENDING_JSF = "DESCENDING";
	public static final String UNSORTED_JSF = "UNSORTED";
	
	private static final String CRESCENTE_HBM = "ASC";
	private static final String DECRESCENTE_HBM = "DESC";
	
	private static final long QTD_REGISTRO_PAGE = 100L;
	private static final Integer PRIMEIRO_REGISTRO_PAGE = 0;
	private String campoOrdenacao; 
	private String direcaoOrdenacao = CRESCENTE_HBM;
	private Long qtdRegistroPaginacao = QTD_REGISTRO_PAGE;
	private Integer primeiroRegistroPage = PRIMEIRO_REGISTRO_PAGE;
	private Map<String, OperadorQuery> filtros = new LinkedHashMap<String, OperadorQuery>();
	
	public void addFiltro(String atributoClasse, Object valor){
		filtros.put(atributoClasse, new Igual(atributoClasse, valor));
	}
	
	public void addFiltro(OperadorQuery valor){
		filtros.put(valor.getAtributo(),  valor );
	}
	
	public void setOrdenacao(String campo){
		setOrdenacao(campo, null);
	}
	
	public void setOrdenacao(String campo, String direcao){
		this.campoOrdenacao = campo;
		if(direcao != null && !direcao.equals(UNSORTED_JSF)){
			String direcaoHbm = converteDirecao(direcao);
			this.direcaoOrdenacao = direcaoHbm;
		}
	}

	private String converteDirecao(String direcao) {
		String order = CRESCENTE_HBM;
		if (direcao.equals(DESCENDING_JSF)) {
			order = DECRESCENTE_HBM;
		} 
		return order;
	}
	

	public void setPrimeiroRegistroPage(Integer primeiroRegistroPage) {
		this.primeiroRegistroPage = primeiroRegistroPage;
	}

	public void setQtdRegistroPaginacao(Long qtdRegistroPaginacao) {
		this.qtdRegistroPaginacao = qtdRegistroPaginacao;
	}
	
	public Map<String, OperadorQuery> getFiltros() {
		return filtros;
	}
	
	public int getFirstResult() {
		return primeiroRegistroPage.intValue();
	}

	public int getMaxResults() {
		return qtdRegistroPaginacao.intValue();
	}
	
	public Long getUltimoRegistroPage() {
		return primeiroRegistroPage + qtdRegistroPaginacao;
	}
	
	public String getCampoOrdenacao() {
		return campoOrdenacao;
	}

	public void setCampoOrdenacao(String campoOrdenacao) {
		this.campoOrdenacao = campoOrdenacao;
	}

	public String getDirecaoOrdenacao() {
		return direcaoOrdenacao;
	}

	public void setDirecaoOrdenacao(String direcaoOrdenacao) {
		this.direcaoOrdenacao = direcaoOrdenacao;
	}

	public Long getQtdRegistroPaginacao() {
		return qtdRegistroPaginacao;
	}

	public Integer getPrimeiroRegistroPage() {
		return primeiroRegistroPage;
	}

	public void setFiltros(Map<String, OperadorQuery> filtros) {
		this.filtros = filtros;
	}

	public String montaQuery(String hbm){
		return montaQuery(hbm, null);
	}
	
	public String montaQuery(String hbm, String apelidoFiltro){
		
		StringBuffer sb = new StringBuffer(hbm.trim());
		
		if(getFiltros() != null && !getFiltros().isEmpty()){
			sb.append(" WHERE ");
			for (Iterator iterator = getFiltros().keySet().iterator(); iterator.hasNext();) {
				String field = (String) iterator.next();
				OperadorQuery operador = getFiltros().get(field);
				
				if(apelidoFiltro!=null && !"".equals(apelidoFiltro)){
					sb.append(apelidoFiltro).append(".");
				}
				
				
				operador.operacao(sb);
//				sb.append(field);
//				sb.append(" = ");
//				sb.append(" :");
//				sb.append(field);
				
				if(iterator.hasNext()){
					sb.append(" AND ");
				}
			}
		}

		if (getCampoOrdenacao() != null) {
			sb.append(" ORDER BY ");
			if(apelidoFiltro!=null && !"".equals(apelidoFiltro)){
				sb.append(apelidoFiltro).append(".");
			}
			sb.append(getCampoOrdenacao())
			.append(" ")
			.append(getDirecaoOrdenacao());
		}
		
		return sb.toString();
		
	}
	

}
